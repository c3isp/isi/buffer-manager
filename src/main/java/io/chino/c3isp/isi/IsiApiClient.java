package io.chino.c3isp.isi;

import io.chino.c3isp.buffermanager.BufferManager;
import io.chino.c3isp.buffermanager.cfg.IsiApiConfig;
import io.chino.c3isp.buffermanager.restapi.impl.BufferManagerController;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.AsyncClientHttpRequest;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.OkHttp3ClientHttpRequestFactory;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.util.Scanner;

/**
 * Wraps the API calls towards the C3ISP ISI API, so that they can be invoked programmatically.
 */
public class IsiApiClient {

    //    private final static String ISI_API_URL = "https://isic3isp.iit.cnr.it:8443/isi-api/";
    private static IsiApiConfig config;
    private static String ISI_API_URL;
    private static String API_VERSION;
    private static String AUTH_ID;
    private static String AUTH_SECRET;

    private static String DPO;

    public static void setConfig(IsiApiConfig configuration) {
        config = configuration;
        ISI_API_URL = config.getProperty("url");
        API_VERSION = config.getProperty("version");
        AUTH_ID = config.getProperty("username");
        AUTH_SECRET = config.getProperty("password");
        DPO = API_VERSION + "dpo/";
    }

    /**
     * Logger class, can be used within the methods to log information
     */
    final static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(BufferManagerController.class);

    static OkHttp3ClientHttpRequestFactory requestFactory = initRequestFactory();

    private static OkHttp3ClientHttpRequestFactory initRequestFactory() {
        OkHttp3ClientHttpRequestFactory factory = new OkHttp3ClientHttpRequestFactory();
        factory.setConnectTimeout(10 * 1000);
        factory.setReadTimeout(30 * 1000);
        factory.setWriteTimeout(10 * 1000);

        return factory;
    }

    /**
     * Retrieve a DPO from ISI given its DPO ID (Uses ISI API v1) in a synchronous way
     *
     * @param DPOid the ID of the DPO to be read
     * @param xC3ispMetadata a String containing the metadata header required by ISI API
     *
     * @return the DPO with the given ID, as a String
     *
     *
     * @throws HttpStatusCodeException
     */
    public static InputStream retrieveCTI(String DPOid, String xC3ispMetadata) throws HttpStatusCodeException {

        String url = ISI_API_URL + DPO + DPOid + "/";

        LOGGER.info("Calling ISI API at " + url);

        // HTTP request
        ClientHttpRequest req = requestFactory.createRequest(URI.create(url), HttpMethod.GET);
        // set up Authorization
        String auth = AUTH_ID + ":" + AUTH_SECRET;
        auth = DatatypeConverter.printBase64Binary(auth.getBytes()); // base64 encoding
        req.getHeaders().set("Authorization", "Basic " + auth);

        // Write metadata for the ReadDPO in the header
        req.getHeaders().set(
                "X-c3isp-input_metadata",
                xC3ispMetadata != null
                        ? xC3ispMetadata
                        : "{}"              // default empty metadata
        );

        // send request and return the response
        ClientHttpResponse resp;
        Scanner s = null;
        try {
            resp = req.execute();
            checkHttpStatus(resp);  // check for HTTP errors and throw a HttpStatusCodeException accordingly
            LOGGER.info("Successfully retrieved DPO: " + DPOid);
            return resp.getBody();
        } catch (SocketTimeoutException timeoutex) {
            LOGGER.error("Got timeout while reading DPO: " + DPOid, timeoutex);
            throw new HttpServerErrorException(
                    HttpStatus.GATEWAY_TIMEOUT,
                    "ISI API timeout. Check the content of '" + BufferManager.C3ISP_METADATA_HEADER + "' header and, if the problem persists, contact an admin."
            );
        } catch (HttpStatusCodeException httpex) {
            LOGGER.error("HTTP error " + httpex.getStatusCode().value() + " while retrieving DPO: " + DPOid, httpex);
            throw httpex;
        } catch (IOException ioex) {
            LOGGER.error("I/O error while retrieving DPO: " + DPOid, ioex);
            throw new HttpStatusCodeException(HttpStatus.INTERNAL_SERVER_ERROR, ioex.getMessage()) {};
        } finally {
            if (s != null)
                s.close();
        }
    }

    /**
     * Check that the response doesn't contain an HTTP error.
     *
     * @param resp the HTTP response to analyze
     *
     * @throws HttpStatusCodeException if an HTTP error is found
     */
    protected static void checkHttpStatus(ClientHttpResponse resp) throws HttpStatusCodeException {
        HttpStatus status;
        String statusTxt;
        try {
            status = resp.getStatusCode();
            statusTxt = resp.getStatusText();
            switch (status) {
                case OK:
                    return;
                case FORBIDDEN:
                case UNAUTHORIZED:
                case NOT_FOUND:
                    throw new HttpClientErrorException(status, statusTxt);
                case INTERNAL_SERVER_ERROR:
                    throw new HttpServerErrorException(status, statusTxt);
                default:
                    throw new HttpStatusCodeException(status, statusTxt) {};
            }
        } catch (IOException ioex) {
            LOGGER.error("Unable to parse response from DPOS.", ioex);
            throw new HttpStatusCodeException(HttpStatus.INTERNAL_SERVER_ERROR, "Read operation returned an invalid response.") {};
        }
    }

    /**
     * Retrieve a DPO from ISI given its DPO ID (Uses ISI API v1) with an asynchronous call
     *
     * @param DPOid the ID of the DPO to be read
     *
     * @return a {@link ListenableFuture<ClientHttpResponse>} where the API response can be read from, as soon as the Async call is done.
     * @throws IOException If there was an I/O error (connection errors, mainly)
     */
    public static ListenableFuture<ClientHttpResponse> retrieveDPOAsync(String DPOid) throws IOException {
        AsyncClientHttpRequest req = requestFactory.createAsyncRequest(URI.create(ISI_API_URL + DPO), HttpMethod.GET);

        // set up Authorization
        String auth = AUTH_ID + ":" + AUTH_SECRET;
        auth = DatatypeConverter.printBase64Binary(auth.getBytes()); // base64 encoding
        req.getHeaders().set("Authorization", "Basic " + auth);

        return req.executeAsync();

    }

}
