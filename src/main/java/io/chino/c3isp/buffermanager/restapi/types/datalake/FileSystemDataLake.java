package io.chino.c3isp.buffermanager.restapi.types.datalake;

import io.chino.c3isp.buffermanager.cfg.BaseConfigurer;
import io.chino.c3isp.buffermanager.cfg.FileSystemConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Date;
import java.util.LinkedList;

/**
 * Implementation of {@link DataLake} class that writes DPO on a local file system.
 * The URI for this DL are in the form:<br>
 * <br>
 *     {@code file:///opt/isi/datalakebuffer/[random UUID]}<br>
 * <br>aut
 * and files can be accessed from the IAI via NFS.
 */
@Configuration
public class FileSystemDataLake extends DataLake {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileSystemDataLake.class);

    static final String URI_PROTOCOL = "file:///";

    /**
     * File System configuration, initialized in package-private constructor
     * {@link #FileSystemDataLake(FileSystemConfig)}
     */
    static FileSystemConfig classDefaultConfigurer;

    /**
     * the root folder where the DataLakes will be saved.<br>
     * <br>
     * See {@code data.filesystem.root} in application.properties
     */
    private String fileSystemRoot;

    /**
     * @param classDefaultConfig an autowired {@link BaseConfigurer}
     */
    @Autowired // DO NOT SET to private, must be accessed by Spring
    FileSystemDataLake(FileSystemConfig classDefaultConfig) {
        super(classDefaultConfig);
        // when Spring initialization occurs, store the configurer in the static pool
        // so it can be reused when this class is created using 'new'
        classDefaultConfigurer = classDefaultConfig;
        // init Data Lake
        init();
    }

    /**
     * Public constructor used by {@link DataLakeManager} and all classes that need to
     * instantiate this Data Lake with {@code new}.
     */
    public FileSystemDataLake() {
        super(classDefaultConfigurer);
        init();
    }

    @Override
    protected void initConfig(BaseConfigurer instanceConfigurer) {
        // init configuration variables
        fileSystemRoot = getConfigProperty("root");

        String tmp = getConfigProperty("expires-in");
        try {
            if (tmp != null) super.maxValidityMillis = Long.valueOf(tmp);
        } catch (NumberFormatException ignored) { /* not a number. use default value (6hrs) */ }
    }

    @Override
    protected void init() {
        File root = new File(URI.create(fileSystemRoot));
        String msg = "failed to connect to DataLake root folder";
        boolean doesNotExist, created;
        try {
            doesNotExist = !root.exists();
        } catch (SecurityException ex) {
            LOGGER.error(msg, ex);
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE, msg);
        }

        try {
            if (doesNotExist) {
                LOGGER.info("Creating new FileSystemDataLake root folder in " + fileSystemRoot);
                msg = "Failed to create directory structure.";
                created = root.mkdirs();
                if (created) {
                    LOGGER.info("FileSystemDataLake root folder is " + root.getAbsolutePath());
                } else {
                    LOGGER.error(msg + " Data Lake File System is not available.");
                    throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to create Data Lake");
                }
            }
        } catch (SecurityException ex) {
            LOGGER.error(msg, ex);
            throw new RemoteDataLakeException(HttpStatus.UNAUTHORIZED, msg);
        }
    }

    @Override
    public String createDL() throws RemoteDataLakeException {
        // Create folder
        String folderPath;
        File dataLake;
        do {
            folderPath = fileSystemRoot + getUUID();
            dataLake = new File(URI.create(folderPath));
        } while (dataLake.exists());
        try {
            boolean created = dataLake.mkdir();
            if (!created) {
                // exception not caught here, handled by the BufferManagerController
                throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR, "failed to create Data Lake.");
            } else {
                this.dataLakeURI = folderPath;
                setURIValue(folderPath);
                LOGGER.info("Successfully created DataLake on file system at '" + folderPath + "'");
                return folderPath;
            }
        } catch (SecurityException ex) {
            LOGGER.error("Failed to create Data Lake in " + folderPath, ex);
            throw new RemoteDataLakeException(HttpStatus.UNAUTHORIZED, "failed to create Data Lake.");
        }
    }

    @Override
    public String writeDPO(String fileName, String newElement) throws RemoteDataLakeException {
        if (dataLakeURI == null) {
            LOGGER.error("writeDPO called before initializing Data Lake URI");
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "Data Lake not initialized. Please report this error to a system admin.");
        }
        String DPOFileName = getFullPath(fileName);
        File destinationFile = new File(URI.create(DPOFileName));
        FileOutputStream out = null;

        try {
            out = new FileOutputStream(destinationFile);
            out.write(newElement.getBytes());
            out.flush();
        } catch (IOException e) {
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE,
                    "Failed to write DPO to Data Lake. Reason: " + e.getMessage());
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    LOGGER.warn("Unable to close output file.", e);
                }
            }
        }

        return DPOFileName;
    }

    /**
     * Alternative version of {@link DataLake#writeDPO(String, String) writeDPO} that copies an existing file to this
     * {@link FileSystemDataLake}.
     *
     * @param fileName the name that will identify the DPO on the {@link DataLake} (suggested name: the DPO ID)
     * @param originalFile the {@link File} instance to copy there.
     *
     * @return either an error message or the name that was assigned to the new DPO on the DataLake. (e.g. a file name on a FS,
     * or an attribute value of a SQL database)
     *
     * @throws RemoteDataLakeException an error occurred while communicating with the remote Data Lake host
     */
    public String writeDPO(String fileName, File originalFile) throws RemoteDataLakeException {
        String DPOFileName = getFullPath(fileName);
        Path origin = originalFile.toPath();
        Path destination = Paths.get(URI.create(DPOFileName));
        try {
            if (!originalFile.setReadable(true)) {
                throw new IOException("The DPO content was written but cannot be read.");
            }
            Files.copy(origin, destination, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE,
                    "Failed to write DPO to Data Lake. Reason: " + e.getMessage());
        }
        return DPOFileName;
    }

    @Override
    public void deleteDL() throws RemoteDataLakeException {
        File dataLakeFolder;
        try {
            dataLakeFolder = getFolder();
        } catch (SecurityException secEx) {
            throw new RemoteDataLakeException(HttpStatus.FORBIDDEN, "Failed to create Data Lake.");
        }

        try {
            boolean deleted = deleteContent(dataLakeFolder);
            if (!deleted)
                throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR,
                        "Unable to delete some of the files in " + dataLakeURI);

            deleted = dataLakeFolder.delete();
            if (!deleted) {
                throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR,
                        "Unable to delete Data Lake. Maybe it was already deleted?");
            }
        } catch (SecurityException secEx) {
            // write internal log
            LOGGER.error("failed to delete FileSystemDataLake root", secEx);
            // notify user
            throw new RemoteDataLakeException(HttpStatus.UNAUTHORIZED,
                    "You don't have 'delete' permission over " + dataLakeURI + "'. " +
                    "If you think this is a mistake, please contact an admin.");
        }
    }

    /**
     * check that this {@link FileSystemDataLake} represents a valid Data Lake folder,
     * i.e. make sure that the provided URI:
     * <ul>
     *     <li>is inside the Data Lake root folder {@link #fileSystemRoot}</li>
     *     <li>is NOT the Data Lake root itself</li>
     * </ul>
     *
     * @return the folder connected to this Data Lake, as a Java {@link File}.
     *
     * @throws RemoteDataLakeException the file this Data Lake points to doesn't exists or is not valid.
     * @throws SecurityException failed to check that the specified folder exists
     */
    private File getFolder() throws RemoteDataLakeException, SecurityException {
        File dataLakeFolder = new File(URI.create(dataLakeURI));
        File rootFolder = new File(URI.create(fileSystemRoot));
        if (dataLakeURI.equals(fileSystemRoot)
                || !dataLakeFolder.getAbsolutePath().startsWith(rootFolder.getAbsolutePath())
        ) {
            throw new RemoteDataLakeException(HttpStatus.FORBIDDEN, "You can't delete this folder.");
        }

        boolean exists;
        try {
            exists = dataLakeFolder.exists();
        } catch (SecurityException se) {
            LOGGER.error("Failed to read Data Lake in " + dataLakeURI, se);
            throw se;
        }

        if (!exists) {
            // throw HTTP error 502 for SQL and HDFS!
            throw new RemoteDataLakeException(HttpStatus.NOT_FOUND,
                    "The Data Lake at '" + dataLakeURI + "' doesn't exist, or it may be expired."
            );
        }

        return dataLakeFolder;
    }

    /**
     * Delete all files in a directory.
     *
     * @param dataLake the {@link File} object - either a file to delete or a directory to empty (in this case the
     *                 directory itself is NOT deleted).
     * @return [@code true} if at the end of the method the parameter {@link File} was deleted, along with all of its
     * content. Otherwise (e.g. if the parameter represents a directory and the BufferManager fails to delete
     * one file), the method will return {@code false}.
     */
    private boolean deleteContent(File dataLake) {
        if (dataLake == null) {
            // no file was passed
            throw new NullPointerException("null File.");
        } else if (dataLake.isDirectory()) {
            // parameter 'dataLake' is a folder
            File folder = dataLake;
            File[] folderContent = folder.listFiles();
            if (folderContent.length == 0) {
                // folder is already empty - don't delete folder
                return true;
            } else {
                // folder is not empty - delete content

                // control flag:
                boolean allFilesWereDeleted = true;

                for (File nestedElement : folderContent) {
                    if (nestedElement.isFile()) {
                        File nestedFile = nestedElement;
                        // check that this file can be deleted and update the flag
                        allFilesWereDeleted = allFilesWereDeleted && nestedFile.delete();
                    } else if (nestedElement.isDirectory()) {
                        File nestedDirectory = nestedElement;
                        // delete folder content recursively and update the flag
                        allFilesWereDeleted = allFilesWereDeleted && deleteContent(nestedDirectory);
                        // delete directory and update the flag
                        allFilesWereDeleted = allFilesWereDeleted && nestedDirectory.delete();
                    }
                }
                // In the end the flag is TRUE if all the delete operations returned true
                return allFilesWereDeleted;
            }
        } else {
            // parameter 'dataLake' is a single file - delete file
            return dataLake.delete();
        }
    }

    @Override
    public void disconnect() {
        LOGGER.info("FileSystemDataLake instance was released.");
    }

    @Override
    public Date getCreationDate() throws Exception {
        Path dataLakeFolderPath;
        try {
            File dataLake = getFolder();
            dataLakeFolderPath = dataLake.toPath();
            // Read and return creation date
            BasicFileAttributes attr = Files.readAttributes(dataLakeFolderPath, BasicFileAttributes.class);
            return new Date(attr.creationTime().toMillis());
        } catch (SecurityException secEx) {
            throw new RemoteDataLakeException(HttpStatus.FORBIDDEN, "Failed to get Data Lake's creation date.");
        }
    }

    /**
     * Utility method that checks and returns the {@link Date} of the last access to this DataLake.
     *
     * @return
     *
     * @throws Exception
     */
    public Date getLastAccess() throws Exception {
        Path dataLakeFolderPath;
        try {
            File dataLake = getFolder();
            dataLakeFolderPath = dataLake.toPath();
            // Read and return last access date
            BasicFileAttributes attr = Files.readAttributes(dataLakeFolderPath, BasicFileAttributes.class);
            return new Date(attr.lastAccessTime().toMillis());
        } catch (SecurityException secEx) {
            throw new RemoteDataLakeException(HttpStatus.FORBIDDEN, "Failed to get Data Lake's last access date.");
        }
    }

    /**
     * Compute the full path to the specified file in this instance of the {@link FileSystemDataLake}.
     * Basically, prepends the DL URI and appends the {@link #DPO_FILE_EXTENSION}
     *
     * @param fileName the name of the file to write.
     *
     * @return a {@link String} containing the full path to the file.
     */
    private String getFullPath(String fileName) {
        return dataLakeURI + "/" + fileName + DPO_FILE_EXTENSION;
    }

    @Override
    LinkedList<String> all() throws RemoteDataLakeException {
        LinkedList<String> names = new LinkedList<>();
        File root = new File(URI.create(fileSystemRoot));
        String msg = "failed to connect to DataLake root folder";
        try {
            if (!root.exists()) return names;

            msg = "failed to read the content of DataLake root folder";
            File[] files = root.listFiles();
            if (files == null)
                throw new IOException(
                        root.getAbsolutePath() + ": not a directory, or an unknown I/O error occurred."
                );
            for (File dataLake : files) {
                try {
                    names.add(dataLake.toURI().toURL().toExternalForm());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    LOGGER.error("Wrong URI format: " + dataLake.toURI().toString());
                }
            }
        } catch (SecurityException | IOException ex) {
            LOGGER.error(msg, ex);
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE, msg);
        }

        return names;
    }

    @Override
    public String toString() {
        return getURI();
    }
}

