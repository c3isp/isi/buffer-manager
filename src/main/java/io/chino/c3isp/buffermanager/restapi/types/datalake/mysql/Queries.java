package io.chino.c3isp.buffermanager.restapi.types.datalake.mysql;

import io.chino.c3isp.buffermanager.restapi.types.datalake.DataLake;
import io.chino.c3isp.buffermanager.restapi.types.datalake.MySQLDataLakeBuffer;
import io.chino.c3isp.buffermanager.restapi.types.datalake.MySQLVirtualDataLake;
import io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeType;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.LinkedList;

public class Queries {
    private PreparedStatement insertStmt = null;
    private String table;
    private Connection db;

    /* CREATE */

    /**
     * Perform a MySQL query to create a new Data Lake table in the database and get an instance of {@link Queries}
     * to execute future queries on that table
     *
     * @return an instance of {@link Queries} that can perform multiple 'insert' queries in an
     *          optimized way.
     *
     * @throws SQLException connection to database failed, e.g. connection was closed
     * @throws SQLTimeoutException SQL server timeout
     */
    public static Queries createTable(Connection connection, String table) throws SQLException, SQLTimeoutException {
        validateName(table);

        String CREATE_DPO_TABLE = "CREATE TABLE " + table + " (" +
                "  id INT NOT NULL AUTO_INCREMENT," +
                "  DPO_id VARCHAR(100) NOT NULL UNIQUE," +
                "  cti BLOB NOT NULL," +
                "  PRIMARY KEY (id)" +
                ");",
                INSERT_DPO_QUERY = "INSERT INTO " + table +
                        "  VALUES (default, ?, ?);";

        // create Data Lake table in database
        Statement stm = connection.createStatement();
        stm.executeUpdate(CREATE_DPO_TABLE);
        stm.close();

        // compile INSERT statement for this table
        Queries instance = new Queries(connection, table);
        instance.insertStmt = connection.prepareStatement(INSERT_DPO_QUERY);
        return instance;
    }

    public static void createDatabase(Connection connection, String dbName) throws SQLException {
        validateName(dbName);
        String CREATE_NEW_DB = "CREATE DATABASE "+ dbName + ";";

        Statement stm = connection.createStatement();
        stm.execute(CREATE_NEW_DB);
        stm.close();

        connection.setCatalog(dbName);
    }

    public static void dropDatabase(Connection rootConnection, String dbName) throws SQLException {
        validateName(dbName);
        String DELETE_DPO_TABLE = "DROP DATABASE " + dbName + ";";

        Statement stm = rootConnection.createStatement();
        stm.executeUpdate(DELETE_DPO_TABLE);
        stm.close();
    }

    public static void emptyDatabase(Connection userConnection, String dbName) throws SQLException {
        validateName(dbName);

        LinkedList<String> tables = new LinkedList<>();

        ResultSet rs = userConnection.getMetaData()
                .getTables(dbName, null, "%", null);
        boolean hasNext = rs.next();
        while(hasNext) {
            tables.add(rs.getNString(3));
            hasNext = rs.next();
        }
        rs.close();

        for (String tableName : tables) {
            new Queries(userConnection, tableName).dropTable();
        }
    }

    /**
     * Create a new user with the provided credentials in the database and grant them permissions over the DLB table.
     *
     * @param connection an open {@link Connection} to a MySQL database
     * @param username the user's username
     * @param password the user's password
     *
     * @throws SQLException connection to database failed, e.g. connection was closed
     * @throws SQLTimeoutException SQL server timeout
     */
    public void createUserWithAllPermissions(Connection connection, DataLakeType type, String username, String password)
            throws SQLException, SQLTimeoutException
    {
        MySQLDataLakeBuffer dataLake = (type == DataLakeType.DLB)
                ? new MySQLDataLakeBuffer("Queries.createUserWithAllPermissions")
                : new MySQLVirtualDataLake("Queries.createUserWithAllPermissions");

        String databaseUser = "'" + username + "'" + "@" + "'" + dataLake.getAllowedHost() + "'";

        String resourceName;
        String database = connection.getCatalog();
        if (database.equals(dataLake.DLB_DATABASE_NAME)) {
            resourceName = database + "." + table;
        } else {
            resourceName = database + ".*";
        }

        Statement stm = connection.createStatement();
        stm.addBatch("GRANT USAGE ON " + resourceName + " TO " + databaseUser + " IDENTIFIED BY '" + password +"';");
        stm.addBatch("GRANT ALL PRIVILEGES ON " + resourceName +  " TO " + databaseUser + ";");
        stm.addBatch("FLUSH PRIVILEGES;");
        stm.executeBatch();
        stm.close();

        try {
            // Attempt to login
            Connection userLoginTest = DriverManager.getConnection(
                    connection.getMetaData().getURL(),
                    username,
                    password
            );
            userLoginTest.close();
        } catch (SQLException e) {
            if (e.getMessage().contains("Access denied for user '" + username + "'")) {
                // Exception was caused by wrong resolution of users to anonymous.
                // see javadoc of method 'clearAnonymousUsers(Connection)'
                int count = clearAnonymousUsers(connection); // delete anonymous users as root
                if (count > 0) {
                    LoggerFactory.getLogger("MySQL DB " + connection.getCatalog())
                            .info(count + " anonymous users were found in the Database and have been deleted.");
                }

                Connection userLoginTest = DriverManager.getConnection(
                        connection.getMetaData().getURL(),
                        username,
                        password
                );
                userLoginTest.close();
            } else {
                throw new SQLException("Unable to login to DB with user " + username + " and password " + password, e);
            }
        }
    }

    /**
     * Check if any user has an empty string as username and delete it.<br/>
     * <br/>
     * <i>
     *     This solution prevents a problematic situation where MySQL auth module
     *     sees effectively the empty string as a wildcard and matches any username
     *     to the anonymous user. Anonymous users are generally safe to delete.
     * </i>
     *
     * @see <a href="https://dba.stackexchange.com/a/10897">this answer</a> on StackExchange
     *
     * @param rootConnection a {@link Connection} that grants root access to the DB
     *
     * @return the number of deleted users
     *
     * @throws SQLException if a DB access error occurs
     */
    private int clearAnonymousUsers(Connection rootConnection) throws SQLException {
        Statement statement = rootConnection.createStatement();
        LinkedList<String> users = new LinkedList<>();

        ResultSet emptyUsers = statement.executeQuery("SELECT Host FROM mysql.user WHERE User='';");

        while(emptyUsers.next()) {
            String host = emptyUsers.getNString("Host");
            users.add("''@'" + host + "'");
        }
        emptyUsers.close();

        for (String username : users) {
            statement.executeUpdate("DROP USER " + username + ";");
        }

        return users.size();
    }

    /**
     * Get an instance of {@link Queries} that can used to execute SQL queries
     * on the specified database and table.
     *
     * @param databaseConnection a {@link Connection} to a MySQL database
     * @param destinationTable   the name of the table to perform queries on.
     *
     * @see #createTable(Connection, String) There is an alternative factory method to create
     *      a new Data Lake in this database
     */
    public Queries(Connection databaseConnection, String destinationTable) {
        this.db = databaseConnection;
        this.table = destinationTable;
        INSERT_DPO_QUERY = "INSERT INTO " + this.table + "  VALUES (default, ?, ?);";
    }

    /* INSERT */

    private final String INSERT_DPO_QUERY;

    public PreparedStatement insertDPO(String ctiContent, String fileName) throws SQLException {
        validateName(table);

        String data = "SQL PreparedStatement";
        int param = 0;

        try {
            if (insertStmt == null) {
                insertStmt = this.db.prepareStatement(INSERT_DPO_QUERY);
            }

            data = "file name";
            insertStmt.setString(++param, fileName);

            data = "cti";
            insertStmt.setString(++param, ctiContent);

        } catch (SQLException e) {
            // then send error msg back to MySQLDataLake
            throw new SQLException("Error while writing " + data, e.getSQLState(), e);
        } finally {
            LoggerFactory.getLogger(Queries.class).debug("Written " + param + " parameters in query.");
        }
        return insertStmt;
    }

    /**
     * Final operations to be performed on this query generator after all the DPOs have been written.
     *
     * @throws SQLException database connection error
     * @see DataLake#disconnect()
     */
    public void done() throws SQLException {
        if (insertStmt != null) {
            insertStmt.close();
            insertStmt = null;
        }
    }

    /* SELECT */

    /**
     * Return a SQL query to retrieve a particular DPO from the table of the database that
     * is bound to this object
     *
     * @param DPO_id the name that was passed as DPO ID to the
     *               {@link #insertDPO(String, String) insertDPO}
     *               method.
     * @return a {@link String} containing a SQL query to retrieve the DPO from the MySQL database
     */
    public String getDpoSelectQueryString(String DPO_id) {
        return "SELECT cti FROM " + table + " WHERE DPO_id='" + DPO_id + "' LIMIT 1";
    }

    /* DROP */

    /**
     * Perform a MySQL query to delete an existing Data Lake table and all its content
     * from the database
     *
     * @throws SQLException connection to database failed, e.g. connection was closed
     * @throws SQLTimeoutException SQL server timeout
     */
    public void dropTable() throws SQLException, SQLTimeoutException {
        validateName(table);
        String DELETE_DPO_TABLE = "DROP TABLE " + table + ";";

        Statement stm = db.createStatement();
        stm.executeUpdate(DELETE_DPO_TABLE);
        stm.close();
    }

    /**
     * Perform a MySQL query to delete a user of a DLB upon deletion.
     *
     * @param rootConnection
     * @param userName
     *
     * @throws SQLException connection to database failed, e.g. connection was closed
     * @throws SQLTimeoutException SQL server timeout
     */
    public static void dropUser(Connection rootConnection, DataLakeType type, String userName, String host)
            throws SQLException, SQLTimeoutException
    {
        if (!userName.contains("@")) {
            // write hostname in username
            userName += "'@'" + host;
        }
        String escapedUserName = "'" + userName + "'";
        if (userName.startsWith("root")) {
            throw new SecurityException("Can not delete user " + escapedUserName + ". Can not delete a root user");
        }
        String DELETE_TEMP_USER = "DROP USER " + escapedUserName + ";"; // @localhost is already in the URL parameter "usr"

        Statement stm = rootConnection.createStatement();
        stm.execute(DELETE_TEMP_USER);
        stm.close();
    }

    /* READ METADATA */

    /**
     * Read metadata of a specific table from the MySQL
     *
     * @return
     * @throws SQLException
     * @throws SQLTimeoutException
     */
    public static MySQLMetadata getMetadata(Connection connection, String name, DataLakeType type) throws SQLException, SQLTimeoutException {
        validateName(name);

        String GET_DATES;
        if (type == DataLakeType.DLB)
            GET_DATES = "SELECT t.TABLE_NAME, t.CREATE_TIME, t.UPDATE_TIME FROM information_schema.TABLES t " +
                    "WHERE t.TABLE_NAME = \"" + name + "\" " +
                    "LIMIT 1;";
        else
            GET_DATES = "SELECT t.TABLE_SCHEMA AS table_name, t.CREATE_TIME, t.UPDATE_TIME FROM information_schema.TABLES t " +
                    "WHERE t.TABLE_SCHEMA = \"" + name + "\" " +
                    "LIMIT 1;";

        Statement stm = connection.createStatement();
        ResultSet results = stm.executeQuery(GET_DATES);
        MySQLMetadata metadata = MySQLMetadata.fromNextItem(results);
        stm.close();

        return metadata;
    }

    public static final String[] nonAllowedStrings = new String[]{";", "\"", "'", "-", ".", "\\", "/"};

    /**
     * Check that the provided name is safe to be inserted in a SQL query.
     * Variables used in {@link Queries} are forced not to use special characters defined in
     * {@link #nonAllowedStrings}
     *
     * @param name the content of the variable to validate
     *
     * @throws SQLSyntaxErrorException
     */
    public static void validateName(String name) throws SQLSyntaxErrorException {
        for (String str : nonAllowedStrings) {
            if (name.contains(str)) {
                throw new SQLSyntaxErrorException("String <" + str + "> in name <" + name + "> is not allowed.");
            }
        }
    }

    /**
     * Wraps the metadata of a MySQL table so they are easily and safely accessible for Java methods to read.
     */
    public static class MySQLMetadata {
        private String tableName;
        private java.util.Date creationDate;
        private java.util.Date updateDate;

        private MySQLMetadata(String tableName, Timestamp creationTime, Timestamp updateTime) {
            this.tableName = tableName;
            this.creationDate = (creationTime == null) ? null
                    : new java.util.Date(creationTime.getTime());
            this.updateDate = (updateTime == null) ? null
                    : new java.util.Date(updateTime.getTime());
        }

        /**
         * Calls {@link ResultSet#next() next()} on the provided {@link ResultSet} and builds a {@link MySQLMetadata}
         * for the selected object. The {@link ResultSet} must contain these 3 columns (case-insensitive):
         * <ul>
         *     <li>{@code table_name}</li>
         *     <li>{@code create_time}</li>
         *     <li>{@code update_time}</li>
         * </ul>
         *
         * @param set a {@link ResultSet} from a MySQL query.
         *
         * @return an instance of {@link MySQLMetadata} for the next element of the {@link ResultSet}. If the call to
         * {@link ResultSet#next() next()} returns {@code false}, this method returns null.
         *
         * @throws SQLException database access error, wrong format of the columns in the provided {@link ResultSet}
         * or this method was called passing a closed {@link ResultSet}.
         *
         * @see ResultSet#next()
         */
        public static MySQLMetadata fromNextItem(ResultSet set) throws SQLException {
            String table = "";
            Timestamp create = null, update = null;

            if (set.next()) {
                table = set.getString("table_name");
                create = set.getTimestamp("create_time");
                update = set.getTimestamp("update_time");
            }
            return new MySQLMetadata(table, create, update);
        }

        /**
         * @return the name of the table
         */
        public String getTableName() {
            return tableName;
        }

        /**
         * @return the date when the associated table was created
         *
         * @see #getTableName()
         */
        public java.util.Date getCreationDate() {
            return creationDate;
        }

        /**
         * @return the date of the last update of the associated table.
         * May be {@code null} if the table was never edited after its creation.
         *
         * @see #getTableName()
         */
        public java.util.Date getLastUpdateDate() {
            return updateDate;
        }
    }

    /* LIST ALL DATA LAKES */

    /**
     * Get a list of all the DLBs.
     *
     * <!-- IMPORTANT IMPLEMENTATION NOTE: please keep package-private -->
     *
     * @param connection the {@link Connection} to the DLB database as root user
     * @param dlbSchemaName the name of the MySQL schema which contains the DLBs
     *
     * @return a list of names of all the tables in the DLB schema.
     *
     * @throws SQLException
     * @throws SQLTimeoutException
     */
    public static LinkedList<String> listDLBTables(Connection connection, String dlbSchemaName)
            throws SQLException, SQLTimeoutException
    {
        validateName(dlbSchemaName);
        LinkedList<String> tables = new LinkedList<>();
        Statement stm = connection.createStatement();
        ResultSet results = stm.executeQuery(
                "SELECT table_name FROM information_schema.tables " +
                        "WHERE table_schema = '" + dlbSchemaName + "';"
        );
        while (results.next()) {
            tables.add(results.getString(1));
        }
        return tables;
    }

    /**
     * Get a list of all the VDLs and the related tables.
     *
     * <!-- IMPORTANT IMPLEMENTATION NOTE: please keep package-private -->
     *
     * @param connection the {@link Connection} to the VDL database as root user
     * @param vdlDatabasePrefix the common {@link String} which is prepended to the name of all the MySQL VDLs.
     *
     * @return a list of names of all the VDL databases.
     *
     *
     * @throws SQLException
     * @throws SQLTimeoutException
     */
    public static LinkedList<String> listVDLTables(Connection connection, String vdlDatabasePrefix)
            throws SQLException, SQLTimeoutException
    {
        // check against SQL injection
        validateName(vdlDatabasePrefix);
        Statement stm = connection.createStatement();
        ResultSet results = stm.executeQuery(
                "SHOW DATABASES LIKE '" + vdlDatabasePrefix + "%';"
        );

        LinkedList<String> databases = new LinkedList<>();
        while (results.next()) {
            databases.add(results.getString(1));
        }

        return databases;
    }
}
