package io.chino.c3isp.buffermanager.restapi.types.requests;

import javax.validation.constraints.NotNull;

public enum Format {
//    STIX,
//    TEXT_PLAIN,
    CEF,
    CSV,
    EML,
    MODEL,
    PCAP,
    BINARY;

    public Format fromString(@NotNull String formatTypeName) {
        if (formatTypeName == null)
            throw new NullPointerException(
                    "Parameter formatTypeName cannot be 'null'"
            );

        switch (formatTypeName.toUpperCase().trim()) {
            case "EML":
                return EML;
            case "CEF":
                return CEF;
            case "MODEL":
                return MODEL;
            case "PCAP":
                return PCAP;
            case "BINARY":
                return BINARY;
            case "CSV":
                return CSV;

            default:
                throw new IllegalArgumentException(
                        "Unknown format type: '" + formatTypeName + "'"
                );
        }
    }
}
