package io.chino.c3isp.buffermanager.restapi.types.datalake;

import io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeFileSystemType;
import io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeType;
import io.chino.c3isp.buffermanager.restapi.types.requests.Format;
import io.chino.c3isp.buffermanager.restapi.types.requests.PrepareDataQuery;
import io.chino.c3isp.buffermanager.scheduled.CleanupTasks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.net.URI;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Contains instruction for the usage of the implementations of {@link DataLake}.
 * Add code to methods of this class in order to add new types of File System
 *
 */
public class DataLakeManager {
    /*
        PROTOCOL SCHEMES:
        The starting part of the URI before the ':'.
    */
    private final static String DEMO_PREFIX = DemoDataLake.URI_PROTOCOL,
            HDFS_PREFIX = HDFSDataLake.URI_PROTOCOL,
            SQL_PREFIX = MySQLDataLakeBuffer.URI_PROTOCOL,
            FS_PREFIX = FileSystemDataLake.URI_PROTOCOL;
    private final static String [] ALLOWED_SCHEMES = new String [] {
            DEMO_PREFIX,
            HDFS_PREFIX,
            SQL_PREFIX,
            FS_PREFIX
    };

    private final static Logger LOGGER = LoggerFactory.getLogger(DataLakeManager.class);

    /**
     * Parses a String and extract the protocol-specific part of the URI (i.e. the Scheme) as a String.
     *
     * @see URI#getScheme()
     * @param URIString A {@link String} containing a valid URI
     * @return the URI prefix if a known protocol is recognized. Otherwise returns {@code null}.
     */
    private static String extractURIProtocol(String URIString) {

        URI uri = URI.create(URIString);
        String URIScheme = uri.getScheme();

        if (URIScheme == null) {
            return "(no URI scheme found): valid URIs may start with " + Arrays.toString(ALLOWED_SCHEMES);
        }

        String actualProtocol = null;
        for (String scheme : ALLOWED_SCHEMES) {
            if (scheme.startsWith(URIScheme)) {
                actualProtocol = scheme;
                break;
            }
        }
        return actualProtocol;
    }

    /**
     * Parse the URI and create an instance of the right {@link DataLake} implementation
     * that suits the URI protocol.
     *
     * @param dataLakeURI the Data Lake URI
     * @return an instance of a subclass of {@link DataLake} with the specified URI
     */
    static DataLake getFromURI(String dataLakeURI) throws HttpClientErrorException {
        /*
         *  When adding new DataLake types, edit this method's body
         *  in order to return the correct implementation
         *  of io.chino.c3isp.buffermanager.restapi.types.datalake.DataLake
         */
        String protocolPrefix = extractURIProtocol(dataLakeURI);
        if (protocolPrefix == null) {
            protocolPrefix = "Invalid URI '" + dataLakeURI + "'"; // will be handled in the 'default' case below
        }
        DataLake dl;
        switch(protocolPrefix) {
            case FS_PREFIX:
                LOGGER.info("Accessing Data Lake on file system at: " + dataLakeURI);
                dl = new FileSystemDataLake();
                dl.setURIValue(dataLakeURI);
                return dl;
            case SQL_PREFIX:
                String usr = null, psw = null;
                try {
                    usr = getUrlParam(dataLakeURI, "usr");
                    psw = getUrlParam(dataLakeURI, "psw");
                } catch (ArrayIndexOutOfBoundsException e) {
                    if (usr == null) {
                        throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "missing parameter 'usr'");
                    } else
                    if (psw == null) {
                        throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "missing parameter 'psw'");
                    }
                }
                if (dataLakeURI.contains(new MySQLDataLakeBuffer("DataLakeManager.getFromURI").DLB_DATABASE_NAME))
                    dl = new MySQLDataLakeBuffer(usr, psw);
                else
                    dl = new MySQLVirtualDataLake(usr, psw, dataLakeURI);
                LOGGER.info("Connecting to Data Lake on MySQL database at URI " + dataLakeURI);

                dl.setURIValue(dataLakeURI);
                return dl;

            case HDFS_PREFIX:
                LOGGER.info("Accessing Data Lake on HDFS at: " + dataLakeURI);
                dl = new HDFSDataLake();
                dl.setURIValue(dataLakeURI);
                return dl;
            case DEMO_PREFIX:
                LOGGER.info("Creating instance of DemoDataLake for URI: " + dataLakeURI);
                return new DemoDataLake();
            default:
                // in case of errors, protocolPrefix contains an error msg
                throw new HttpClientErrorException(
                        HttpStatus.BAD_REQUEST,
                        "Bad Request - " + protocolPrefix + ". Connection to remote Data Lake failed."
                );
        }
    }

    public static String getUrlParam(String URIString, String paramName) throws ArrayIndexOutOfBoundsException {
        return URIString
                // cut away everything before the param name
                .split(paramName + "=")[1]
                // split before the next parameter and keep only the name.
                .split("&")[0];
    }

    /**
     * Parse the query and select the correct implementation of {@link DataLake} that will be used to create a DLB or VDL.
     * Call {@link DataLake#init()} on the new Instance.
     *
     * @param query the {@link PrepareDataQuery} that was sent to
     *      {@link  io.chino.c3isp.buffermanager.restapi.impl.BufferManagerController#prepareData(List, String, String, Format, DataLakeType, DataLakeFileSystemType) /prepareData}
     * @return the right implementation of {@link DataLake} according to the {@code query} parameters
     */
    static DataLake getFromQuery(PrepareDataQuery query) {
        DataLakeFileSystemType fsType = query.getFileSystem();
        switch (fsType) {
            case FS:
                LOGGER.info("using FileSystemDataLake");
                return new FileSystemDataLake();
            case MYSQL:
                if (query.getDataLake().equals(DataLakeType.DLB)) {
                    // create table in DB 'datalakebuffer'
                    MySQLDataLakeBuffer dlb = new MySQLDataLakeBuffer(query.getServiceName());
                    LOGGER.info("using MySQLDataLakeBuffer on table: " + MySQLDataLakeBuffer.getDBName());
                    return dlb;
                } else {
                    // create new DB
                    MySQLVirtualDataLake vdl = new MySQLVirtualDataLake(query.getServiceName());
                    LOGGER.info("using MySQLVirtualDataLake on database: " + MySQLVirtualDataLake.getDBName());
                    return vdl;
                }
            case HDFS:
                // create new DB
                LOGGER.info("using HDFSDataLake at " + HDFSDataLake.getDescription());
                return new HDFSDataLake();
            default:
                throw new IllegalArgumentException("Unsupported file system \"" + fsType.name() + "\"");
        }
    }

    /**
     * Return all the DataLake that match the provided type and file system.
     *
     * @param dataLakeType the {@link DataLakeType} of the Data Lakes to be filtered
     * @param fileSystemType the {@link DataLakeFileSystemType} of the Data Lakes to be filtered
     *
     * @return a {@link LinkedList} of containing the URI of all the {@link DataLake DataLakes} that match the provided criteria
     */
    public static LinkedList<String> getAll(DataLakeType dataLakeType, DataLakeFileSystemType fileSystemType) {
        switch(fileSystemType) {
            case MYSQL:
                return (dataLakeType == DataLakeType.DLB)
                        ? new MySQLDataLakeBuffer("BufferManager.getAll").all()
                        : new MySQLVirtualDataLake("BufferManager.getAll").all();
            case FS:
                return new FileSystemDataLake().all();
            case HDFS:
                return new HDFSDataLake().all();
            default:
                return new LinkedList<>();
        }
    }

    /**
     * Ensure the {@link DataLake} and all of its content is deleted. Used during the
     * {@link io.chino.c3isp.buffermanager.scheduled.CleanupTasks}
     *
     * @param dl the {@link DataLake} to delete
     */
    public static void cleanup(DataLake dl, CleanupTasks trigger) {
        if (dl instanceof MySQLVirtualDataLake)
            dl.forceDeleteDL(trigger);
        else
            dl.deleteDL();
    }
}
