package io.chino.c3isp.buffermanager.restapi.types.datalake;

import io.chino.c3isp.buffermanager.cfg.BaseConfigurer;
import io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeFileSystemType;
import io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeType;
import io.chino.c3isp.buffermanager.restapi.types.requests.Format;
import io.chino.c3isp.buffermanager.restapi.types.requests.PrepareDataQuery;
import io.chino.c3isp.buffermanager.scheduled.CleanupTasks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import static io.chino.c3isp.buffermanager.BufferManager.PATH_SEP;

/**
 * Generic interface used to create/populate/delete the different Data Lake types of C3ISP
 */
public abstract class DataLake {


    /* * * STATIC INITIALIZATION * * */
    static final String SUCCESS_MESSAGE = "Success.";
    static final String DPO_FILE_EXTENSION = ".dpo";

    /* * * INSTANCE VARIABLES * * */
    protected String dataLakeURI;
    DataLakeType dataLakeType = null;

    /* * * CONFIGURER * * */
    private BaseConfigurer instanceConfigurer;

    /* * * DATALAKE VALIDITY * * */
    /**
     * The maximum validity of the Data Lake before expiration, in milliseconds.<br>
     * <br>
     * See {@code data.XXX.delete-after} in application.properties, where XXX is the namespace of the Data Lake
     * implementation.
     * Default value is <b>6 hours</b>.
     */
    protected long maxValidityMillis;

    /**
     * Constructor called at startup by Spring {@link Autowired} processor.
     * Initializes this class configuration and then runs {@link #init()}
     *
     * @param instanceConfigurer an autowired {@link BaseConfigurer}
     */
    protected DataLake(BaseConfigurer instanceConfigurer) {
        // default: 6 hours, may be overwritten by subclass configurers
        this.maxValidityMillis = 6 * 3600 * 1000;
        this.instanceConfigurer = instanceConfigurer.init();
        // Copy configuration variables in the DataLake's attribute
        // (implemented by each subclass according to the required properties).
        this.initConfig(this.instanceConfigurer);
    }

    /**
     * Get a {@link String} with the URI that can be used to access this {@link DataLake} remotely.
     * @return a URI for this {@link DataLake}, which always ends with {@link io.chino.c3isp.buffermanager.BufferManager#PATH_SEP BufferManager.PATH_SEP}
     */
    public String getURI(){
        return dataLakeURI + (dataLakeURI.endsWith(PATH_SEP) ? "" : PATH_SEP);
    }

    /*
    *
    *  WORKFLOW METHODS: all abstract, implemented by subclasses of DataLake.
    *  They are used in the main /prepareData and /releaseData workflow and are
    *  directly called by the BufferManagerController.
    *
    */
    /**
     * Receive a {@link BaseConfigurer} and initialize the Data Lake's variables
     * accordingly. This method is called whenever a new Data Lake instance is created
     * through the {@link DataLakeManager}.
     *
     * @param instanceConfigurer an initialized subclass of {@link BaseConfigurer}
     */
    abstract protected void initConfig(BaseConfigurer instanceConfigurer);

    /**
     * Perform preliminary operations on the Data Lake (e.g. establish a connection with the host,
     * check existence, log connection status...). This method is invoked by the default constructor
     * of {@link DataLake}.
     *
     * @see #DataLake(BaseConfigurer)
     */
    abstract protected void init();

    /**
     * Create a new Data Lake and save the internal URI so that it can be read with {@link #getURI()}.
     * IMPLEMENTATION NOTE: It's good practice to use {@link #setURIValue(String URI)} to save the URI value
     * in the subclasses of DataLake.
     * @see #setURIValue(String)
     *
     * @return the Data Lake URI
     *
     * @throws RemoteDataLakeException an error occurred while communicating with the remote Data Lake host
     */
    abstract public String createDL() throws RemoteDataLakeException;

    /**
     * Write a new element in this {@link DataLake}
     *
     * @param fileName the name that will identify the DPO on the {@link DataLake} (suggested name: the DPO ID)
     * @param newElement the new object that needs to be added to the {@link DataLake}
     *
     * @return either an error message or the name that was assigned to the new DPO on the DataLake. (e.g. a file name on a FS,
     * or an attribute value of a SQL database)
     *
     * @throws RemoteDataLakeException an error occurred while communicating with the remote Data Lake host
     */
    abstract public String writeDPO(String fileName, String newElement) throws RemoteDataLakeException;

    /**
     * Delete this instance of the {@link DataLake} from the remote host. Implementations of this method should check
     * the validity of the URI before performing the delete operation
     *
     * @throws RemoteDataLakeException an error occurred while communicating with the remote Data Lake host.
     */
    abstract public void deleteDL() throws RemoteDataLakeException;

    /**
     * Perform final operations on the Data Lake, such as closing connections and freeing resources.
     * This method should be called when no other operations are needed for that particular instance of the
     * {@link DataLake}, since after the call returns the Data Lake may be unusable.
     */
    abstract public void disconnect() throws Exception;

    /*
     *
     *   UTILITY METHODS: both abstract and not, those methods perform operations
     *   on an instance of Data Lake, mainly reading / updating class attributes,
     *   metadata, configuration properties and other information.
     *
     */

    void forceDeleteDL(CleanupTasks trigger) {
        // This method may only be called by an instance of CleanupTasks,
        // otherwise it will do nothing
        if (trigger == null) {
            return;
        }
        deleteDL();
    }

    /**
     * Get a {@link Date} representing the moment this {@link DataLake} was created.
     * Should be used to compute the outcome of {@link #getExpirationDate()} and {@link #hasExpired()}.
     *
     * @return the {@link Date} when this Data Lake was created.
     */
    abstract public Date getCreationDate() throws Exception;

    /**
     * Compute how long ago this Data Lake was created (using {@link #getCreationDate()}) and check it hasn't
     * expired.<br>
     * The policies to decide when a DL expires may vary between implementations.
     *
     * @return {@code true} if this {@link DataLake} has reached the maximum time to live and can be safely deleted,
     * false otherwise.
     */
    public boolean hasExpired() throws Exception {
        Date now = new Date();
        return getExpirationDate().before(now);
    }

    /**
     * Get a {@link Date} representing the moment this {@link DataLake} is considered expired.
     * Should be used to compute the outcome of {@link #hasExpired()}.
     *
     * @return the {@link Date} when this Data Lake expires.
     */
    public Date getExpirationDate() throws Exception {
        long creationMillis = getCreationDate().toInstant().toEpochMilli();
        return new Date(creationMillis + maxValidityMillis);
    }

    /**
     * @return this DataLake's {@link DataLakeType type}
     */
    public DataLakeType getDataLakeType() {
        return dataLakeType;
    }

    /**
     * Set the value of this {@link DataLake}'s URI. Should be used by {@link #createDL()}
     * after a successful creation of a Data Lake to store the URI in this {@link DataLake}.
     *
     * @param thisURI the value to store as {@link #dataLakeURI} for this {@link DataLake}
     */
    protected void setURIValue(String thisURI) {
        this.dataLakeURI = thisURI;
    }

    /**
     * Get a unique {@link java.util.UUID} as a String.
     *
     * @return a String containing a type 4 UUID, generated with {@link java.util.UUID#randomUUID()}
     */
    final String getUUID() {
        return UUID.randomUUID().toString();
    }

    /**
     * Return the property with the given name
     *
     * @param name the property name
     * @return the property with the given name, or null if any.
     *
     * @see BaseConfigurer#getProperty(String)
     */
    String getConfigProperty(String name) {
        return instanceConfigurer.getProperty(name);
    }

    /**
     * Routine that collects all the existing {@link DataLake DataLakes} of a given implementation. <br>
     * This method is never invoked directly: it is used only by
     * {@link DataLakeManager#getAll(DataLakeType, DataLakeFileSystemType)}.
     *
     * <!-- IMPORTANT IMPLEMENTATION NOTE
     *      This method MUST always be kept package-private and never be used in Controllers
     *      and its output must not be used in logs or reports.
     *      The returned List may contain root credentials (e.g. of the MySQL database)
     *      that must NOT be leaked.
     * -->
     *
     * @return a {@link LinkedList LinkedList}&lt;{@link String}&gt; with the URIs of all the existing
     * DataLakes for the current implementation.
     */
    abstract LinkedList<String> all() throws RemoteDataLakeException;

    /*
    *
    *   STATIC DATA LAKE METHODS: they wrap the methods of the DataLakeManager and are
    *   used to create an instance of a DataLake without knowing its DataLakeType
    *   or DataLakeFileSystemType in advance.
    *
    */

    /**
     * Get an instance of {@link DataLake} that connects to an existing remote Data Lake via a URI.
     *
     * @param URI the URI of a remote Data Lake
     * @return a subclass of {@link DataLake} that can communicate with the remote DL.
     * @throws RemoteDataLakeException (400) The URI doesn't identify a valid directory
     */
    public static DataLake fromURI(String URI) throws HttpClientErrorException {
        return DataLakeManager.getFromURI(URI);
    }

    /**
     * Parse a {@link PrepareDataQuery} and create a new remote Data Lake that satisfies the request.
     * Then creates a subclass of {@link DataLake} that can communicate with the remote DL.
     *
     * @param query a {@link PrepareDataQuery} as received by the
     *              {@link io.chino.c3isp.buffermanager.restapi.impl.BufferManagerController#prepareData(List, String, String, Format, DataLakeType, DataLakeFileSystemType) /prepareData}
     *              endpoint
     * @return a subclass of {@link DataLake} that can communicate with the new remote DL.
     */
    public static DataLake fromQuery(PrepareDataQuery query) {
        try {
            return DataLakeManager.getFromQuery(query);
        } catch (IllegalArgumentException iae) {
            throw new RemoteDataLakeException(HttpStatus.BAD_REQUEST, iae.getMessage());
        }
    }
}
