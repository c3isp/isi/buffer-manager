package io.chino.c3isp.buffermanager.restapi.types.datalake;

import io.chino.c3isp.buffermanager.cfg.BaseConfigurer;
import io.chino.c3isp.buffermanager.cfg.MySQLConfig;
import io.chino.c3isp.buffermanager.restapi.types.datalake.mysql.Queries;
import io.chino.c3isp.buffermanager.restapi.types.datalake.mysql.SecureCredentialsGenerator;
import io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeType;
import io.chino.c3isp.buffermanager.scheduled.CleanupTasks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

import java.sql.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeType.DLB;

@Configuration
public class MySQLDataLakeBuffer extends DataLake {

    private static Logger LOGGER = LoggerFactory.getLogger(MySQLDataLakeBuffer.class);

    /* BASE CONFIGURATION */
    protected static final String UUID_SEPARATOR_CHAR = "_";
    protected static final String URI_PROTOCOL = "jdbc:mysql:";

    /**
     * This workaround is required to avoid "Unrecognized time zone value" error
     */
    protected static String DB_TIMEZONE_FIX = "?useLegacyDatetimeCode=false&serverTimezone=Europe/Rome";

    /**
     * The name of this Data Lake
     */
    private final String name;

    /**
     * Username of the user that has access rights over this Data Lake.
     * The user is created in {@link #createDL()}.
     *
     * @see #dataLakePassword
     */
    protected String dataLakeUser;

    /**
     * Password of the user that has access rights over this Data Lake.
     * The user is created in {@link #createDL()}.
     *
     @see #dataLakeUser
     */
    protected String dataLakePassword;

    /* CONFIGURATION LOADED FROM application.properties */
    /**
     * MySQL db configuration, initialized in package-private constructor
     */
    static BaseConfigurer classDefaultConfigurer;

    /**
     * Name of the table which contains DLBs.<br>
     * <br>
     * See {@code data.mysql.dlb.database} in application.properties
     */
    public String DLB_DATABASE_NAME = getConfigProperty("database");

    /**
     * Only users from this host can access the DLBs. If you don't want to restrict access,
     * specify {@code %} as the {@code data.mysql.dlb.host} in application.properties
     */
    private String ALLOWED_HOST = getConfigProperty("host");

    /**
     * The URL to the MySQL instance.<br>
     * <br>
     * See {@code data.mysql.dlb.url} in application.properties
     */
    protected String DB_SERVER_BASE_URL = getConfigProperty("url");

    /**
     * Full URL to MySQL database containing DLBs.
     *
     * @see #DB_SERVER_BASE_URL
     * @see #DLB_DATABASE_NAME
     * @see #DB_TIMEZONE_FIX
     */
    private String DB_URL = DB_SERVER_BASE_URL + DLB_DATABASE_NAME
            + DB_TIMEZONE_FIX;


    /* database root credentials */
    /**
     * Database root username, used to access the db and create users/tables/databases<br>
     * <br>
     * See {@code data.mysql.dlb.user.name} in application.properties
     */
    protected String rootUser = getConfigProperty("user.name");

    /**
     * Database root password, goes along with {@link #rootUser}.<br>
     * <br>
     * See {@code data.mysql.dlb.user.password} in application.properties
     */
    protected String rootPassword = getConfigProperty("user.password");


    /**
     * JDBC CONNECTOR
     * Initialized in {@link #init()}, connects to the Data Lake instance.
     */
    protected Connection dbConnection;


    /**
     * INJECTION-SAFE QUERY GENERATOR
     * Initialized in {@link #createDL()} to create the table and
     * whenever changing the Data Lake URI with {@link #setURIValue(String)}.
     * Contains the SQL code to generate the tables for Data Lake
     */
    private Queries queries = null;

    /* CONSTRUCTORS */
    /**
     * @param classDefaultConfig an autowired {@link BaseConfigurer}
     */
    @Autowired
    MySQLDataLakeBuffer(MySQLConfig classDefaultConfig) {
        super(classDefaultConfig.forType(DLB));
        if (dataLakeType == null) {
            // dataLakeType was not initialized by MySQLVirtualDataLake
            dataLakeType = DLB;
        }
        this.name = dataLakeType.toString() + " (created with @Autowired)";
        // when Spring initialization occurs, store the configurer in the static pool
        // so it can be reused when this class is created using 'new'
        classDefaultConfigurer = classDefaultConfig.forType(DLB).init();
        // This is used by this DataLake and its subclass MySQLVirtualDataLake
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            LOGGER.debug("Loaded MySQL driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("MySQL Data Lake: JDBC Driver not found");
        }
    }

    /**
     * Create a new {@link MySQLDataLakeBuffer} instance on the configured database.
     * This constructor generates a new username-password pair that will be used during
     * {@link #createDL()} to make the Data Lake accessible.
     *
     * @param name a name that will be assigned to this Data Lake instance. Can be the name of the
     *             analytic that requested the Data Lake or a human-readable name.
     */
    public MySQLDataLakeBuffer(String name) {
        super(classDefaultConfigurer);
        init();
        if (dataLakeType == null) {
            // dataLakeType was not initialized by MySQLVirtualDataLake
            dataLakeType = DLB;
        }
        this.name = dataLakeType.toString() + ": " + name;
        // analytic-specific database user name - max 16 chars
        this.dataLakeUser = "u_" + getUUID().replaceAll("-", "").substring(0, 14);
        // random 8-chars database password of MEDIUM strength
        this.dataLakePassword = generateMediumPassword();
    }

    /**
     * Set up this {@link MySQLDataLakeBuffer} to login using the provided credentials.
     *
     * @param username the Data Lake user's username
     * @param password the Data Lake user's password
     */
    protected MySQLDataLakeBuffer(String username, String password) {
        super(classDefaultConfigurer);
        init();
        if (dataLakeType == null) {
            dataLakeType = DLB;
        }
        name = getDataLakeType().toString() + " for user " + username;
        this.dataLakeUser = username;
        this.dataLakePassword = password;
    }

    /* SETTERS AND GETTERS */

    protected void initConfig(BaseConfigurer instanceConfigurer) {
        DLB_DATABASE_NAME = getConfigProperty("database");
        ALLOWED_HOST = getConfigProperty("host");
        DB_SERVER_BASE_URL = getConfigProperty("url");

        DB_URL = DB_SERVER_BASE_URL + DLB_DATABASE_NAME + DB_TIMEZONE_FIX;

        rootUser = getConfigProperty("user.name");
        rootPassword = getConfigProperty("user.password");

        String tmp = getConfigProperty("expires-in");
        try {
            if (tmp != null) super.maxValidityMillis = Long.valueOf(tmp);
        } catch (NumberFormatException ignored) { /* not a number. use default value (6hrs) */ }
    }

    /**
     * Get the name of the container (table or database) of this Data Lake.
     */
    public static String getDBName() {
        return classDefaultConfigurer.getProperty("url") + classDefaultConfigurer.getProperty("database");
    }

    /**
     * Get the allowed hostname. The database only accepts incoming connections from users that
     * come from this hostname.<br>
     * <br>
     * The value {@code %} is a wildcard that allows connections from any host.
     *
     * @return the host name as a String.
     */
    public static String getAllowedHost() {
        return new MySQLDataLakeBuffer("getAllowedHost").ALLOWED_HOST;
    }

    @Override
    public String getURI() {
        return dataLakeURI.replace(DB_TIMEZONE_FIX, "");
    }

    @Override
    public void setURIValue(String thisURI) {
        super.setURIValue(thisURI);
        queries = new Queries(dbConnection, getTableFromUri(thisURI));
    }

    @Override
    public DataLakeType getDataLakeType() {
        return DLB;
    }

    /**
     * Create a valid password of MEDIUM complexity (as defined by MySQL password plugin)
     * https://dev.mysql.com/doc/refman/5.6/en/validate-password-options-variables.html#sysvar_validate_password_policy
     */
    private String generateMediumPassword() {
        return new SecureCredentialsGenerator()
                .withConstraints(1, 0, 0, 1, 1, 8)
                .getPassword();
    }

    @Override
    protected void init() {
        try {
            initDLBRoot();
            dbConnection = DriverManager.getConnection(
                    DB_URL,
                    rootUser,
                    rootPassword
            );
        } catch (SQLTimeoutException e) {
            LOGGER.error("Connection timeout", e);
            throw new RemoteDataLakeException(HttpStatus.GATEWAY_TIMEOUT, "Unable to connect to remote database '" +
                    DB_URL +
                    "'. Reason: request timeout.");
        } catch (SQLException e) {
            LOGGER.error("Connection failure: " + e.getMessage());
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE, "Unable to connect to remote database '" +
                    DB_URL +
                    "'. Check server logs for more information about the error.");
        } catch (Exception e) {
            LOGGER.error("Connection failure: " + e.getMessage());
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to connect to remote database '" +
                    DB_URL +
                    "'. Check server logs for more information about the error.");
        }
    }

    private void initDLBRoot() throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        Connection c = DriverManager.getConnection(
                DB_SERVER_BASE_URL + DB_TIMEZONE_FIX,
                rootUser,
                rootPassword
        );

        String INIT_DLB_ROOT = "CREATE DATABASE IF NOT EXISTS " + DLB_DATABASE_NAME + ";";
        c.createStatement().executeUpdate(INIT_DLB_ROOT);
    }

    @Override
    public String createDL() throws RemoteDataLakeException {
        String tableName = "table_" + getUUID().replaceAll("-", UUID_SEPARATOR_CHAR);

        String action = "<n.d.>";
        try {
            action = "create table";
            queries = Queries.createTable(dbConnection, tableName);

            action = "create user";
            queries.createUserWithAllPermissions(dbConnection, getDataLakeType(), dataLakeUser, dataLakePassword);
        } catch (SQLTimeoutException e) {
            LOGGER.error("error (timeout)", e);
            throw new RemoteDataLakeException(HttpStatus.GATEWAY_TIMEOUT, "Remote SQL Data Lake took too long to respond.");
        } catch (SQLSyntaxErrorException e) {
            LOGGER.error("error (syntax)", e);
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR, "There was an error with the server. Please check the logs for more information or contact an admin.");
        } catch (SQLException e) {
            try {
                queries.dropTable();
            } catch (SQLException nestedException) {
                e.setNextException(nestedException);
            }
            LOGGER.error("error (SQL) during ' " + action + "' action", e);

            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE, "Unable to create Data Lake on remote SQL server");
        }

        dataLakeURI = buildUriFromTable(tableName);

        return dataLakeURI;
    }

    @Override
    public String writeDPO(String fileName, String newElement) throws RemoteDataLakeException {
        String table = getTableFromUri(dataLakeURI);
        if (!exists(dbConnection, table) || queries == null) {
            LOGGER.error("Data Lake not found: did you invoke 'init' and 'createDL'?");
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR, "Wrong initialization. Check server logs or contact an admin for more information.");
        }

        String action = "verify table name syntax";
        try {
            // write DPO
            PreparedStatement insertQuery = queries.insertDPO(
                    newElement,
                    fileName
            );
            insertQuery.executeUpdate();
        } catch (SQLTimeoutException e) {
            LOGGER.error("error (timeout)", e);
            throw new RemoteDataLakeException(HttpStatus.GATEWAY_TIMEOUT, "Remote SQL Data Lake took too long to respond.");
        } catch (SQLSyntaxErrorException e) {
            LOGGER.error("error (syntax)", e);
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR, "There was an error with the server. Please check the logs for more information or contact an admin.");
        } catch (SQLException e) {
            LOGGER.error("error (SQL) during ' " + action + "' action", e);
            String cause = "Unable to write on remote Data Lake.";
            if (e.getErrorCode() == 1062) { // duplicate entry for the same UNIQUE attribute (DPO_id)
                cause = "A DPO with ID '" + fileName + "' already exists in the Data Lake.";
            }
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE, cause);
        }

        LOGGER.info("Written: '" + fileName + "'");
        return queries.getDpoSelectQueryString(fileName);
    }

    @Override
    public void deleteDL() throws RemoteDataLakeException {
        String table = getTableFromUri(dataLakeURI);
        if (!exists(dbConnection, table))
            throw new RemoteDataLakeException(HttpStatus.NOT_FOUND,
                    "Table '" + table + "' does not exist, or the Data Lake may be expired.");

        String action = "verify table name syntax";
        try {
            action = "delete table";
            queries.dropTable();
            action = "delete user";
            Queries.dropUser(
                    dbConnection,
                    DLB,
                    DataLakeManager.getUrlParam(dataLakeURI, "usr"),
                    getConfigProperty("host")
            );
        } catch (SQLTimeoutException e) {
            LOGGER.error("error (timeout)", e);
            throw new RemoteDataLakeException(HttpStatus.GATEWAY_TIMEOUT,
                    "Remote SQL Data Lake took too long to respond.");
        } catch (SQLSyntaxErrorException e) {
            LOGGER.error("error (syntax)", e);
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "There was an error with the server. " +
                            "Please check the logs for more information or contact an admin.");
        } catch (SQLException e) {
            LOGGER.error("error (SQL) during '" + action + "' action", e);
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE,
                    "Unable to delete Data Lake table '" + table + "' from remote SQL server");
        }

    }

    @Override
    public void disconnect() throws Exception {
        String problem = "Connection interrupted";
        try {
            dbConnection.close();
            problem = "Pending SQL statements";
            queries.done();
            LOGGER.info("MySQLDataLakeBuffer instance was released.");
        } catch (SQLException e) {
            LOGGER.error("Error in MySQLDataLakeBuffer: " + problem, e);
            throw new Exception("Unable to reach remote MySQL database", e);
        }
    }

    @Override
    void forceDeleteDL(CleanupTasks trigger) throws RemoteDataLakeException {
        // This method may only be called by an instance of CleanupTasks,
        // otherwise it will do nothing
        if (trigger == null) {
            LOGGER.warn("MySQLVirtualDataLake.forceDeleteDL was called by an invalid caller.");
            return;
        }
        try {
            queries.dropTable();
        } catch (SQLTimeoutException e) {
            LOGGER.error("error (timeout)", e);
            throw new RemoteDataLakeException(HttpStatus.GATEWAY_TIMEOUT,
                    "Remote SQL Data Lake took too long to respond."
            );
        } catch (SQLSyntaxErrorException e) {
            LOGGER.error("error (syntax)", e);
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR, "There was an error with the server. " +
                    "Please check the logs for more information or contact an admin."
            );
        } catch (SQLException e) {
            LOGGER.error("Unable to get list of DataLakes from remote SQL server", e);
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE, "Unable to force delete DataLakes from " +
                    "remote SQL server");
        }
    }

    @Override
    public Date getCreationDate() throws Exception {
        String table = getTableFromUri(dataLakeURI);
        if (!exists(dbConnection, table))
            throw new RemoteDataLakeException(HttpStatus.NOT_FOUND,
                    "This Data Lake does not exist, or it may be expired.");

        String action = "get creation Date";
        try {
            Queries.MySQLMetadata md = Queries.getMetadata(dbConnection, table, getDataLakeType());
            return md.getCreationDate();
        } catch (SQLTimeoutException e) {
            LOGGER.error("error (timeout)", e);
            throw new RemoteDataLakeException(HttpStatus.GATEWAY_TIMEOUT,
                    "Remote SQL Data Lake took too long to respond.");
        } catch (SQLSyntaxErrorException e) {
            LOGGER.error("error (syntax)", e);
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "There was an error with the server. " +
                    "Please check the logs for more information or contact an admin.");
        } catch (SQLException e) {
            LOGGER.error("error (SQL) during ' " + action + "' action", e);
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE,
                    "Unable to read Data Lake creation date '" + table + "' from remote SQL server");
        }
    }

    public Date getLastUpdate() {
        String table = getTableFromUri(dataLakeURI);
        if (!exists(dbConnection, table))
            throw new RemoteDataLakeException(HttpStatus.NOT_FOUND,
                    "This Data Lake does not exist, or it may be expired.");

        String action = "get last update Date";
        try {
            Queries.MySQLMetadata md = Queries.getMetadata(dbConnection, table, getDataLakeType());
            return md.getLastUpdateDate();
        } catch (SQLTimeoutException e) {
            LOGGER.error("error (timeout)", e);
            throw new RemoteDataLakeException(HttpStatus.GATEWAY_TIMEOUT, "Remote SQL Data Lake took too long to respond.");
        } catch (SQLSyntaxErrorException e) {
            LOGGER.error("error (syntax)", e);
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR, "There was an error with the server. Please check the logs for more information or contact an admin.");
        } catch (SQLException e) {
            LOGGER.error("error (SQL) during ' " + action + "' action", e);
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE, "Unable to read Data Lake last update date for table '" + table + "'");
        }
    }

    /**
     * Check that the MySQL table related to this instance of Data Lake exists.
     *
     * @return {@code true} only if the database table was both created and bound to this instance of
     *                   {@link MySQLDataLakeBuffer}.
     */
    public boolean exists() {
        if (dataLakeURI == null)
            return false;

        return exists(dbConnection, getTableFromUri(dataLakeURI));
    }

    /**
     * Verify the existence of a SQL table
     *
     * @param tabName the name of the table to check
     *
     * @return {@code true} if the table exists in the database, {@code false} otherwise.
     *
     * @throws RemoteDataLakeException database access error (caused by {@link SQLException})
     */
    protected static boolean exists(Connection connection, String tabName) throws RemoteDataLakeException {
        try {
            return connection
                    .getMetaData()
                    .getTables(null, null, tabName, null)
                    .next();
        } catch (SQLException e) {
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE, "Unable to connect to remote database.");
        }
    }

    /**
     * Get the name of this dataLake's table
     *
     * @return the table's name, or just the name of the DLB database if this {@link MySQLDataLakeBuffer} has no URI set.
     */
    public String getDataLakeName() {
        String name = DLB_DATABASE_NAME;
        return name;
    }

    /**
     * Return a URI that uniquely identifies this Data Lake and points to a specific table in the database.
     *
     * @param tableName the name of the table in the MySQL {@link DataLake}
     *
     * @return a URI that identifies the specified table
     */
    public String buildUriFromTable(String tableName) {
        return DB_URL
                + "&usr=" + dataLakeUser
                + "&psw=" + dataLakePassword
                + "&table=" + tableName;
    }

    /**
     * Extract the name of a table from a URI
     *
     * @param URIString String representation of the URI
     *
     * @return the name of the table, or {@code null} if the URI does not contain the parameter "table="
     */
    public static String getTableFromUri(String URIString) {
        if (!URIString.contains("table="))
            return null;

        String tableName = URIString
                // cut away everything before the table name
                .split("table=")[1]
                // split before the next parameter and keep only the name.
                .split("&")[0];

        return tableName;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    LinkedList<String> all() throws RemoteDataLakeException {
        LinkedList<String> names = new LinkedList<>();
        try {
            // read table names
            List<String> tables = Queries.listDLBTables(dbConnection, DLB_DATABASE_NAME);
            // set root credentials to be used in buildUriFromTable()
            this.dataLakeUser = rootUser;
            this.dataLakePassword = rootPassword;
            // populate list with the URIs built from the table names
            for (String tableName : tables) {
                names.add(buildUriFromTable(tableName));
            }
        } catch (SQLTimeoutException e) {
            LOGGER.error("error (timeout)", e);
            throw new RemoteDataLakeException(HttpStatus.GATEWAY_TIMEOUT,
                    "Remote SQL Data Lake took too long to respond."
            );
        } catch (SQLSyntaxErrorException e) {
            LOGGER.error("error (syntax)", e);
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR, "There was an error with the server. " +
                    "Please check the logs for more information or contact an admin."
            );
        } catch (SQLException e) {
            LOGGER.error("Unable to get list of DataLakes from remote SQL server", e);
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE, "Unable to get list of DataLakes from " +
                    "remote SQL server");
        }

        return names;
    }

}
