
package io.chino.c3isp.buffermanager.restapi.types.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeFileSystemType;
import io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeType;
import io.chino.c3isp.buffermanager.restapi.types.requests.Format;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.ALWAYS;

/**
 * The result of an API call to
 * {@link io.chino.c3isp.buffermanager.restapi.impl.BufferManagerController#prepareData(List, String, String, Format, DataLakeType, DataLakeFileSystemType) /v1/prepareData},
 * which contains the URI of the Data Lake that was created.
 * It also contains a list of {@link DPOWriteOutcome} objects with the results of the DPO read API calls
 * to the ISI.
 *
 * @see DPOWriteOutcome
 *
 * @author Andrea Arighi [andrea@chino.io]
 */
@ApiModel(value = "response",
        description = "Contains the URI of the new Data Lake as well as information about any errors " +
                "encountered during the retrieval of the DPO from the ISI."
)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "datalake",
    "data"
})
public class PrepareDataResponse implements Serializable
{

    @ApiModelProperty(value = "Contains information about the new Data Lake, such as the DL URI and a message with the outcome of the " +
            "DL creation operation. In case of error, the URI wil be 'null'", required = true)
    @JsonInclude(ALWAYS)
    @JsonProperty("datalake")
    private DataLakeStatus DLStatus;

    @ApiModelProperty(value = "The results of the DPO read operations from the ISI. May be empty", required = true, allowEmptyValue = true)
    @JsonInclude(ALWAYS)
    @JsonProperty("data")
    private List<DPOWriteOutcome> outcomes = new LinkedList<>();

    @JsonIgnore
    private boolean isValid = true;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PrepareDataResponse() {
    }

    /**
     * Create a new {@link PrepareDataResponse} to be sent by the
     * {@link io.chino.c3isp.buffermanager.restapi.impl.BufferManagerController#prepareData(List, String, String, Format, DataLakeType, DataLakeFileSystemType) prepareData}
     *
     * @param status the URI of the DLB/VDL
     */
    public PrepareDataResponse(DataLakeStatus status) {
        super();
        this.DLStatus = status;
    }

    public PrepareDataResponse(DataLakeStatus dataLakeStatus, boolean b) {
        this(dataLakeStatus);
        isValid = b;
    }

    public String extractDataLakeURI() {
        return DLStatus.getURI();
    }

    public String extractDataLakeStatusMessage() {
        return DLStatus.getMessage();
    }

    @JsonIgnore
    public boolean isValid() {
        return isValid;
    }

    @JsonProperty("datalake")
    private DataLakeStatus getDLStatus() {
        return DLStatus;
    }

    @JsonProperty("datalake")
    private void setDLStatus(DataLakeStatus dls) {
        this.DLStatus = dls;
    }

    @JsonProperty("data")
    public List<DPOWriteOutcome> getDPOList() {
        return outcomes;
    }

    @JsonProperty("data")
    public void setDPOList(List<DPOWriteOutcome> outcomes) {
        this.outcomes = outcomes;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof PrepareDataResponse)) {
            return false;
        }

        PrepareDataResponse rhs = ((PrepareDataResponse) other);

        if (this.outcomes != rhs.outcomes && this.outcomes.size() != rhs.outcomes.size())
            return false;

        EqualsBuilder conditions = new EqualsBuilder().append(this.DLStatus, rhs.DLStatus);

        HashSet<DPOWriteOutcome> ourErrors = new HashSet<>(this.outcomes),
                theirErrors = new HashSet<>(rhs.outcomes);

        return conditions.append(ourErrors, theirErrors).isEquals();
    }
}
