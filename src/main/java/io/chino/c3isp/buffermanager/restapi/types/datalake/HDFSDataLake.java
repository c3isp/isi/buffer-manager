package io.chino.c3isp.buffermanager.restapi.types.datalake;

import io.chino.c3isp.buffermanager.cfg.BaseConfigurer;
import io.chino.c3isp.buffermanager.cfg.HDFSConfig;
import io.chino.c3isp.buffermanager.restapi.types.datalake.hdfs.Impala;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.fs.permission.FsPermission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import java.io.*;
import java.net.URI;
import java.sql.*;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.UUID;

/**
 *  Implementation of {@link DataLake} over a stack of HDFS + Impala
 */
// use full path of @Configuration to avoid conflicts with 'org.apache.hadoop.conf.Configuration'
@org.springframework.context.annotation.Configuration
public class HDFSDataLake extends DataLake {
    public static final String URI_PROTOCOL = "hdfs://";
    private static final Logger LOGGER = LoggerFactory.getLogger(HDFSDataLake.class);
    static HDFSConfig classDefaultConfigurer;

    // Impala configuration
    private String impalaDatabaseName = getConfigProperty("impala.database");
    private String impalaUri = getConfigProperty("impala.uri");  // this value is set in initConfig
    private Connection impalaConnection;  // the connection is established in init()

    // HDFS configuration
    private FileSystem hdfsRoot;
    private static final Configuration hadoopConfiguration = new Configuration();
    private String hdfsUri = getConfigProperty("uri");
    private String hdfsWorkDir = getConfigProperty("root");
    private String hdfsUsername = getConfigProperty("user.name");
    private static FsPermission dataLakeFolderPermissions = null;

    // Data Lake
    private String dataLakeFolderName;
    private Path dataLakeFolderPath;
    private FileSystem dataLake = null;


    /**
     * Name of the pool file, the one which contains all data of the Data Lake
     */
    /// POOL
    private static final String POOL_FILE_NAME = "pool", POOL_FILE_EXT = ".csv";
    /**
     * Data pool file {@link Path}.
     * In the data pool file all the data written to the Data Lake will be aggregated in order to be loaded in the
     * Impala table.
     */
    /// POOL
    // must not be static/final: every instance replaces this with the path to its own pool file.
    private Path poolFilePath = new Path(POOL_FILE_NAME + POOL_FILE_EXT);
    /**
     * The number of lines in the Pool file for this Data Lake. If the value is <0, the count has not been performed
     * and must be updated using {@link #getLinesCount()}.
     * <br>
     * <b>Only method {@link #cleanData(String)} should access the value of this field directly.</b>
     */
    /// POOL
    int poolSize = -1;

    /**
     * The name of the column that contains the row index.
     */
    /// POOL
    private final String indexColumn = "row_count";
    /**
     * The names of columns that are allowed in the CTI file.
     */
    /// POOL
    private final String[] columns = new String[] {
            indexColumn,  // the index is always appended to each row
            "header_version", "header_deviceVendor", "header_deviceProduct", "header_deviceVersion",
            "header_deviceEventClassId", "header_name", "header_severity", "extension_rt", "extension_start",
            "extension_dvchost", "extension_reason", "extension_Tags", "extension_act", "extension_Rank",
            "extension_deviceDirection", "extension_deviceInboundInterface", "extension_TrendMicroDsFrameType",
            "extension_proto", "extension_TCP Flags", "extension_src", "extension_smac", "extension_spt",
            "extension_dst", "extension_dmac", "extension_dpt", "extension_out", "extension_cnt", "extension_end",
            "extension_Flow", "extension_Status", "extension_msg", "extension_Data_Flags", "extension_Data_Index",
            "extension_TrendMicroDsPacketData", "extension_cat"
    };

    @Autowired
    HDFSDataLake(HDFSConfig classDefaultConfig) {
        super(classDefaultConfig);
        classDefaultConfigurer = classDefaultConfig;
        try {
            Class.forName("com.cloudera.impala.jdbc41.Driver");
            LOGGER.debug("Loaded Impala driver");
        } catch (ClassNotFoundException ignored) {
            throw new RuntimeException("HDFS Data Lake: JDBC Driver not found");
        }
        init();
    }

    public HDFSDataLake() {
        super(classDefaultConfigurer);
        init();
    }

    public static String getDescription() {
        HDFSDataLake tmpDl = new HDFSDataLake();
        return String.format("%s with Impala at %s%s",
                tmpDl.hdfsUri, tmpDl.impalaUri, tmpDl.impalaDatabaseName);
    }

    @Override
    protected void initConfig(BaseConfigurer instanceConfigurer) {
        try {
            dataLakeFolderPermissions = new FsPermission(getConfigProperty("chmod"));
            dataLakeFolderPermissions.validateObject();
        } catch (Exception e) {
            LOGGER.error("Property 'data.hdfs.chmod' is not set, " +
                    "or its value is not a valid Linux file system permission.");
            System.exit(1);
        }
        // init HDFS configuration
        hdfsUri = getConfigProperty("uri");
        hdfsWorkDir = "/" + getConfigProperty("root");
        hdfsUsername = getConfigProperty("user.name");
        // Set FileSystem URI
        hadoopConfiguration.set("fs.defaultFS", hdfsUri);
        // Let HDFS append data to files
        hadoopConfiguration.setBoolean("dfs.support.append", true);
        // Because of Maven
        hadoopConfiguration.set("fs.hdfs.impl", org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
        hadoopConfiguration.set("fs.file.impl", org.apache.hadoop.fs.LocalFileSystem.class.getName());
        // prevent HDFS to look for more than 1 node
        hadoopConfiguration.setInt("dfs.replication", 1);
        hadoopConfiguration.set("dfs.client.block.write.replace-datanode-on-failure.policy", "NEVER");

        // init Impala configuration
        impalaUri = getConfigProperty("impala.uri");
        impalaDatabaseName = getConfigProperty("impala.database");

        String tmp = getConfigProperty("expires-in");
        try {
            if (tmp != null) super.maxValidityMillis = Long.valueOf(tmp);
        } catch (NumberFormatException ignored) { /* not a number. use default value (6hrs) */ }
    }

    @Override
    protected void init() {
        // Check connection to HDFS
        try {
            if (hdfsUsername == null)
                throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR,
                        "Failed to load HDFS user information. Please contact an administrator");

            // Set HADOOP user
            System.setProperty("HADOOP_USER_NAME", hdfsUsername);
            System.setProperty("hadoop.home.dir", hdfsWorkDir);
            // Connect to remote Data Lake
            hdfsRoot = FileSystem.get(URI.create(hdfsUri), hadoopConfiguration, hdfsUsername);
            Path workdir = new Path(hdfsRoot.getWorkingDirectory(), hdfsWorkDir);
            if (!hdfsRoot.exists(workdir)) {
                hdfsRoot.mkdirs(workdir);
                LOGGER.info("Created HDFS folder: " + hdfsWorkDir);
            }
            hdfsRoot.setWorkingDirectory(workdir);

            LOGGER.info("Connection to '{}' successful with username = '{}'",
                    hdfsUri, hdfsUsername);
            LOGGER.debug("Current HDFS folder:" + hdfsWorkDir);
        } catch (IOException | InterruptedException ex) {
            LOGGER.error("Failed to connect to HDFS at URI: " + getConfigProperty("uri") +
                    " ~ folder: " + getConfigProperty("root"), ex);
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE,
                    "Failed to connect to Data Lake root folder.");
        }

        // Test connection to Impala
        String username = "unknown";
        try {
            impalaConnection = DriverManager.getConnection(impalaUri + impalaDatabaseName + "/;auth=noSasl;");
            // read current user's name
            try {
                String usernameBuffer = impalaConnection.getMetaData().getUserName();
                if (usernameBuffer == null)
                    username += " (null value)";
                else
                    username = String.format("= '%s'", usernameBuffer);
            } catch (SQLException e) {
                username += " (SQL Exception)";
            }

            impalaConnection.setSchema(impalaDatabaseName);
            Impala.initDatabase(impalaConnection, impalaDatabaseName);
            LOGGER.info("Connection to '{}' successful with principal_name {}",
                    impalaUri + impalaDatabaseName, username);
        } catch (SQLTimeoutException e) {
            LOGGER.error("Connection timeout", e);
            throw new RemoteDataLakeException(HttpStatus.GATEWAY_TIMEOUT,
                    String.format("Unable to connect to Impala at '%s'. Reason: request timeout.", impalaUri));
        } catch (SQLException e) {
            LOGGER.error("Failed to connect to Impala at '{}' with principal_name {}.",
                    impalaUri + impalaDatabaseName, username);
            LOGGER.error("The following Exception was thrown when connecting to Impala.", e);
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE,
                    "Failed to connect to Data Lake database.");
        } catch (Exception e) {
            LOGGER.error("Connection failure: " + e.getMessage());
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to connect to Impala at '" +
                    impalaUri +
                    "'. Check server logs for more information about the error.");
        }
    }

    @Override
    public String createDL() throws RemoteDataLakeException {
        dataLakeFolderName = UUID.randomUUID().toString();
        dataLakeFolderPath = new Path(hdfsRoot.getWorkingDirectory(), dataLakeFolderName);

        try {
            if (FileSystem.mkdirs(hdfsRoot, dataLakeFolderPath, dataLakeFolderPermissions)) {
                // if the folder was created, store its URI
                dataLake = FileSystem.get(dataLakeFolderPath.toUri(), hadoopConfiguration);
                dataLake.setWorkingDirectory(dataLakeFolderPath);
                setURIValue(dataLake.getWorkingDirectory().toString());
            } else throw new IOException("mkdirs returned FALSE. Unable to create Data Lake folder.");
        } catch (IOException e) {
            LOGGER.error("Failed to create Data Lake in " + dataLakeFolderPath.toString(), e);
            throw new RemoteDataLakeException(HttpStatus.UNAUTHORIZED,
                    "failed to create Data Lake folder.");
        }

        return getURI();
    }

    @Override
    public String writeDPO(String fileName, String newElement) throws RemoteDataLakeException {
        if (dataLakeURI == null) {
            LOGGER.error("writeDPO called before initializing Data Lake URI");
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "Data Lake not initialized. Please report this error to a system admin.");
        }
        // init Hadoop FileSystem
        try {
            dataLake = FileSystem.get(URI.create(dataLakeURI), hadoopConfiguration);
            dataLake.setWorkingDirectory(dataLakeFolderPath);
            poolFilePath = dataLake.makeQualified(poolFilePath);
        } catch (IOException e) {
            LOGGER.error("Unable to open Hadoop FileSystem for DataLake.", e);
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE, "Unable to connect to remote Data Lake");
        }

        // Write the DPO to the HDFS
        Path dpoFile = computePath(dataLakeURI, fileName);
        String message = writeHDFS(dpoFile, newElement);
        if (message == null)  // success.
            LOGGER.info("Written: '" + fileName + "' to HDFS");
        else
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE, message);

        // Append lines to pool file
        Path dataPool = refreshDataPool(newElement);

        // Create Impala table and load data from HDFS
        String action = "<n.d.>";
        String tableName = getTableName();
        try {
            action = "check if table exists";
            if (Impala.tableExists(impalaConnection, impalaDatabaseName, tableName)) {
                action = "delete old table";
                Impala.dropTable(impalaConnection, tableName);
            }
            action = "create new table";
            Impala.createTable(impalaConnection, tableName, columns);
            action = "load data";
            Impala.loadData(impalaConnection, dataPool, tableName);
            LOGGER.info(String.format("Content of Impala table '%s.%s' refreshed",
                    impalaDatabaseName, tableName));
        } catch (SQLTimeoutException e) {
            LOGGER.error("error (timeout)", e);
            throw new RemoteDataLakeException(HttpStatus.GATEWAY_TIMEOUT,
                    "Remote SQL Data Lake took too long to respond.");
        } catch (SQLSyntaxErrorException e) {
            LOGGER.error("error (syntax)", e);
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR, "There was an error with the server. " +
                    "Please check the logs for more information or contact an admin.");
        } catch (SQLException e) {
            try {
                Impala.dropTable(impalaConnection, tableName);
            } catch (SQLException nestedException) {
                e.setNextException(nestedException);
            }
            LOGGER.error("error (SQL) during ' " + action + "' action", e);

            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE,
                    String.format("Unable to %s on remote Impala schema '%s' (table %s was deleted)",
                            action, impalaDatabaseName, tableName));
        }
        return Impala.getSelectQueryString(impalaDatabaseName, tableName);
    }

    /**
     * Append the provided content to the Pool file for this Data Lake.
     *
     * @param newContent the new data to be appended
     *
     * @return the path to a file that can be imported using
     *         {@link Impala#loadData(Connection, Path, String) Impala.loadData}
     */
    /// POOL
    private Path refreshDataPool(String newContent) {
        StringBuilder poolFileContent = new StringBuilder();
        String[] lines = newContent.split("\\n");
        // if needed, write the first line of headers in the file.
        // The line is REMOVED from the 'content' List.
        String headers = cleanHeaders(lines[0]);
        poolFileContent.append(headers);
        // clean the lines of newElement
        for (String line : Arrays.copyOfRange(lines, 1, lines.length)) {
            String cleanedData = cleanData(line);
            poolFileContent.append(cleanedData);
        }
        // Append the data
        FSDataOutputStream append = null;
        BufferedWriter out = null;
        try {
            append = dataLake.append(poolFilePath);
            out = new BufferedWriter(new OutputStreamWriter(append.getWrappedStream()));
            out.write(poolFileContent.toString());
            out.flush();
            // Files loaded by Impala are removed from their original folder.
            // We create a snapshot of the current Pool file and load it in Impala,
            // so we can add other DPOs later without losing the line count and the previous data.
            try {
                // name the snapshot with the current line count
                Path poolSnapshot = new Path(dataLakeFolderPath, POOL_FILE_NAME + "_" + getLinesCount() + POOL_FILE_EXT);
                FileUtil.copy(dataLake, poolFilePath, dataLake, poolSnapshot, false, true, hadoopConfiguration);
                return poolSnapshot;
            } catch (IOException e) {
                LOGGER.error("Failed to take a snapshot of Pool file. Line count: " + getLinesCount(), e);
                throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE, "Unable to update Data Pool");
            }
        } catch (IOException e) {
            LOGGER.error("Failed to append data to Pool file.", e);
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE, "Unable to update Data Pool");
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (append != null) {
                    append.close();
                }
            } catch (IOException e) {
                LOGGER.warn("Output Stream not closed!");
            }
        }
    }

    /**
     * Compute the number of lines in the Pool file of this Data Lake and returns the count.
     * The returned number is the index of the next line of data.
     *
     * @return the line count of this Pool file
     */
    /// POOL
    private int getLinesCount() {
        if (poolSize < 0) {  // count the number of lines in the pool file
            BufferedReader poolFileStream = null;
            try {
                if (!dataLake.exists(poolFilePath)) {
                    dataLake.create(poolFilePath).close();
                }
                poolFileStream = new BufferedReader(new InputStreamReader(dataLake.open(poolFilePath)));
                String nextLine;
                do {
                    // poolSize is -1 when not initialized, so we must increment the value at least once:
                    // if the first readLine() returns null, then there are no lines and poolSize will be 0.
                    poolSize++;
                    nextLine = poolFileStream.readLine();
                } while (nextLine != null);
            } catch (IOException e) {
                poolSize = -1;  // invalidate count
                LOGGER.error("Failed to compute poolSize.", e);
                throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE,
                        String.format("Unable to access Data Pool. %s", e.getMessage())
                );
            } finally {
                if (poolFileStream != null) {
                    try {
                        poolFileStream.close();
                    } catch (IOException e) {
                        LOGGER.warn("Output Stream not closed!");
                    }
                }
            }
        }
        return poolSize;
    }

    /**
     * Return an header that is ready to be written in the Pool file. If the file already has a header, returns null.
     * If the given parameter is not a header (i.e. does not start with '#'), it will be handled by
     * {@link #cleanData(String)} like it were a normal data line.
     *
     * @param headers the first line of a CSV file
     *
     * @return a String that is cleaned and ready to be written to the Pool file. Returns an empty string if
     */
    /// POOL
    private String cleanHeaders(String headers) {
        int lines = getLinesCount();  // init the poolSize counter for the first call to cleanData

        // If it's just a common line of data, just write it out
        if (!headers.startsWith("#")) return cleanData(headers);
        // If the Pool file is not empty, it must already have an header
        if (lines > 0) return "";
        // Otherwise, strip comment character and spaces...
        String line = headers.split("#")[1].trim();
        // ...and prepend the name of the column for the unique line index
        return String.format("# %s, %s\n", indexColumn, line);
    }

    /**
     * Return data that is ready to be written in the Pool file.
     *
     * @param dirty a CSV line to be cleaned
     *
     * @return the line with an index number prepended, or null if the line started with '#'.
     */
    /// POOL
    private String cleanData(String dirty) {
        // Ignore this line if it's a comment
        if (dirty.startsWith("#")) return "";
        // Write the line index number, then increment poolSize and append the original line
        return String.format("%s, %s\n", poolSize++, dirty);
    }

    /**
     * return a {@link Path} object that points to the specified fileName.
     * The fileName can be the name of this Data Lake, in which case only the path to the Data Lake is returned.
     * Otherwise, it will be resolved as a path to this file name as if the file were inside the Data Lake.
     *
     * @param dataLakeURI the base URI of this {@link HDFSDataLake}
     * @param fileName the name of the Data Lake itself or the name
     *
     * @return
     */
    private Path computePath(String dataLakeURI, String fileName) {
        if (dataLakeFolderName == null)
            // the path follows the base URI, i.e. 'hdfs://myhost:myport/tmp' -> /tmp
            dataLakeFolderName = "/" + dataLakeURI.replace(hdfsUri, "");

        Path dataLake = new Path(hdfsRoot.getWorkingDirectory(), dataLakeFolderName);
        if (fileName.equals(dataLakeFolderName))
            return dataLake;
        else
            return new Path(dataLake, fileName);

    }

    @Override
    public void deleteDL() throws RemoteDataLakeException {
        // delete Impala table
        String tableName = getTableName();
        try {
            if (Impala.tableExists(impalaConnection, impalaDatabaseName, tableName))
                Impala.dropTable(impalaConnection, tableName);
        } catch (SQLTimeoutException e) {
            LOGGER.error("Impala connection timed out.", e);
            throw new RemoteDataLakeException(HttpStatus.GATEWAY_TIMEOUT,
                    "Impala took too long to respond.");
        } catch (SQLSyntaxErrorException e) {
            LOGGER.error("error (syntax)", e);
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "There was an error with the server. " +
                            "Please check the logs for more information or contact an admin.");
        } catch (SQLException e) {
            LOGGER.error("error (SQL)", e);
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE,
                    "Unable to delete Data Lake table '" + tableName + "' from Impala");
        }

        // delete HDFS folder with 'recursive' = true
        try {
            if (! hdfsRoot.delete(dataLakeFolderPath, true)) {
                LOGGER.warn("Unable to delete HDFS folder: " + getURI());
                throw new RemoteDataLakeException(HttpStatus.NOT_FOUND,
                        "This DataLake does not exist or it may be expired: " + this.getURI());
            } else {
                LOGGER.info("Deleted " + getURI());
            }
        } catch (IOException e) {
            LOGGER.error("Failed to delete Data Lake instance.", e);
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE,
                    "Unable to completely delete HDFSDataLake.");
        }
    }

    @Override
    public void disconnect() throws Exception {
        String problem = "Connection to Impala was interrupted";
        try {
            impalaConnection.close();
            problem = "Connection to HDFS was interrupted";
            hdfsRoot.close();
            LOGGER.info("HDFSDataLake instance was released.");
        } catch (SQLException e) {
            LOGGER.error("Error in HDFSDataLake: " + problem, e);
            throw new Exception("Unable to reach remote Impala database", e);
        } catch (IOException e) {
            LOGGER.error("Error in HDFSDataLake: " + problem, e);
            throw new Exception("Unable to reach remote HDFS", e);
        }
    }

    @Override
    public Date getCreationDate() throws Exception {
        Path dataFilePath = computePath(dataLakeURI, dataLakeFolderName);
        try {
            // get information about all the files in the parent folder
            FileStatus status = hdfsRoot.getFileStatus(dataFilePath);
            return new Date(status.getModificationTime());
        } catch (IOException e) {
            throw new RemoteDataLakeException(HttpStatus.NOT_FOUND,
                    "Failed to get Data Lake's creation date. Caused by:\n" + e.getMessage());
        }
    }

    @Override
    LinkedList<String> all() throws RemoteDataLakeException {
        LinkedList<String> hdfsFiles = new LinkedList<>();
        String msg = "Unable to read Data Lakes";
        try {
            FileStatus[] files = hdfsRoot.listStatus(new Path("."));
            for (FileStatus f : files) {
                hdfsFiles.add(f.getPath().toString());
            }
        } catch (IOException e) {
            LOGGER.error(msg, e);
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE, msg);
        }

        return hdfsFiles;
    }

    @Override
    public String toString() {
        return getURI();
    }

    @Override
    public String getURI() {
        return dataLakeURI;
    }

    @Override
    protected void setURIValue(String thisURI) {
        super.setURIValue(thisURI);
        String folder = thisURI
                .replace(hdfsWorkDir, "")
                .replace(hdfsUri, "")
                .replaceAll("/", "");
        this.dataLakeFolderName = folder;
        this.dataLakeFolderPath = new Path(hdfsRoot.getWorkingDirectory(), dataLakeFolderName);
    }

    private String writeHDFS(Path filePath, String content) {
        FSDataOutputStream stream = null;
        PrintWriter out = null;
        String action, error = null;

        try {
            if (! hdfsRoot.exists(filePath)) {
                stream = hdfsRoot.create(filePath, false);
                hdfsRoot.setPermission(filePath, dataLakeFolderPermissions);
                action = "Writing";
            } else {
                stream = hdfsRoot.append(filePath);
                action = "Appending to";
            }

            LOGGER.info(action + " new file " + filePath.toString());

            out = new PrintWriter(stream);
            out.append(content);
            out.flush();

            stream.hflush();
        } catch (IOException e) {
            error = "Failed to write DPO to Data Lake. Reason: " + e.getMessage()
                    .split("\n\tat")[0];  // this operation removes the stack trace from the msg
            LOGGER.error("Failed to write DPO", e);
        } finally {
            // close the output writer
            if (out != null) {
                out.close();
            }
            // close the output stream
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    LOGGER.warn("Unable to close HDFS output stream.", e);
                }
            }
        }
        return error;
    }

    private String getTableName() {
        return "t_" + dataLakeFolderName.replaceAll("-", "_");
    }
}
