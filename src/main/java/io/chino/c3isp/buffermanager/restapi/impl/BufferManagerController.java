/*
 * Based on the template by Hewlett Packard Enterprise Development Company, L.P.
 */
package io.chino.c3isp.buffermanager.restapi.impl;

import io.chino.c3isp.buffermanager.BufferManager;
import io.chino.c3isp.buffermanager.restapi.types.datalake.DataLake;
import io.chino.c3isp.buffermanager.restapi.types.datalake.FileSystemDataLake;
import io.chino.c3isp.buffermanager.restapi.types.datalake.RemoteDataLakeException;
import io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeFileSystemType;
import io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeType;
import io.chino.c3isp.buffermanager.restapi.types.requests.Format;
import io.chino.c3isp.buffermanager.restapi.types.requests.PrepareDataQuery;
import io.chino.c3isp.buffermanager.restapi.types.response.DPOWriteOutcome;
import io.chino.c3isp.buffermanager.restapi.types.response.DataLakeStatus;
import io.chino.c3isp.buffermanager.restapi.types.response.PrepareDataResponse;
import io.chino.c3isp.formatadapter.FormatAdapterApiClient;
import io.chino.c3isp.isi.IsiApiClient;
import io.swagger.annotations.*;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;

import static io.chino.c3isp.buffermanager.restapi.types.requests.Format.CSV;

/**
 * REST Controller for the BufferManager component of C3ISP ISI.
 *
 * @author Andrea Arighi [andrea@chino.io]
 */
@ApiModel(value = "Buffer Manager",
        description = "Manages the creation and deletion of the VDL/DLB for C3ISP-aware analytics")
@RestController
@RequestMapping("/v1")
public class BufferManagerController {

    @Value("${data.filesystem.root}")
    private String fileSystemRoot;
    @Value("${rest.endpoint.url.callGet}")
    private String callGetEndpoint;
    @Value("${security.user.name}")
    private String restUser;
    @Value("${security.user.password}")
    private String restPassword;
    /**
     * Used to call REST endpoints; configured by RestTemplateBuilder
     */
    @Autowired
    private RestTemplate restTemplate;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder.basicAuthorization(restUser, restPassword).build();
    }

    /**
     * Logger class, can be used within the methods to log information
     */
    private final static Logger LOGGER = LoggerFactory.getLogger(BufferManagerController.class);

    /**
     * Path where files will be saved before conversion
     */
    private Path tempFolderPath;

    /**
     * Compute and check the path to the BufferManager temp folder
     */
    private void initialize() {
        try {
            if (tempFolderPath != null && Files.exists(tempFolderPath))
                return;
            tempFolderPath = Paths.get(URI.create(fileSystemRoot)).resolve(".private");
            Files.createDirectories(tempFolderPath);
            tempFolderPath.toFile().setReadable(false);
        } catch (NullPointerException | IOException e) {
            LOGGER.error("Failed to initialize temp folder.", e);
            tempFolderPath = null;
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR, "Buffer Manager initialization failed. " +
                    "Please stop using the service and notify an admin.");
        }
    }

    // BEGIN: BufferManage REST API endpoints //


    /**
     * Start the prepareData workflow, which creates a DLB/VDL and populates it with data from ISI.
     *
     * @param DPOIdList the list of DPO IDs to be read from the ISI API.
     * @param ISIMetadata metadata to call the ISI API
     * @param serviceName the name of the Analytic that requested the creation of the Data Lake.
     * @param dataDestinationFormat Destination format of the data contained in the Data Lake.
     * @param dataLakeType Type of the Data Lake that needs to be created.
     * @param fileSystem Type of filesystem where the Data Lake must be created.
     *
     * @return The Data Lake URI as a {@link String}.
     */
    @ApiOperation(httpMethod = "POST",
            value = "Starts the prepareData task",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE,
            response = PrepareDataResponse.class,
            code  =  201,
            notes = "Start the prepareData workflow, where a new Data Lake (DLB or VDL) is created and populated with DPOs. " +
                    "Data are read using the ISI API <b>readDPO</b>, using the metadata that are sent in the " +
                    "<code>X-c3isp-metadata</code> header." +
                    "</br>" +
                    "</br>" +
                    "<h4>Note on the prepareData response:</h4>" +
                    "The prepareData returns with a \"201\" HTTP status code if the Data Lake is created successfully; " +
                    "then, for each requested DPO an entry in the <i>\"data\"</i> list is added. " +
                    "</br>" +
                    "If the DPO is correctly read and written on the Data Lake, the entry will have a \"201\" HTTP code and the " +
                    "location of the new file will be written in the <i>\"file\"</i> field. " +
                    "For each DPO that cannot be read or written, the entry will report the error code and the cause of" +
                    "the error in the <i>\"message\"</i> field." +
                    "</br>" +
                    "</br>" +
                    "See the structure of the response JSON below."
    )
    @ApiResponses({
            @ApiResponse(
                    code = 400,
                    message = "Bad request. Reasons may vary, but most common causes are an empty request or an empty DPO list"
            ),
            @ApiResponse(
                    code = 401,
                    message = "Unauthorized. Wrong authentication credentials for ISI API provided"
            ),
            @ApiResponse( // caused by DataLake.init()
                    code = 403,
                    message = "Access denied. Either the user or the 'prepareData' doesn't have authentication credentials or " +
                            "lacks permissions to read/write DPO on the Data Lake"
            ),
            @ApiResponse(
                    code = 500,
                    message = "Server error. Can be caused by errors in the server's code (in this case an error message " +
                            "will be printed in the server's logs, with more details about the problem). " +
                            "Also returned when the data lake folder can not be created on the server."
            ),
            @ApiResponse(
                    code = 503,
                    message = "Remote Data Lake error (service unavailable). Caused by errors during interaction with a " +
                            "remote DB or file system. Common causes are that 'prepareData' could not connect to the " +
                            "Data Lake or couldn't write DPO to it."
            ),
            @ApiResponse(
                    code = 504,
                    message = "Remote Data Lake timeout. A remote database or file system took too long to respond."
            )
    })
    @PostMapping(value = "/prepareData", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PrepareDataResponse> prepareData(
            @ApiParam(name = "DPO_IDs", value = "the list of DPO IDs to be read from the ISI API.",
                    required = true, type = "array")
            @RequestBody() List<String> DPOIdList,

            @ApiParam(name = BufferManager.C3ISP_METADATA_HEADER, value = "ISI API metadata for the readDPO. " +
                    "Check readDPO guide to learn more.",
                    required = true, allowEmptyValue = true, type = MediaType.APPLICATION_JSON_UTF8_VALUE)
            @RequestHeader(BufferManager.C3ISP_METADATA_HEADER) String ISIMetadata,

            @ApiParam(name = "service_name", value = "the name of the Analytic that requested the creation of the Data Lake.",
                    required = true, example = "spamEmailClassify", type = "string")
            @RequestParam(name = "service_name") String serviceName,

            @ApiParam(name = "format", value = "Destination format of the data contained in the Data Lake.",
                    required = true, allowableValues = "CEF, CSV, EML, MODEL, PCAP, BINARY", example = "CSV", type = "enum")
            @RequestParam(name = "format") Format dataDestinationFormat,

            @ApiParam(name = "data_lake", value = "Type of the Data Lake that needs to be created.",
                    required = true, example = "DLB", allowableValues = "DLB, VDL", type = "enum")
            @RequestParam(name = "data_lake") DataLakeType dataLakeType,

            @ApiParam(name = "type", value = "Type of filesystem where the Data Lake must be created. Different types " +
                    "have different URI structures.<br>" +
                    "<b>NOTE:</b> only <code>FS</code> can handle binary files.",
                    required = true, allowableValues = "FS, MYSQL, HDFS", example = "MYSQL", type = "enum")
            @RequestParam(name = "type") DataLakeFileSystemType fileSystem
    ) {
        PrepareDataResponse response;
        this.initialize();
        try {
            LOGGER.info("* * * * Start prepareData workflow (prepare empty > populate > release)");
            PrepareDataQuery query = new PrepareDataQuery(DPOIdList, ISIMetadata, serviceName, dataDestinationFormat, dataLakeType, fileSystem);

            if (query.getDPOIdList() == null || query.getDPOIdList().isEmpty()) {
                PrepareDataResponse errorResponse = new PrepareDataResponse(
                        new DataLakeStatus(null,
                                "Bad request. The query must not be emtpy and contain at least 1 DPO ID. " +
                                        "(no Data Lake was created)")
                );
                return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
            }

            LinkedList<DPOWriteOutcome> DPOList = new LinkedList<>();
            int missingDPOs = 0;

            // parse query and initialize DataLake
            DataLake dataLake = DataLake.fromQuery(query);
            try {
                String uri = dataLake.createDL();
                response = new PrepareDataResponse(
                        new DataLakeStatus(uri)
                );
            } catch (RemoteDataLakeException rdlex) {
                // failed to create, return the error immediately to avoid wasting time reading DPOss
                response = new PrepareDataResponse(
                        new DataLakeStatus(null, rdlex.getStatusText())
                );
                return new ResponseEntity<>(response, rdlex.getStatusCode());
            }

            // read DPO using ISI API (from DPO list in query) and add to Data Lake.
            // If the read fails for some reason, save error information in 'DPOList'
            for (String DPOId : query.getDPOIdList()) {
                String fileName = null;
                Path tempFilePath = null;
                File tempFile = null;
                try {
                    InputStream cti = IsiApiClient.retrieveCTI(DPOId, ISIMetadata);

                    // Write the CTI bytes to a temp file, to be read by Format Adapter
                    tempFilePath = tempFolderPath.resolve(DPOId + "-" + UUID.randomUUID());
                    tempFile = Files.createFile(tempFilePath).toFile();
                    writeStream(cti, tempFile);
                    tempFile.setReadable(true, true);

                    // call FormatAdapter.convertDL, add the CSV parameter if needed.
                    String convertedCti = FormatAdapterApiClient.convertDL(tempFile, dataDestinationFormat == CSV);

                    // save converted data to the actual DPO file and return the URI
                    if (convertedCti != null) {
                        // successful conversion
                        fileName = dataLake.writeDPO(DPOId, convertedCti);
                    } else if (dataLake instanceof FileSystemDataLake) {
                        // conversion failed, but file might be binary; just copy it as it is to the FS Datalake
                        fileName = ((FileSystemDataLake) dataLake).writeDPO(DPOId, tempFile);
                    } else {
                        // conversion failed: write the original content of the DPO as a String
                        fileName = dataLake.writeDPO(DPOId,
                                Strings.join(Files.readAllLines(tempFile.toPath()), '\n')
                        );
                    }
                    DPOList.add(new DPOWriteOutcome(
                            DPOId,
                            201,
                            "Success.",
                            fileName
                    ));
                } catch (RemoteDataLakeException rdlex) { // exception during WRITE
                    DPOList.add( new DPOWriteOutcome(
                            DPOId,
                            rdlex.getStatusCode(),
                            "Failed to write DPO: " + rdlex.getStatusText()
                    ));
                    missingDPOs ++;
                } catch (HttpStatusCodeException e) { // exception during READ
                    int HTTPcode = e.getStatusCode().value();
                    DPOList.add( new DPOWriteOutcome(
                            DPOId,
                            HTTPcode,
                            "Error: " + e.getStatusText()
                    ));
                    missingDPOs ++;
                } catch (IOException e) {
                    LOGGER.error(e.getMessage(), e);
                    DPOList.add( new DPOWriteOutcome(
                            DPOId,
                            HttpStatus.INTERNAL_SERVER_ERROR,
                            "Error: Unable to create intermediate file for format conversion. " +
                            "Please contact an administrator."
                    ));
                } finally {
                    if (tempFile != null && tempFile.exists()) {
                        boolean deleted = tempFile.delete();
                        if (!deleted) {
                            LOGGER.warn("Format Adapter tmp file not deleted: " + tempFile.getAbsolutePath());
                        }
                    }
                }
            }

            // update errors and return the response
            response.setDPOList(DPOList);

            try {
                dataLake.disconnect();
            } catch (Exception e) {
                LOGGER.error("prepareData: Data Lake not disconnected.", e);
            }

            LOGGER.info("* * * * Completed prepareData (" + missingDPOs + " missing DPO" +
                    (missingDPOs == 1 ? "" : "s") + ")");
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (RemoteDataLakeException e) {
            response = new PrepareDataResponse(
                    new DataLakeStatus(
                            "null",
                            e.getMessage()
                    )
            );
            return new ResponseEntity<>(response, e.getStatusCode());
        }
    }


    /**
     * Create an empty Data Lake. It is possible to write in the new instance using the
     * {@link #populateDataLake(String, String, Map, String)} endpoint.
     *
     * @param dataLakeType Type of the Data Lake that needs to be created.
     * @param fileSystem Type of filesystem where the Data Lake must be created.
     *
     * @return The Data Lake URI as a {@link String}.
     */
    @ApiOperation(httpMethod = "POST",
            value = "Create a new Data Lake without reading any DPOs",
            consumes = MediaType.TEXT_PLAIN_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE,
            notes = "Creates a new Data Lake with the specified type and file system and returns a URI " +
                    "that can be used to directly interact with it. The Data Lake is empty, but can be written to " +
                    "using the /populateDataLake endpoint."
    )
    @ApiResponses({
            @ApiResponse(
                    code = 400,
                    message = "Bad request. Reasons may vary, but most common cause is a syntax error in the query."
            ),
            @ApiResponse(
                    code = 401,
                    message = "Unauthorized. Wrong authentication credentials for ISI API provided"
            ),
            @ApiResponse( // caused by DataLake.init()
                    code = 403,
                    message = "Forbidden. Either the user or the BufferManager has no permission to create a new Data Lake."
            ),
            @ApiResponse(
                    code = 500,
                    message = "Server error. Can be caused by errors in the server's code (in this case an error message " +
                            "will be printed in the server's logs, with more details about the problem)."
            ),
            @ApiResponse(
                    code = 503,
                    message = "Remote Data Lake error (service unavailable). Caused by errors during interaction with a " +
                            "remote DB or file system. Common causes are that 'prepareEmptyDataLake' could not connect " +
                            "to the server or create the Data Lake."
            ),
            @ApiResponse(
                    code = 504,
                    message = "Remote Data Lake timeout. A remote database or file system took too long to respond."
            )
    })
    @PostMapping(value = "/prepareEmptyDataLake", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DataLakeStatus> prepareEmptyDataLake(
            @ApiParam(name = "data_lake", value = "Type of the Data Lake that needs to be created.",
                    required = true, example = "DLB", allowableValues = "DLB, VDL", type = "enum")
            @RequestParam(name = "data_lake") DataLakeType dataLakeType,

            @ApiParam(name = "type", value = "Type of filesystem where the Data Lake must be created. Different types " +
                    "have different URI structures.<br>" +
                    "<b>NOTE (v0.3): HDFS is not implemented yet.</b>",
                    required = true, allowableValues = "FS, MYSQL, HDFS", example = "MYSQL", type = "enum")
            @RequestParam(name = "type") DataLakeFileSystemType fileSystem
    ) {
        this.initialize();
        LOGGER.info("* * * * Creating empty Data Lake");
        String outcome = "success";
        DataLake dl = DataLake.fromQuery(
                new PrepareDataQuery(new LinkedList<>(), "{}", null, null, dataLakeType, fileSystem)
        );

        DataLakeStatus response;
        HttpStatus httpStatus;

        try {
            // create DataLake
            String uri = dl.createDL();
            response = new DataLakeStatus(uri);

            httpStatus = HttpStatus.CREATED;
        } catch (RemoteDataLakeException rdlex) {
            // failed to create - return error response
            response = new DataLakeStatus(null, rdlex.getStatusText());

            httpStatus = rdlex.getStatusCode();
        }

        LOGGER.info("* * * * Completed prepareEmptyDataLake (" + outcome + ")");

        return new ResponseEntity<>(response, httpStatus);
    }


    /**
     * Write a new file in the Data Lake.
     *
     * @param dataLakeURI URI to the desired Data Lake instance
     * @param dpoName the name of the new file (optional - if empty, a random, unique name will be generated).
     *                NOTE: if a file with the specified name exists in the Data Lake, it will be overwritten!
     * @param uriQueryParams {@link HashMap} with the URL parameter of this request
     * @param newDpo the actual content of the new file (as {@link String})
     *
     * @return {@link DPOWriteOutcome}
     */
    @ApiOperation(httpMethod = "POST",
            value = "Push some data to an existing Data Lake",
            consumes = MediaType.TEXT_PLAIN_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE,
            notes = "Access an existing Data Lake using the URI and write the content of the request in a new file. " +
                    "You can optionally specify the name of the file; if you don't, a random name (UUID) is generated.<br>" +
                    "<b>WARNING:</b> if a file with the same name already exists in the Data Lake, it will be overwritten."
    )
    @ApiResponses({
            @ApiResponse(
                    code = 400,
                    message = "Bad request. Reason may vary, but usually caused by wrong or misspelled request parameters"
            ),
            @ApiResponse(
                    code = 401,
                    message = "Unauthorized. Credentials were not provided or the user can not access the Data Lake."
            ),
            @ApiResponse( // caused by DataLake.init()
                    code = 403,
                    message = "Access denied. Either the user or the 'prepareData' doesn't have authentication credentials or " +
                            "lacks permissions to read/write DPO on the Data Lake"
            ),
            @ApiResponse(
                    code = 404,
                    message = "Not found. The provided URI does not identify an existing Data Lake. The URI may be not" +
                            "valid, expired or the Data Lake was already deleted"
            ),
            @ApiResponse(
                    code = 500,
                    message = "Server error. Can be caused by errors in the server's code (in this case an error message " +
                            "will be printed in the server's logs, with more details about the problem)."
            ),
            @ApiResponse(
                    code = 503,
                    message = "Remote Data Lake error (service unavailable). Caused by errors during interaction with a " +
                            "remote DB or file system. Common causes are that 'populateDataLake' could not connect to " +
                            "the Data Lake or couldn't write DPO to it."
            ),
            @ApiResponse(
                    code = 504,
                    message = "Remote Data Lake timeout. A remote database or file system took too long to respond."
            )
    })
    @PostMapping(value = "/populateDataLake", consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DPOWriteOutcome> populateDataLake(
            @ApiParam(name = "uri", value = "URI that points to an existing DLB or VDL. Valid URIs are returned by the " +
                    "/prepareData and /prepareEmptyDataLake API endpoints of the BufferManager.",
                    type = "string", required = true, example = "file:///opt/dlb/42f7bb50-12b0-42ee-b804-1cde84cf10e6")
            @RequestParam(name = "uri") String dataLakeURI,

            @ApiParam(name = "file_name", value = "[OPTIONAL] If specified, the DPO will be saved with this name in the " +
                    "Data Lake. Otherwise, a random name will be generated for the file.",
                    type = "string", example = "mail0.dpo")
            @RequestParam(name = "file_name", required = false) String dpoName,

            @ApiParam(value = "This attributes only exists to collect URI parameters of the Data Lake. It should NOT be " +
                    "visible and shall not be used neither in the Swagger UI nor in the API calls",
                    hidden = true)
            @RequestParam(required = false) Map<String, Object> uriQueryParams,

            @ApiParam(name = "File content (body)",
                    value = "The content of the new file.",
                    required = true, type = "string")
            @RequestBody String newDpo
    ) {
        this.initialize();
        LOGGER.info("* * * * Writing data inside Data Lake");

        if (!uriQueryParams.isEmpty()) {
            uriQueryParams.remove("uri");
            uriQueryParams.remove("file_name");
            dataLakeURI = appendParameters(dataLakeURI, uriQueryParams);
        }

        DataLake dl = DataLake.fromURI(dataLakeURI);

        DPOWriteOutcome response;
        HttpStatus httpStatus;

        try {
            // write DPO
            if (dpoName == null || dpoName.isEmpty()) {
                dpoName = UUID.randomUUID().toString();
            }

            String retrieveDpoUri = dl.writeDPO(dpoName, newDpo);

            httpStatus = HttpStatus.CREATED;
            // DPO written - return SUCCESS_MESSAGE response
            response = new DPOWriteOutcome(
                    dpoName,
                    httpStatus,
                    "Success.",
                    retrieveDpoUri
            );
        } catch (RemoteDataLakeException rdlex) {
            // failed to write DPO - return ERROR response
            response = new DPOWriteOutcome(
                    dpoName,
                    rdlex.getStatusCode(),
                    "Failed to write DPO: " + rdlex.getStatusText()
            );
            httpStatus = rdlex.getStatusCode();
        }

        LOGGER.info("* * * * Completed populateDataLake (" + httpStatus + ")");
        return new ResponseEntity<>(response, httpStatus);
    }


    /**
     * Delete a Data Lake
     *
     * @param dataLakeUri the URI of the DLB or VDL that will be deleted
     *
     * @return a success {@link String} or an HTTP error, returned as "text/plain"
     */
    @ApiOperation(httpMethod = "POST",
            value = "Delete a Data Lake",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.TEXT_PLAIN_VALUE,
            notes = "Deletes the Data Lake instance that can be reached with the parameter URI."
    )
    @ApiResponses({
            @ApiResponse(
                    code = 400,
                    message = "Bad request. A syntax error was found in the query or in the Data Lake URI"
            ),
            @ApiResponse(
                    code = 401,
                    message = "Unauthorized. Credentials were not provided in the API call Authorization header."
            ),
            @ApiResponse( // caused by DataLake.init()
                    code = 403,
                    message = "Forbidden. Either the user or the BufferManager has no permission to create a new Data Lake."
            ),
            @ApiResponse(
                    code = 404,
                    message = "Not found. The provided URI does not identify an existing Data Lake. The URI may be not" +
                            "valid, expired or the Data Lake was already deleted"
            ),
            @ApiResponse(
                    code = 500,
                    message = "Server error. Can be caused by errors in the server's code (in this case an error message " +
                            "will be printed in the server's logs, with more details about the problem)."
            ),
            @ApiResponse(
                    code = 503,
                    message = "Remote Data Lake error (service unavailable). Caused by errors during interaction with a " +
                            "remote DB or file system. Common causes are that 'releaseData' could not connect to the " +
                            "Data Lake or delete it."
            ),
            @ApiResponse(
                    code = 504,
                    message = "Remote Data Lake timeout. A remote database or file system took too long to respond."
            )
    })
    @PostMapping(value = "/releaseData", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> releaseData(
            @ApiParam(name = "DataLake URI", value = "the URI of the DLB or VDL that will be deleted", required = true,
                    example = "file:///opt/dlb/42f7bb50-12b0-42ee-b804-1cde84cf10e6")
            @RequestBody()
                    String dataLakeUri
    ) {
        this.initialize();
        try {
            LOGGER.info("* * * * Removing Data Lake");

            DataLake dl = null;
            try {
                if (dataLakeUri == null) {
                    throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "URI can not be empty");
                }
                // check validity of the URI
                dl = DataLake.fromURI(dataLakeUri);
                // delete DataLake
                dl.deleteDL();

                return new ResponseEntity<String>("Successfully removed " + dl.getURI(), HttpStatus.OK);
            } catch (HttpStatusCodeException e) {
                LOGGER.info("* * * * Completed releaseData with an error", e);
                return new ResponseEntity<String>("Error " + e.getMessage(), e.getStatusCode());
            } finally {
                try {
                    if (dl != null) {
                        dl.disconnect();
                        LOGGER.info("Successfully disconnected.");
                    }
                } catch (Exception e) {
                    LOGGER.error("Exception thrown by DataLake.disconnect()", e);
                } finally {
                    LOGGER.info("* * * * Completed releaseData");
                }
            }
        } catch (RemoteDataLakeException e) {
            return new ResponseEntity<>(e.getMessage(), e.getStatusCode());
        }
    }

    /**
     * Append parameters to a URI string from a HashMap
     *
     * @param baseUri the URI String
     * @param params a {@link Map} that contains parameters to be appended
     *
     * @return a new URI with the new parameters
     */
    private String appendParameters(String baseUri, Map<String, Object> params) {
        StringBuilder dataLakeURIBuilder = new StringBuilder(baseUri);
        if (!baseUri.contains("?")) {
            dataLakeURIBuilder.append("?");
        } else {
            dataLakeURIBuilder.append("&");
        }

        for (String key : params.keySet()) {
            if (!baseUri.contains(key + "="))
                dataLakeURIBuilder.append(key).append("=").append(params.get(key)).append("&");
        }
        return dataLakeURIBuilder.toString().substring(0, dataLakeURIBuilder.length() - 1);
    }

    /**
     * Output the content of a {@link InputStream} to a {@link File}, then close the Stream.
     *
     * @param stream
     * @param destinationFile
     */
    private void writeStream(InputStream stream, File destinationFile) throws IOException {
        try {
            destinationFile.setWritable(true);
            Files.copy(stream, destinationFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            destinationFile.setWritable(false);
        } finally {
            stream.close();
        }
    }

    // Used for rendering on Swagger UI
    private static final String METADATA_EXAMPLE = "see ISI API metadata";
}