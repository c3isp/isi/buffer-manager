package io.chino.c3isp.buffermanager.restapi.types.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.chino.c3isp.buffermanager.restapi.types.datalake.DataLake;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

/**
 * Contains information about the outcome of the {@link DataLake#createDL() createDL()} operation
 */
@ApiModel(value = "DataLake status",
        description = "Contains the URI of the new Data Lake and a message string with information about the DL creation operation." +
                "In case of errors during the Data Lake creation, the URI parameter will be null."
)
@JsonInclude(JsonInclude.Include.ALWAYS)
@JsonPropertyOrder({
        "URI",
        "message"
})
public class DataLakeStatus implements Serializable
{

    public final static String SUCCESS_MSG_DEFAULT = "Success.";

    @ApiModelProperty(value = "URI that points to a Data Lake", required = true, example = "file:///opt/isi/datalakebuffer/1a3d88d1-07fc-47b6-8579-5c07107b9302")
    @JsonInclude(JsonInclude.Include.ALWAYS)
    @JsonProperty("URI")
    @Nullable
    private String URI;

    @ApiModelProperty(value = "Outcome message of the Data Lake creation operation.", required = true)
    @JsonInclude(JsonInclude.Include.ALWAYS)
    @JsonProperty("message")
    private String message;

    /**
     * No args constructor for use in serialization
     *
     */
    public DataLakeStatus() {
    }

    /**
     * Create a new {@link DataLakeStatus} for a successful DL creation.
     * (Will set the 'message' param of the {@link #DataLakeStatus(String, String) full constructor} to {@link #SUCCESS_MSG_DEFAULT})
     *
     * @param uri the {@link DataLake} URI or {@code null} (if an error occurred)
     */
    public DataLakeStatus(String uri) {
        this(uri, SUCCESS_MSG_DEFAULT);
    }

    /**
     * Create a new {@link DataLakeStatus} for a DL creation with a custom message.
     *
     * @param uri the {@link DataLake} URI or {@code null} (if an error occurred)
     * @param message the outcome of the {@link DataLake#createDL() createDL()} operation
     */
    public DataLakeStatus(@Nullable String uri, String message) {
        super();
        this.URI = uri;
        this.message = message;
    }

    /**
     * Get the {@link DataLake}'s URI
     * @return a String containing the URI of a Data Lake, or {@code null} if the DL could not be created.
     */
    @Nullable
    @JsonProperty("URI")
    public String getURI() {
        return URI;
    }

    /**
     * Change the value of the Data Lake URI
     * @param uri the URI of a Data Lake, or {@code null}
     */
    @JsonProperty("URI")
    public void setURI(@Nullable String uri) {
        this.URI = uri;
    }

    /**
     * Get the outcome message of tne {@link DataLake#createDL() createDL()} operation
     */
    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

}