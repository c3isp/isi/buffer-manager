package io.chino.c3isp.buffermanager.restapi.types.datalake.hdfs;

import io.chino.c3isp.buffermanager.restapi.types.datalake.mysql.Queries;
import org.apache.hadoop.fs.Path;

import java.sql.*;

public class Impala {
    private PreparedStatement insertStmt = null;
    private String table;
    private Connection db;

    public static void initDatabase(Connection impala, String dbName) throws SQLException {
        PreparedStatement createStatement = impala.prepareStatement(
                "CREATE DATABASE IF NOT EXISTS " + dbName + ";"
        );
        createStatement.execute();
    }

    public static boolean tableExists(Connection impala, String database, String table) throws SQLException {
        Queries.validateName(table);
        PreparedStatement showTables = impala.prepareStatement("SHOW TABLES in " + database + " LIKE '" + table + "';");
        ResultSet results = showTables.executeQuery();
        // this returns a boolean if the next (i.e. at least one) row exists in the ResultSet.
        // It does not return the actual row.
        return results.next();
    }

    public static void createTable(Connection impala, String table, String ... columns) throws SQLException {
        Queries.validateName(table);
        String schema = impala.getSchema();
        Queries.validateName(schema);
        // create Data Lake table in database
        String CREATE_CSV_TABLE = "CREATE TABLE " + schema + "." + table +
                " (" + columnList(columns) + ") " +
                "row format delimited fields terminated by ',';";
        PreparedStatement stm = impala.prepareStatement(CREATE_CSV_TABLE);
        stm.executeUpdate();
        // instruct Impala to skip the first line in the CSV file, which contains the header names
        String SKIP_CSV_HEADERS = "ALTER TABLE " + schema + "." + table + " " +
                "set tblproperties('skip.header.line.count'='1');";
        stm = impala.prepareStatement(SKIP_CSV_HEADERS);
        stm.executeUpdate();

        stm.close();
    }

    /**
     * Load data from a HDFS **file OR folder** into an Impala table.
     *
     * @param impala a {@link Connection} to an Impala DB
     * @param sourcePath {@link Path} of a file or folder in HDFS
     * @param table the name of the destination table
     *
     * @throws SQLException
     */
    public static void loadData(Connection impala, Path sourcePath, String table) throws SQLException {
        if (!sourcePath.isAbsolute()) {
            throw new IllegalArgumentException("The path to the Data Lake must be absolute!");
        }
        Queries.validateName(table);
        String tableIdentifier = impala.getSchema() + "." + table;
        // load data
        String LOAD_CSV_DATA = "LOAD DATA INPATH '" + sourcePath.toUri().getPath() + "' " +
                "into table " + tableIdentifier + ";";
        PreparedStatement stm = impala.prepareStatement(LOAD_CSV_DATA);
        stm.executeUpdate();
        stm.close();
        // update table stats
        stm = impala.prepareStatement("COMPUTE STATS " + tableIdentifier + ";");
        stm.executeQuery();
        stm.close();
    }

    public static void dropTable(Connection impala, String table) throws SQLException {
        Queries.validateName(table);

        String DROP_CSV_TABLE = "DROP TABLE " + impala.getSchema() + "." + table + ";";

        // create Data Lake table in database
        Statement stm = impala.createStatement();
        stm.executeUpdate(DROP_CSV_TABLE);
        stm.close();
    }

    public static String getSelectQueryString(String database, String table) {
        return "SELECT * FROM " + database + "." + table + ";";
    }

    private static String columnList(String[] names) {
        StringBuilder namesList = new StringBuilder(
                (names.length > 0)
                        ? getValidColumnName(names[0]) + " STRING"
                        : ""
        );
        for (int i=1; i < names.length; i ++) {
            namesList.append(", ")
                    .append(getValidColumnName(names[i]))
                    .append(" STRING");
        }
        return namesList.toString();
    }

    public static String getValidColumnName(String s) {
        return s.replace(" ", "_").replace(".", "__");
    }
}
