package io.chino.c3isp.buffermanager.restapi.types.requests;

import javax.validation.constraints.NotNull;

/**
 * Represent the possible types of Data Lake:<br>
 *     {@link #DLB}: Data Lake Buffer<br>
 *     {@link #VDL}: Virtual Data Lake<br>
 */
public enum DataLakeType {
    /**
     * Data Lake Buffer
     */
    DLB,
    /**
     * Virtual Data Lake
     */
    VDL;

    /**
     * Get a {@link DataLakeType} whose name matches the parameter {@link String}
     *
     */
    public DataLakeType fromString(@NotNull String dataLakeTypeString) {
        if (dataLakeTypeString == null)
            throw new NullPointerException(
                    "Parameter dataLakeTypeString cannot be 'null'"
            );

        if (dataLakeTypeString.toUpperCase().equals(DLB.toString()))
            return DLB;

        if (dataLakeTypeString.toUpperCase().equals(VDL.toString()))
            return VDL;

        throw new IllegalArgumentException(
                "Parameter dataLakeTypeString must be either 'DLB' or 'VDL'\n" +
                "(found '" + dataLakeTypeString + "' instead)"
        );
    }
}
