package io.chino.c3isp.buffermanager.restapi.types.datalake;

import io.chino.c3isp.buffermanager.cfg.BaseConfigurer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.util.Date;
import java.util.LinkedList;

public class DemoDataLake extends DataLake {
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoDataLake.class);

    public final static String DEMO_URI = "DEMO: no Data Lake was created.";
    final static String URI_PROTOCOL = "DEMO";
    private boolean success = false;

    DemoDataLake() {
        super(new BaseConfigurer() {
            @Override public BaseConfigurer init() {
                return this;
            }
        });
    }

    @Override
    protected void initConfig(BaseConfigurer instanceConfigurer) {}

    @Override
    protected void init() {}

    @Override
    public String createDL() throws RemoteDataLakeException {
        setURIValue(DEMO_URI);
        return dataLakeURI;
    }

    @Override
    public String writeDPO(String fileName, String newElement) throws RemoteDataLakeException {
        success = !success;
        if (success)
            return fileName + " (DEMO response)";
        else
            throw new RemoteDataLakeException(HttpStatus.valueOf(404), "Not found (DEMO)");
    }

    @Override
    public void deleteDL() throws RemoteDataLakeException {
    }

    @Override
    public void disconnect() {
        LOGGER.info("Released instance of DemoDataLake.");
    }

    @Override
    public Date getCreationDate() {
        return new Date(0);
    }

    @Override
    public boolean hasExpired() {
        return true;
    }

    @Override
    LinkedList<String> all() {
        return new LinkedList<>();
    }

}
