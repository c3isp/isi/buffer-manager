package io.chino.c3isp.buffermanager.restapi.types.requests;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Represents the JSON object that is sent in the body of the POST API call for 'prepareData'
 *
 * @author Andrea Arighi [andrea@chino.io]
 */
@ApiModel(value = "query",
        description = "The actual parameters for the 'prepareData' request."
)
public class PrepareDataQuery implements Serializable {

    @ApiModelProperty(value = "List of IDs of the DPOs that will be included in the new Data Lake.", required = true)
    @JsonProperty("DPO_id_list")
    private List<String> DPOIdList = null;

    @ApiModelProperty(value = "Query metadata, passed as a JSON object with customizable field names.")
    @JsonProperty("metadata")
    private String queryMetadata = null;

    @ApiModelProperty(value = "Name of the analytic the Data Lake is prepared for.", required = true, example = "spamEmailClassify")
    @JsonProperty("service_name")
    private String serviceName;

    @ApiModelProperty(value = "Destination format of the data contained in the Data Lake.", required = true, example="EML") // allowableValues are read from enum Format
    @JsonProperty("format")
    private Format dataFormat;

    @ApiModelProperty(value = "Type of the Data Lake that needs to be created.", required = true, example = "DLB") // allowableValues are read from enum DataLakeType
    @JsonProperty("data_lake")
    private DataLakeType dataLake;

    @ApiModelProperty(value = "Type of filesystem (and thus of URI) where the Data Lake must be created.", required = true, example="FS") // allowableValues are read from enum DataLakeFileSystemType
    @JsonProperty("type")
    private DataLakeFileSystemType fileSystem;

    /**
     * No args constructor for use in serialization
     */
    private PrepareDataQuery() {
    }

    public PrepareDataQuery(List<String> dpoIdList, String metadata, String serviceName,
                            Format dataDestinationFormat, DataLakeType dataLakeType,
                            DataLakeFileSystemType fileSystem
    ) {
        setDPOIdList(dpoIdList);
        queryMetadata = metadata;
        setServiceName(serviceName);
        setDataFormat(dataDestinationFormat);
        setDataLake(dataLakeType);
        setFileSystem(fileSystem);
    }

    @JsonProperty("metadata")
    public String getMetadata() {
        return queryMetadata;
    }

    @JsonProperty("metadata")
    public void setMetadata(String queryMetadata) {
        this.queryMetadata = queryMetadata;
    }

    @JsonProperty("service_name")
    public String getServiceName() {
        return serviceName;
    }

    @JsonProperty("service_name")
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    @JsonProperty("format")
    public Format getDataFormat() {
        return dataFormat;
    }

    @JsonProperty("format")
    public void setDataFormat(Format dataFormat) {
        this.dataFormat = dataFormat;
    }

    @JsonProperty("data_lake")
    public DataLakeType getDataLake() {
        return dataLake;
    }

    @JsonProperty("data_lake")
    public void setDataLake(DataLakeType dataLake) {
        this.dataLake = dataLake;
    }

    @JsonProperty("type")
    public DataLakeFileSystemType getFileSystem() {
        return fileSystem;
    }

    @JsonProperty("type")
    public void setFileSystem(DataLakeFileSystemType fileSystem) {
        this.fileSystem = fileSystem;
    }

    @JsonProperty("DPO_id_list")
    public List<String> getDPOIdList() {
        return DPOIdList;
    }

    @JsonProperty("DPO_id_list")
    public void setDPOIdList(List<String> dPOIdList) {
        this.DPOIdList = dPOIdList;
    }
}
