package io.chino.c3isp.buffermanager.restapi.types.datalake;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;

import java.io.IOException;

/**
 * Exception class that will be thrown if there are problems during the communication with a remote Data Lake.
 */
public class RemoteDataLakeException extends HttpStatusCodeException {

    public RemoteDataLakeException() {
        super(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public RemoteDataLakeException(HttpStatus statusCode, String message) {
        super(statusCode, message);
    }

    @Override
    public String getMessage() {
        String msg = super.getStatusCode().value() + " " + super.getStatusCode().getReasonPhrase();
        if (super.getStatusText() != null && ! super.getStatusText().isEmpty()) {
            msg += ": " + super.getStatusText();
        }
        return msg;
    }
}
