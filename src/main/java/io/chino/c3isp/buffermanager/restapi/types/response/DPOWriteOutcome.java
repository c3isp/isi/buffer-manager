
package io.chino.c3isp.buffermanager.restapi.types.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * Represents the outcome of a read DPO operation from the ISI.
 * The class holds information about the server's response that can be used to
 * locate the DPO after a successful read or to handle the error that may have occurred.
 *
 * @author Andrea Arighi [andrea@chino.io]
 */
@ApiModel(value = "DPOWriteOutcome",
        description = "Displays information about the outcome of a DPO read and write operation."
)
@JsonInclude(JsonInclude.Include.ALWAYS)
@JsonPropertyOrder({
    "DPO_id",
    "code",
    "message",
    "file"
})
public class DPOWriteOutcome implements Serializable
{

    @ApiModelProperty(value = "ID of a DPO that was sent within the request", required = true,
            example = "1544607834804-fbcb6fb9-a011-0516-9d36-b77de41657bd")
    @JsonProperty("DPO_id")
    private String DPOId;

    @ApiModelProperty(value = "The status code for this DPO", required = true, example = "201")
    @JsonProperty("code")
    private int code;

    @ApiModelProperty(value = "A brief description of the write operation's outcome. In case of failure will contain " +
            "a description of the error", required = true, example = "Success.")
    @JsonProperty("message")
    private String message;

    @ApiModelProperty(value = "In case of success, will contain either a URI or a SQL query to get the DPO. " +
            "In case of failure will be set to \"null\"", required = true,
            example = "file:///opt/isi/datalakebuffer/5ee94fa3-c408-43c9-a031-588f03240f1e/1544607834804-fbcb6fb9-a011-0516-9d36-b77de41657bd.dpo")
    @JsonProperty("file")
    private String fileLocator;

    /**
     * No args constructor for use in serialization
     *
     */
    private DPOWriteOutcome() {
    }

    /**
     * Create a new instance of {@link DPOWriteOutcome} for a <b>successful</b> DPO read
     *
     * @param DPOid The ID of the DPO who could not be read
     * @param HTTPStatusCode the status code sent by the server, or {@code -1} if no response was received.
     * @param message A brief description of the error
     * @param dpoURI the URI returned by a successful call to {@code readDPO}
     */
    public DPOWriteOutcome(String DPOid, int HTTPStatusCode, String message, String dpoURI) {
        super();
        this.DPOId = DPOid;
        this.code = HTTPStatusCode;
        this.message = message;
        this.fileLocator = dpoURI;
    }

    /**
     * Create a new instance of {@link DPOWriteOutcome} for an <b>unsuccessful</b> DPO read.
     * The DPO URI will be set to null by default
     *
     * @param DPOid The ID of the DPO who could not be read
     * @param statusCode the status code sent by the server, or {@code -1} if no response was received.
     * @param message A brief description of the error
     */
    public DPOWriteOutcome(String DPOid, HttpStatus statusCode, String message) {
        this(DPOid, statusCode.value(), message, null);
    }

    /**
     * Create a new instance of {@link DPOWriteOutcome} for a <b>successful</b> DPO read
     *
     * @param DPOid      The ID of the DPO who could not be read
     * @param statusCode the status code sent by the server, or {@code -1} if no response was received.
     * @param message    A brief description of the error
     * @param dpoURI     the URI returned by the successful call to {@code readDPO}
     */
    public DPOWriteOutcome(String DPOid, HttpStatus statusCode, String message, String dpoURI) {
        this(DPOid, statusCode.value(), message, dpoURI);
    }

    /**
     * Create a new instance of {@link DPOWriteOutcome} for an <b>unsuccessful</b> DPO read.
     * The DPO URI will be set to null by default
     *
     * @param DPOid The ID of the DPO who could not be read
     * @param HTTPStatusCode the status code sent by the server, or {@code -1} if no response was received.
     * @param message A brief description of the error
     */
    public DPOWriteOutcome(String DPOid, int HTTPStatusCode, String message) {
        this(DPOid, HTTPStatusCode, message, null);
    }

    @JsonProperty("DPO_id")
    public String getDPOId() {
        return DPOId;
    }

    @JsonProperty("DPO_id")
    public void setDPOId(String DPOId) {
        this.DPOId = DPOId;
    }

    @JsonProperty("code")
    public int getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(int code) {
        this.code = code;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("file")
    public String getFileLocator() {
        return fileLocator;
    }

    @JsonProperty("file")
    public void setFileLocator(String fileLocator) {
        this.fileLocator = fileLocator;
    }


    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof PrepareDataResponse)) {
            return false;
        }

        DPOWriteOutcome fail = (DPOWriteOutcome) other;

        return fail.DPOId.contentEquals(this.DPOId) &&
                fail.code == this.code &&
                fail.message.equals(this.message);
    }
}
