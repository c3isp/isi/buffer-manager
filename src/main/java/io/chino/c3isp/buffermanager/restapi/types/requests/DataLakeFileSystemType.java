package io.chino.c3isp.buffermanager.restapi.types.requests;

import javax.validation.constraints.NotNull;

/**
 * Represents the type of file system that is used to organize data in a
 * {@link io.chino.c3isp.buffermanager.restapi.types.datalake.DataLake DataLake}.<br>
 * <br>
 *     {@link #HDFS}: the DataLake uses Hadoop file system<br>
 *     {@link #FS}: the DataLake uses a standard (Windows) file system<br>
 *     {@link #MYSQL}: the DataLake uses a MySQL database<br>
 */
public enum DataLakeFileSystemType {
    /**
     * the DataLake uses Hadoop file system.<br>
     *     Implementation:
     *     none
     */
    HDFS,
    /**
     * the DataLake uses a standard (Windows) file system.<br>
     *     Implementation:
     *     {@link io.chino.c3isp.buffermanager.restapi.types.datalake.FileSystemDataLake FileSystemDataLake}
     */
    FS,
    /**
     * the DataLake uses a MySQL database.<br>
     *     Implementation:
     *     none
     */
    MYSQL;

    /**
     * Get a {@link DataLakeFileSystemType} whose name matches the parameter {@link String}
     *
     */
    public DataLakeFileSystemType fromString(@NotNull String fileSystemTypeString) {
        if (fileSystemTypeString == null)
            throw new NullPointerException(
                    "Parameter formatTypeName cannot be 'null'"
            );

        switch (fileSystemTypeString.toUpperCase().trim()) {
            case "HDFS":
                return HDFS;
            case "FS":
                return FS;
            case "MYSQL":
                return MYSQL;
            default:
                throw new IllegalArgumentException(
                        "Unknown Data Lake filesystem type: '" + fileSystemTypeString + "'"
                );
        }
    }
}
