package io.chino.c3isp.buffermanager.restapi.types.datalake;

import io.chino.c3isp.buffermanager.cfg.BaseConfigurer;
import io.chino.c3isp.buffermanager.cfg.MySQLConfig;
import io.chino.c3isp.buffermanager.restapi.types.datalake.mysql.Queries;
import io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeType;
import io.chino.c3isp.buffermanager.scheduled.CleanupTasks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

import java.sql.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeType.VDL;

@Configuration
public class MySQLVirtualDataLake extends MySQLDataLakeBuffer {

    private static Logger LOGGER = LoggerFactory.getLogger(MySQLVirtualDataLake.class);
    private String DB_SERVER_URL;

    private String databaseName;

    /* CONFIGURATION LOADED FROM application.properties */
    /**
     * MySQL db configuration, initialized in package-private constructor
     */
    static BaseConfigurer classDefaultConfigurer;

    @Autowired
    MySQLVirtualDataLake(MySQLConfig classDefaultConfig) {
        super(classDefaultConfig.forType(VDL));
        // when Spring initialization occurs, store the configurer in the static pool
        // so it can be reused when this class is created using 'new'
        classDefaultConfigurer = classDefaultConfig.forType(VDL).init();
    }

    public MySQLVirtualDataLake(String analyticsName) {
        // init user and password for Data Lake user (for createDL)
        super(analyticsName);
    }

    public MySQLVirtualDataLake(String username, String password, String URI) {
        // init Data Lake user credentials (for deleteDL)
        super(username, password);
        setURIValue(URI);
    }

    @Override
    protected void initConfig(BaseConfigurer instanceConfigurer) {
        super.initConfig(instanceConfigurer);
        DB_SERVER_URL = DB_SERVER_BASE_URL + DB_TIMEZONE_FIX;
    }

    public static String getDBName() {
        return new MySQLDataLakeBuffer("getDBName").DB_SERVER_BASE_URL;
    }

    @Override
    public void setURIValue(String thisURI) {
        // delete last '/' char in the URI, if any - otherwise it is interpreted by JDBC as part of the DB name
        super.setURIValue(thisURI.replace("/?", "?"));
        String[] URLTokens = thisURI.split("\\?")[0]    // discard URL params
                .split("/");        // tokenize URL
        databaseName = URLTokens[URLTokens.length - 1];        // keep last token
    }

    public DataLakeType getDataLakeType() {
        return VDL;
    }

    @Override
    protected void init() {
        try {
            dbConnection = DriverManager.getConnection(
                    DB_SERVER_URL,
                    rootUser,
                    rootPassword
            );
        } catch (SQLTimeoutException e) {
            LOGGER.error("Connection timeout", e);
            throw new RemoteDataLakeException(HttpStatus.GATEWAY_TIMEOUT, "Unable to connect to remote database '" +
                    DB_SERVER_URL +
                    "'. Reason: request timeout.");
        } catch (Exception e) {
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to connect to remote database '" +
                    DB_SERVER_URL +
                    "'. Check server logs for more information about the error.");
        }
    }

    @Override
    public String createDL() throws RemoteDataLakeException {
        String dbName = "vdl_" + dataLakeUser;

        String action = "create database for VDL";
        try {
            Queries.createDatabase(dbConnection, dbName);

        } catch (SQLTimeoutException e) {
            LOGGER.error("error (timeout)", e);
            throw new RemoteDataLakeException(HttpStatus.GATEWAY_TIMEOUT, "Remote SQL Data Lake took too long to respond.");
        } catch (SQLSyntaxErrorException e) {
            LOGGER.error("error (syntax)", e);
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR, "There was an error with the server. Please check the logs for more information or contact an admin.");
        } catch (SQLException e) {
            try {
                Queries.dropDatabase(dbConnection, dbName);
            } catch (SQLException nestedException) {
                e.setNextException(nestedException);
            }
            LOGGER.error("error (SQL) during ' " + action + "' action", e);
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE, "Unable to create Data Lake on remote SQL server");
        }

        databaseName = dbName;

        String tableURI;
        try {
            tableURI = super.createDL();
            String tableName = DataLakeManager.getUrlParam(tableURI, "table");

            return buildUriFromTable(tableName);
        } catch (RemoteDataLakeException e) {
            try {
                Queries.dropDatabase(dbConnection, dbName);
            } catch (SQLException nestedException) {
                LOGGER.error("Unable to delete VDL after error", nestedException);
            }
            throw e;
        }
    }

    @Override
    public String writeDPO(String fileName, String newElement) throws RemoteDataLakeException {
        try {
            dbConnection.setCatalog(databaseName);
        } catch (SQLException e) {
            LOGGER.error("Unable to connect to database '" + databaseName + "' (writeDPO)", e);
        }

        String retrieveDPOQuery = super.writeDPO(fileName, newElement);

        return retrieveDPOQuery.replace("FROM table_", "FROM " + databaseName + ".table_");
    }

    @Override
    public void deleteDL() throws RemoteDataLakeException {
        String action = "delete database";
        try {
            if (!dataLakeURI.contains(DB_TIMEZONE_FIX)) {
                // append parameters delimiter
                dataLakeURI += (dataLakeURI.contains("?") ? "&" : "?");
                // append string to fix the unrecognized timezone bug
                dataLakeURI += DB_TIMEZONE_FIX.replace("?", "");
            }

            if (!exists(dbConnection, databaseName))
                throw new RemoteDataLakeException(HttpStatus.NOT_FOUND,
                        "Data Lake " + databaseName + " doesn't exist, or it may be expired.");

            Connection userConnection = DriverManager.getConnection(
                    dataLakeURI,
                    dataLakeUser,
                    dataLakePassword
            );

            Queries.emptyDatabase(userConnection, databaseName); // delete DB content as user
            Queries.dropDatabase(dbConnection, databaseName); // delete empty DB as root
            // delete user as root
            Queries.dropUser(dbConnection,
                    VDL,
                    DataLakeManager.getUrlParam(dataLakeURI, "usr"),
                    getConfigProperty("host")
            );
        } catch (SQLTimeoutException e) {
            LOGGER.error("error (timeout)", e);
            throw new RemoteDataLakeException(HttpStatus.GATEWAY_TIMEOUT,
                    "Remote SQL Data Lake took too long to respond.");
        } catch (SQLException e) {
            if (e instanceof SQLSyntaxErrorException) {
                LOGGER.error("SQL Syntax error", e);
            } else {
                LOGGER.error("error (SQL) during ' " + action + "' action", e);
            }
            if (e.getMessage().contains("Access denied for user")) {
                throw new RemoteDataLakeException(HttpStatus.UNAUTHORIZED,
                        "Access denied to user " + dataLakeUser + ".");
            } else if (e.getMessage().contains("Unknown database")) {
                throw new RemoteDataLakeException(HttpStatus.NOT_FOUND,
                        "Virtual Data Lake not found: " + databaseName + ". It may be expired.");
            }
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE,
                    "Unable to delete Data Lake from remote SQL server");
        } catch (RemoteDataLakeException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error("error (" + e,getClass().getName() + ") during ' " + action + "' action", e);
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "Unable to delete Data Lake from remote SQL server");
        }
    }

    @Override
    void forceDeleteDL(CleanupTasks trigger) throws RemoteDataLakeException {
        // This method may only be called by an instance of CleanupTasks,
        // otherwise it will do nothing
        if (trigger == null) {
            LOGGER.warn("MySQLVirtualDataLake.forceDeleteDL was called by an invalid caller.");
            return;
        }
        try {
            Queries.dropDatabase(dbConnection, databaseName);
        } catch (SQLTimeoutException e) {
            LOGGER.error("error (timeout)", e);
            throw new RemoteDataLakeException(HttpStatus.GATEWAY_TIMEOUT,
                    "Remote SQL Data Lake took too long to respond."
            );
        } catch (SQLSyntaxErrorException e) {
            LOGGER.error("error (syntax)", e);
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR, "There was an error with the server. " +
                    "Please check the logs for more information or contact an admin."
            );
        } catch (SQLException e) {
            LOGGER.error("Unable to get list of DataLakes from remote SQL server", e);
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE, "Unable to force delete DataLakes from " +
                    "remote SQL server");
        }
    }


    /**
     * Return a URI that uniquely identifies this Data Lake.
     *
     * @param tableName the name of the table in the MySQL {@link DataLake}
     *
     * @return a URI that identifies the specified table
     */
    public String buildUriFromTable(String tableName) {
        return DB_SERVER_BASE_URL
                + databaseName
                + DB_TIMEZONE_FIX
                + "&usr=" + dataLakeUser
                + "&psw=" + dataLakePassword
                + "&table=" + tableName;
    }

    @Override
    public Date getCreationDate() throws Exception {
        String action = "get creation Date";
        try {
            Queries.MySQLMetadata md = Queries.getMetadata(dbConnection, databaseName, getDataLakeType());
            return md.getCreationDate();
        } catch (SQLTimeoutException e) {
            LOGGER.error("error (timeout)", e);
            throw new RemoteDataLakeException(HttpStatus.GATEWAY_TIMEOUT,
                    "Remote SQL Data Lake took too long to respond.");
        } catch (SQLSyntaxErrorException e) {
            LOGGER.error("error (syntax)", e);
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR, "There was an error with the server. " +
                    "Please check the logs for more information or contact an admin.");
        } catch (SQLException e) {
            LOGGER.error("error (SQL) during ' " + action + "' action", e);
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE, "Unable to read Data Lake creation date " +
                    "'" + databaseName + "' from remote SQL server");
        }
    }

    @Override
    public Date getLastUpdate() {
        String action = "get last update Date";
        try {
            Queries.MySQLMetadata md = Queries.getMetadata(dbConnection, databaseName, getDataLakeType());
            return md.getLastUpdateDate();
        } catch (SQLTimeoutException e) {
            LOGGER.error("error (timeout)", e);
            throw new RemoteDataLakeException(HttpStatus.GATEWAY_TIMEOUT,
                    "Remote SQL Data Lake took too long to respond.");
        } catch (SQLSyntaxErrorException e) {
            LOGGER.error("error (syntax)", e);
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR, "There was an error with the server. " +
                    "Please check the logs for more information or contact an admin.");
        } catch (SQLException e) {
            LOGGER.error("error (SQL) during ' " + action + "' action", e);
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE, "Unable to read Data Lake creation date "+
                    "'" + databaseName + "' from remote SQL server");
        }
    }

    public static boolean exists(Connection connection, String dbName) {
        String action = "fetch";
        try {
            ResultSet rs = connection.getMetaData().getCatalogs();
            action = "read";
            boolean hasNext = rs.next();
            while (hasNext) {
                try {
                    if (rs.getNString(1).equals(dbName))
                        return true;
                    hasNext = rs.next();
                } catch (SQLException e) {
                    LOGGER.error("failed to read next element of Database metadata");
                }
            }
            action = "close";
            rs.close();
        } catch (SQLException e) {
            LOGGER.error("failed to " + action + " Database metadata");
        }
        return false;
    }

    /**
     * Get the name of the database that contains this this dataLake
     *
     * @return the database name of this {@link MySQLVirtualDataLake}. <br>
     * <b>WARNING</b>: returns {@code null} if the VDL has not been created yet.
     */
    @Override
    public String getDataLakeName() {
        return databaseName;
    }

    @Override
    LinkedList<String> all() throws RemoteDataLakeException {
        LinkedList<String> names = new LinkedList<>();
        try {
            // read the names of VDL databases
            List<String> databases = Queries.listVDLTables(dbConnection, "vdl_");
            // set root credentials to be used in buildUriFromTable()
            this.dataLakeUser = rootUser;
            this.dataLakePassword = rootPassword;
            // store current value
            String tmp = this.databaseName;

            // populate list with the URIs built from the database names
            for (String databaseName : databases) {
                // this value is required to build the correct URI
                this.databaseName = databaseName;
                names.add(buildUriFromTable("none"));
            }
            // restore previous value
            this.databaseName = tmp;
        } catch (SQLTimeoutException e) {
            LOGGER.error("error (timeout)", e);
            throw new RemoteDataLakeException(HttpStatus.GATEWAY_TIMEOUT,
                    "Remote SQL Data Lake took too long to respond."
            );
        } catch (SQLSyntaxErrorException e) {
            LOGGER.error("error (syntax)", e);
            throw new RemoteDataLakeException(HttpStatus.INTERNAL_SERVER_ERROR, "There was an error with the server. " +
                    "Please check the logs for more information or contact an admin."
            );
        } catch (SQLException e) {
            LOGGER.error("Unable to get list of DataLakes from remote SQL server", e);
            throw new RemoteDataLakeException(HttpStatus.SERVICE_UNAVAILABLE, "Unable to get list of DataLakes from " +
                    "remote SQL server");
        }

        return names;
    }
}
