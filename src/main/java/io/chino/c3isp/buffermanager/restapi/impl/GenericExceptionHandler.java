package io.chino.c3isp.buffermanager.restapi.impl;

import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.context.request.RequestAttributes;

import java.util.Map;

@Component
public class GenericExceptionHandler extends DefaultErrorAttributes {

    public static String[] hiddenErrorFields = {
            "timestamp",
            "exception"
    };

    @Override
    public Map<String, Object> getErrorAttributes(RequestAttributes requestAttributes, boolean includeStackTrace) {
        final Map<String, Object> attrs = super.getErrorAttributes(requestAttributes, includeStackTrace);

        for (String field : hiddenErrorFields) {
            attrs.remove(field);
        }

        Throwable error = super.getError(requestAttributes);

        if (error instanceof HttpStatusCodeException) {
            int status = ((HttpStatusCodeException) error).getStatusCode().value();
            String errorMsg = ((HttpStatusCodeException) error).getStatusCode().getReasonPhrase();

            attrs.put("status", status);
            attrs.put("error", errorMsg);
        }

        return attrs;
    }
}
