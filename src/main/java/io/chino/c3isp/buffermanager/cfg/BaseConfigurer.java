package io.chino.c3isp.buffermanager.cfg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

/**
 * Wrap properties of a Data Lake so that they can be loaded with
 * {@link org.springframework.beans.factory.annotation.Value @Value}
 * from application.properties and re-used whenever required.<br>
 * <br>
 * Create a subclass for each DataLake that uses a different set of properties.
 * Properties values are loaded from {@code application.properties}<br>
 * <br>
 *     <b>&gt;&gt;&gt; How to implement this class &lt;&lt;&lt;</b>
 *     <ol>
 *         <li>
 *             Define the names of the properties in application.properties.
 *             Best practice is to use a common prefix for all properties related to the same
 *             {@link BaseConfigurer} (e.g. all the properties for the FileSystemDataLake start with 'data.fs')
 *         </li>
 *         <li>
 *             Create a variable for each property that you want to load in this {@link BaseConfigurer}.
 *             Use the {@link org.springframework.beans.factory.annotation.Value @Value} annotation
 *             to load the property value.
 *         </li>
 *         <li>
 *             Load them as key-value pairs in {@link #init()}.
 *         </li>
 *     </ol>
 */
public abstract class BaseConfigurer {

    private final static Logger LOGGER = LoggerFactory.getLogger("Configuration");

    private final HashMap<String, String> properties = new HashMap<>();

    /**
     * Load values in this Configurer's properties map.<br>
     * If not already done, follow {@link BaseConfigurer these instructions} to load the properties in class fields.
     * Then implement this method, adding a key-value pair to this Configurer with
     * {@link #addProperty(String, String)}.<br>
     *     <br>
     *     <b>Example:</b> load property 'data.fs.root' in {@link FileSystemConfig FileSystemConfig}
     * <pre><code>
     *{@literal @Value}("data.fs.root")
     * public String fsRoot;
     *
     * public FileSystemConfig init() {
     *      addProperty("root", fsRoot);
     *      return this;
     * }
     * </code></pre>
     *
     */
    public abstract BaseConfigurer init();

    /**
     * Read the value of the property with the provided name from the properties map.
     *
     * @param name the property name
     * @return the property value, or {@code null} if no property with the provided name exists
     */
    public String getProperty(String name) {
        return properties.getOrDefault(name, null);
    }

    /**
     * Bind the value of a parameter to a key and store it in this object's properties
     *
     * @param name the key that refers to the parameter value
     * @param value the parameter value
     */
    protected void addProperty(String name, String value) {
        properties.put(name, value);
        LOGGER.debug(String.format("%s: Added property %s -> %s", getClass().getName(), name, value));
    }
}
