package io.chino.c3isp.buffermanager.cfg;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HDFSConfig extends BaseConfigurer {

    @Value("${data.hdfs.uri}")
    private String uri;
    @Value("${data.hdfs.root}")
    private String root;
    @Value("${data.hdfs.chmod}")
    private String permissions;
    @Value("${data.hdfs.user.name}")
    private String userName;
    @Value("${data.hdfs.impala.uri}")
    private String impalaUri;
    @Value("${data.hdfs.impala.user.name}")
    private String impalaUsername;
    @Value("${data.hdfs.impala.user.password}")
    private String impalaPassword;
    @Value("${data.hdfs.impala.database}")
    private String impalaDatabase;
    @Value("${data.hdfs.delete-after}")
    private String expiresIn;

    @Override
    public HDFSConfig init() {
        addProperty("uri", uri);
        addProperty("root", root);
        addProperty("chmod", permissions);
        addProperty("user.name", userName);
        addProperty("impala.uri", impalaUri);
        addProperty("impala.username", impalaUsername);
        addProperty("impala.password", impalaPassword);
        addProperty("impala.database", impalaDatabase);
        addProperty("expires-in", expiresIn);

        return this;
    }
}
