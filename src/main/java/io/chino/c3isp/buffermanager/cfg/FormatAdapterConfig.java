package io.chino.c3isp.buffermanager.cfg;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FormatAdapterConfig extends BaseConfigurer {

    @Value("${rest.endpoint.url.c3isp.format-adapter}")
    private String faUrl;

    @Override
    public FormatAdapterConfig init() {
        addProperty("url", faUrl);

        return this;
    }
}
