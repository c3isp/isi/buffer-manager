package io.chino.c3isp.buffermanager.cfg;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FileSystemConfig extends BaseConfigurer {

    @Value("${data.filesystem.root}")
    private String fsRoot;
    @Value("${data.filesystem.delete-after}")
    private String expiresIn;

    @Override
    public FileSystemConfig init() {
        addProperty("root", fsRoot);
        addProperty("expires-in", expiresIn);

        return this;
    }
}
