package io.chino.c3isp.buffermanager.cfg;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class IsiApiConfig extends BaseConfigurer {
    @Value("${rest.endpoint.url.c3isp.isi}")
    private String isiEndpoint;
    @Value("${rest.endpoint.url.c3isp.isi.version}")
    private String isiVersion;
    @Value("${rest.endpoint.url.c3isp.isi.user}")
    private String isiBasicUser;
    @Value("${rest.endpoint.url.c3isp.isi.password}")
    private String isiBasicPassword;

    @Override
    public IsiApiConfig init() {
        addProperty("url", isiEndpoint);
        addProperty("version", isiVersion);
        addProperty("username", isiBasicUser);
        addProperty("password", isiBasicPassword);

        return this;
    }
}
