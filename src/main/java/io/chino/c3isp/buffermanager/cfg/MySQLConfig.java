package io.chino.c3isp.buffermanager.cfg;

import io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import static io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeType.DLB;

@Configuration
public class MySQLConfig extends BaseConfigurer {

    private DataLakeType type = null;

    public class UnmutableConfig extends BaseConfigurer {
        private boolean initialized = false;
        /**
         * Does nothing. The content of this {@link BaseConfigurer} is unmutable.
         */
        @Override protected void addProperty(String name, String value) {
            if (!initialized) super.addProperty(name, value);
        }
        /**
         * Does nothing. The content of this {@link BaseConfigurer} is unmutable.
         */
        @Override public UnmutableConfig init() {
            initialized = true;
            return this;
        }
    }

    @Value("${data.mysql.dlb.url}")
    private String dlbUrl;
    @Value("${data.mysql.dlb.host}")
    private String dlbHost;
    @Value("${data.mysql.dlb.database}")
    private String dlbName;
    @Value("${data.mysql.dlb.user.name}")
    private String dlbUser;
    @Value("${data.mysql.dlb.user.password}")
    private String dlbPassword;
    @Value("${data.mysql.dlb.delete-after}")
    private String dlbExpiration;

    @Value("${data.mysql.vdl.url}")
    private String vdlUrl;
    @Value("${data.mysql.vdl.host}")
    private String vdlHost;
    @Value("${data.mysql.vdl.database}")
    private String vdlName;
    @Value("${data.mysql.vdl.user.name}")
    private String vdlUser;
    @Value("${data.mysql.vdl.user.password}")
    private String vdlPassword;
    @Value("${data.mysql.vdl.delete-after}")
    private String vdlExpiration;

    /**
     * Set the value of {@link #type} to the specified value, unless the value was set by a previous call of
     * {@link #forType(DataLakeType)}.
     * During inheritance, this allows for child classes to decide the Data Lake Type regardless of the parent's code.
     *
     * @param type the {@link DataLakeType} this configuration refers to.
     *
     * @return this object
     */
    public MySQLConfig forType(DataLakeType type) {
        this.type = type;
        return this;
    }

    public DataLakeType getType(){
        return type;
    }

    /**
     * Add values to the this class' properties map.
     * This method must be called after {@link #forType(DataLakeType)}.
     *
     * @see BaseConfigurer#init()
     */
    @Override
    public BaseConfigurer init() {
        return init(type);
    }

    private BaseConfigurer init(DataLakeType type) {
        if (type == null) {
            throw new IllegalStateException("Did you forget to call 'MySQLConfig.forType(DataLakeType)'?");
        }
        UnmutableConfig map = new UnmutableConfig();
        map.addProperty("url", type == DLB ? dlbUrl : vdlUrl);
        map.addProperty("host", type == DLB ? dlbHost : vdlHost);
        map.addProperty("database", type == DLB ? dlbName : vdlName);
        map.addProperty("expires-in", type == DLB ? dlbExpiration : vdlExpiration);
        map.addProperty("user.name", type == DLB ? dlbUser : vdlUser);
        map.addProperty("user.password", type == DLB ? dlbPassword : vdlPassword);
        // lock this map so it can't change
        return map.init();
    }
}
