package io.chino.c3isp.buffermanager.scheduled;

import java.util.LinkedList;

public class HDFSCleanupReport {
    private final LinkedList<String> deleted = new LinkedList<>();
    private final StringBuilder report;

    public HDFSCleanupReport(String name) {
        report = new StringBuilder(name);
    }

//    public void deleted(HDFSDataLake dataLake) {
//        try {
//            deleted.add(dataLake.getWarehouseFilePath());
//        } catch (IOException e) {
//            deleted.add("NOT FOUND");
//        }
//    }

    public LinkedList<String> getDeleted() {
        return deleted;
    }

    public StringBuilder getBuilder() {
        return report;
    }

    @Override
    public String toString() {
        return report.toString();
    }
}
