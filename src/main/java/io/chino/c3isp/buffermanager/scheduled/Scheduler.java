package io.chino.c3isp.buffermanager.scheduled;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;

@Configuration
@Profile("default")
public class Scheduler {

    public static Scheduler getProperties() {
        return new Scheduler();
    }

    @Bean
    public TaskScheduler taskScheduler() {
        return new ConcurrentTaskScheduler();
    }

    /*
    *  CLEANUP TASKS
    *  class: CleanupTasks
    *  properties: scheduled.cleanup.XXX
    */

    @Value("${scheduled.cleanup.report.print}")
    boolean cleanupLoggingEnabled;

    @Bean
    CleanupTasks cleanupTasks() {
        CleanupTasks.enableLogging(cleanupLoggingEnabled);
        return new CleanupTasks();
    }
}
