package io.chino.c3isp.buffermanager.scheduled;

import io.chino.c3isp.buffermanager.restapi.types.datalake.*;
import io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeFileSystemType;
import io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.SubstituteLogger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;

/**
 * In this class are defined the scheduled tasks to periodically
 * cleanup expired DataLakes on various file system types.
 *
 * @see DataLake#hasExpired()
 */
@Component
public class CleanupTasks {

    private static Logger LOGGER;
    private static String loggerName = "BufferManager Scheduled Cleanup";
    // dummy logger
    private static final SubstituteLogger noLogger = new SubstituteLogger(
            loggerName, new PriorityQueue<>(), true
    );

    public static void enableLogging(boolean enable) {
        LOGGER = (!enable) ? noLogger
                :  LoggerFactory.getLogger(loggerName);
    }

    // set as package-private
    CleanupTasks() {
        super();
    }

    //    @Scheduled(cron = "0 0/1 * * * *")    // once every minute at 0 sec - use this to test
    @Scheduled(cron = "0 0 */4 * * *")   // once every 4 hours at minute 0
    void scheduledCleanup_FS() {
        String name = "FileSystemCleanup";
        LOGGER.info("Started task '{}'", name);

        String report = cleanupFileSystem_all(new StringBuilder(name));

        outputReport(report, name);
    }

    //    @Scheduled(cron = "30 0/1 * * * *")    // once every minute at 30 sec - use this to test
    @Scheduled(cron = "0 30 */4 * * *")   // once every 4 hours at minute 30
    void scheduledCleanup_MySQL() {
        String name = "MySQLCleanup";
        LOGGER.info("Started task '{}'", name);

        String reportVDL = cleanupMySQL_VDL(new StringBuilder(name + " VDL"));
        String reportDLB = cleanupMySQL_DLB(new StringBuilder(name + " DLB"));

        outputReport(reportVDL + "\n" + reportDLB, name);
    }

//        @Scheduled(cron = "*/30 * * * * *")    // once per 30s - use this to test
    @Scheduled(cron = "0 15 */4 * * *")   // once every 4 hours at minute 15
    void scheduledCleanup_HDFS() {
        String name = "HDFSCleanup";
        LOGGER.info("Started task '{}'", name);

        HDFSCleanupReport reportHDFS = cleanupHadoop_all(new HDFSCleanupReport(name));

        outputReport(reportHDFS.toString(), name);
    }

    private HDFSCleanupReport cleanupHadoop_all(HDFSCleanupReport report) {
        report.getBuilder().append("[" + timestamp() + "]\n");
        // Read VDLs
        HashSet<String> URIs = new HashSet<>(
                DataLakeManager.getAll(DataLakeType.VDL, DataLakeFileSystemType.HDFS)
        );
        // Read DLBs
        URIs.addAll(
                DataLakeManager.getAll(DataLakeType.DLB, DataLakeFileSystemType.HDFS)
        );
        // Delete Data Lakes
        for (String dataLakeURI : URIs) {
            HDFSDataLake dl = (HDFSDataLake) DataLake.fromURI(dataLakeURI);
            cleanup(dl, report.getBuilder(), true);
        }
        // Delete leftover files
//        cleanupHadoop_warehouse(report);
        return report;
    }

    String cleanupMySQL_VDL(StringBuilder report) {
        report.append("[" + timestamp() + "]\n");
        LinkedList<String> URIs = DataLakeManager.getAll(DataLakeType.VDL, DataLakeFileSystemType.MYSQL);
        for (String dataLakeURI : URIs) {
            MySQLVirtualDataLake dl = (MySQLVirtualDataLake) DataLake.fromURI(dataLakeURI);
            cleanup(dl, report, true);
        }

        return report.toString();
    }

    String cleanupMySQL_DLB(StringBuilder report) {
        report.append("[" + timestamp() + "]\n");
        LinkedList<String> URIs = DataLakeManager.getAll(DataLakeType.DLB, DataLakeFileSystemType.MYSQL);
        for (String dataLakeURI : URIs) {
            MySQLDataLakeBuffer dl = (MySQLDataLakeBuffer) DataLake.fromURI(dataLakeURI);
            cleanup(dl, report, true);
        }

        return report.toString();
    }

    String cleanupFileSystem_all(StringBuilder report) {
        report.append("[" + timestamp() + "]\n");
        LinkedList<String> URIs = DataLakeManager.getAll(DataLakeType.DLB, DataLakeFileSystemType.FS);
        for (String dataLakeURI : URIs) {
            FileSystemDataLake dl = (FileSystemDataLake) DataLake.fromURI(dataLakeURI);
            cleanup(dl, report, true);
        }

        return report.toString();
    }

//    // Not sure this is needed, Impala/Hive should cleanup its own warehouse
//    private void cleanupHadoop_warehouse(HDFSCleanupReport reportHDFS) {
//        Configuration hdfsUserConf = new Configuration();
//        // Set FileSystem URI
//        hdfsUserConf.set("fs.defaultFS", schedulerProperties.hdfsUri);
//        hdfsUserConf.set("hadoop.job.ugi", schedulerProperties.hdfsUser);
//        // Because of Maven
//        hdfsUserConf.set("fs.hdfs.impl", org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
//        hdfsUserConf.set("fs.file.impl", org.apache.hadoop.fs.LocalFileSystem.class.getName());
//        // prevent HDFS to look for more than 1 node
//        hdfsUserConf.setInt("dfs.replication", 1);
//        hdfsUserConf.set("dfs.client.block.write.replace-datanode-on-failure.policy", "NEVER");
//        FileSystem wh;
//        try {
//            wh = FileSystem.get(URI.create(schedulerProperties.warehouseDir), hdfsUserConf);
//        } catch (IOException e) {
//            reportHDFS.getBuilder().append("Delete FAILED: unable to openHDFS file system")
//                    .append(" | ").append(e.getMessage()).append("\n");
//            return;
//        }
//
//        for (String deletedID : reportHDFS.getDeleted()) {
//            Path staleFile = new Path(deletedID);
//            try {
//                if (!wh.delete(staleFile, true))  // try to delete recursively
//                    throw new IOException(String.format("the Data lake was removed, but the warehouse file %s still exists.", staleFile.toString()));
//            } catch (Exception e) {
//                reportHDFS.getBuilder().append("Delete FAILED").append(staleFile.toUri())
//                        .append(" | ").append(e.getMessage()).append("\n");
//            }
//        }
//    }

    private boolean cleanup(DataLake dl, StringBuilder report, boolean additionalCondition) {
        boolean canDelete = additionalCondition;
        try {
            canDelete = canDelete && dl.hasExpired();
        } catch (Exception e) {
            report.append("Failed to check Data Lake expiration: ").append(dl.toString())
                    .append("\n");
            canDelete = false;
        }

        try {
            if (canDelete) {
                DataLakeManager.cleanup(dl, this);
                report.append("Deleted: ").append(dl.toString())
                        .append("\n");
                return true;
            } // otherwise, it's ok to ignore this DL.
        } catch (RemoteDataLakeException e) {
            report.append("Delete FAILED: ").append(dl.toString())
                    .append(" | ").append(e.getMessage())
                    .append("\n");
        }
        return false;
    }

    private void outputReport(String report, String taskName) {
        int successful = 0, failures = 0;
        // count errors
        for (String line : report.split("\\n")) {
            if (line.toLowerCase().contains("failed"))
                failures++;
            else if (line.toLowerCase().contains("deleted:"))
                successful++;
        }
        // print full report (DEBUG)
        LOGGER.debug("Report for task: '%s'\n" + report);
        // print recap (INFO)
        if (failures > 0) {
            LOGGER.error("Task '{}' completed. Deleted: {} - Errors: {}. See DEBUG log for full report.", taskName, successful, failures);
        } else if (successful > 0) {
            LOGGER.info("Task '{}' completed. Deleted: {} Data Lakes. See DEBUG log for full report.", taskName, successful);
        } else LOGGER.info("Task '{}' completed. No Data Lakes were deleted.", taskName);
    }

    private String timestamp() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return df.format(new Date());
    }
}