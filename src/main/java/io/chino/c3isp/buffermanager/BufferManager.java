/*
 *  Based on the template by Hewlett Packard Enterprise Development Company, L.P.
 */
package io.chino.c3isp.buffermanager;

import io.chino.c3isp.buffermanager.cfg.FormatAdapterConfig;
import io.chino.c3isp.buffermanager.cfg.IsiApiConfig;
import io.chino.c3isp.formatadapter.FormatAdapterApiClient;
import io.chino.c3isp.isi.IsiApiClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Main Application class for C3ISP ISI BufferManager.
 * Start the Application and provides methods to handle security and auth
 *
 * @author Andrea Arighi [andrea@chino.io]
 */
@SpringBootApplication
@EnableSwagger2
@EnableScheduling
@PropertySources({
        @PropertySource("classpath:/application.properties")
})
public class BufferManager extends SpringBootServletInitializer {

    /**
     * The currently deployed version of the BufferManager.
     * Also update project.version in Maven pom.xml
     */
    public static final String CURRENT_VERSION = "1.0";

    private static final Logger LOGGER = LoggerFactory.getLogger(BufferManager.class);

    public final static String PATH_SEP = File.separator;

    /**
     * HTTP header which contains C3ISP metadata object
     */
    public static final String C3ISP_METADATA_HEADER = "X-c3isp-metadata";

    /**
     * setup FormatAdapter configuration
     */
    @Autowired public void initFormatAdapter(FormatAdapterConfig config) {
        FormatAdapterApiClient.setConfig(config.init());
        FormatAdapterApiClient.setUrl(config.init().getProperty("url"));
        LOGGER.debug("Using FormatAdapter running at " + config.init().getProperty("url"));
    }
    /**
     * setup ISI API configuration
     */
    @Autowired public void initIsiApi(IsiApiConfig config) {
        IsiApiClient.setConfig(config.init());
        LOGGER.debug("Using ISI API running at " + config.init().getProperty("url") + config.init().getProperty("version"));
    }

    // this is set automatically to 'true' during tests
    private static boolean IS_TESTING = false;

    @Value("${security.activation.status}")
    private boolean securityActivationStatus;

    /**
     * Spring boot method for configuring the current application, right now it
     * automatically scans for interfaces annotated via spring boot methods in
     * all sub classes
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(BufferManager.class);
    }

    /**
     * Docket is a SwaggerUI configuration component, in particular specifies to
     * use the V2.0 (SWAGGER_2) of swagger generated interfaces. It also tells to
     * include only paths that are under /v1/. If other rest interfaces are
     * added with different base path, they won't be included. This path selector
     * can be removed if all interfaces should be documented.
     */
    @Bean
    public Docket documentation() {
        Docket docket = new Docket(DocumentationType.SWAGGER_2);
        docket.apiInfo(metadata());
        Docket d;
        if (!securityActivationStatus) {
            d = docket
                    .select()
                    .paths(PathSelectors.regex("/v1/.*"))
                    .build();
        } else {
            d = docket
                    //.securitySchemes(new ArrayList<ApiKey>(Arrays.asList(new ApiKey("mykey", "api_key", "header"))))   // NOTE: Add API keys here
                    .securitySchemes(new ArrayList<>(Collections.singletonList(new BasicAuth("basicAuth"))))
                    .securityContexts(new ArrayList<>(Collections.singletonList(securityContext())))
                    .select()
                    .paths(PathSelectors.regex("/v1/.*"))
                    .build();
        }
        return d.useDefaultResponseMessages(false);
    }

    /**
     * Selector for the paths this security context applies to ("/v1/.*")
     */
    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth()).forPaths(PathSelectors.regex("/v1/.*"))
                .build();
    }

    /**
     * Here we use the same key defined in the security scheme (basicAuth)
     */
    List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return new ArrayList<>(Collections.singletonList(new SecurityReference("basicAuth", authorizationScopes)));
    }

    /**
     * it just tells swagger that no special configuration are requested
     */
    @Bean
    public UiConfiguration uiConfig() {
        return new UiConfiguration(
                "validatorUrl" // set url
        );
    }

    @Value("${info.build.version}")
    private String version;

    /**
     * the metadata are information visualized in the /basepath/swagger-ui.html
     * interface, only for documentation
     */
    private ApiInfo metadata() {
        String baseTitle = "C3ISP ISI BufferManager API%s";
        String versionNumber = version == null ? "" : String.format(" (%s)", version);
        return new ApiInfoBuilder()
                .title(String.format(baseTitle, versionNumber))
                .description("API for creation and deletion of the DLB/VDL for analytics.")
                .version(version)
                .contact(new Contact("Andrea Arighi", "https://chino.io", "andrea@chino.io"))
                .build();
    }

    /**
     * boot out SpringBoot application
     */
    public static void main(String[] args) throws ClassNotFoundException, Exception {
        LOGGER.info("Starting BufferManager v" + CURRENT_VERSION);
        SpringApplication.run(BufferManager.class, args);
    }

    public static void startTesting() {
        IS_TESTING = true;
    }

    public static boolean isRunningTest() {
        return IS_TESTING;
    }

}
