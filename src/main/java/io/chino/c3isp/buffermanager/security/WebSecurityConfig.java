/**
 *  Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package io.chino.c3isp.buffermanager.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Value("${security.activation.status}")
	private boolean securityActivationStatus;
	
	/**
	 * These paths are required for Swagger and so must not be protected
	 */
	String[] apiPath = {
			"/v2/api-docs",
			"/configuration/ui",
			"/swagger-resources",
			"/configuration/security",
			"/swagger-ui.html",
			"/webjars/**"
	};
	
	/**
	 * Here we configure basic authentication for REST endpoints starting with '/v1/' path
	 */
	@Override
    protected void configure(HttpSecurity http) throws Exception {
		System.out.println("securityActivationStatus=" + securityActivationStatus);
		if (!securityActivationStatus)
			http.authorizeRequests().anyRequest().permitAll();
		else {
        http.httpBasic();
        http.authorizeRequests()
    			.antMatchers(apiPath).permitAll()
    			.antMatchers("/v1/**").authenticated();
		}
        http.csrf().disable();
        http.headers().frameOptions().disable();
    }

}
