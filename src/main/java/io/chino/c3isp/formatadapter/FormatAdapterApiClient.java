package io.chino.c3isp.formatadapter;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import io.chino.c3isp.buffermanager.cfg.FormatAdapterConfig;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;

import java.io.File;

public class FormatAdapterApiClient {
    private static FormatAdapterConfig config;

    public static void setConfig(FormatAdapterConfig configuration) {
        config = configuration;
    }

    public static void setUrl(String url) {
        faApiURl = url;
    }

    private static String faApiURl;

    static {
        Unirest.setTimeouts(10000, 30000);
    }

    /**
     * Logger class, can be used within the methods to log information
     */
    final static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(FormatAdapterApiClient.class);

    /**
     * Calls the C3ISP FormatAdapter API /convertDL endpoint to remove the STIX layer from
     * the file
     *
     * @param CTI the file to be converted
     * @param convertToCsv whether or not the output must be returned in CSV format.
     *
     * @return a converted file, or {@code null} if the data are malformed (e.g. not a STIX).
     *
     * @throws HttpStatusCodeException
     */
    public static String convertDL(File CTI, boolean convertToCsv) throws HttpStatusCodeException {
        try {
            HttpResponse<String> jsonResponse = Unirest.post(faApiURl + "/convertDL" + (convertToCsv ? "?CSV=true" : ""))
                    .field("file", CTI)
                    .asString();

            HttpStatus status = HttpStatus.valueOf(jsonResponse.getStatus());
            if (status.equals(HttpStatus.BAD_REQUEST)) {
                return null;
            } else if (status.is4xxClientError()) {
                throw new HttpServerErrorException(status, jsonResponse.getStatusText());
            } else if (status.is5xxServerError()) {
                throw new HttpClientErrorException(status, jsonResponse.getStatusText());
            }

            return jsonResponse.getBody();
        } catch (UnirestException e) {
            LOGGER.error("ERROR with Format Adapter API:", e);
            throw new HttpServerErrorException(HttpStatus.BAD_GATEWAY, "Bad response received while converting data");
        }
    }
}
