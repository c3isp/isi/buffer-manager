/*
 * Based on the template by Hewlett Packard Enterprise Development Company, L.P.
 */
package io.chino.c3isp.buffermanager.restapi.types.datalake;


import com.fasterxml.jackson.databind.ObjectMapper;
import io.chino.c3isp.buffermanager.cfg.MySQLConfig;
import io.chino.c3isp.buffermanager.restapi.types.datalake.mysql.Queries;
import io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeType;
import io.chino.c3isp.buffermanager.restapi.types.response.DPOWriteOutcome;
import io.chino.c3isp.buffermanager.restapi.types.response.DataLakeStatus;
import io.chino.c3isp.buffermanager.restapi.types.response.PrepareDataResponse;
import io.chino.c3isp.buffermanager.test.BufferManagerBaseTest;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.sql.*;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Logger;

import static io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeFileSystemType.MYSQL;
import static io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeType.DLB;
import static io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeType.VDL;
import static org.junit.Assert.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MySQLDataLakeTest extends BufferManagerBaseTest {

    private static String DLB_DB_BASE_URL, DLB_DB_URL, DLB_DB_NAME, DLB_PREV_DB_NAME;
    private static String VDL_DB_BASE_URL, VDL_DB_URL, VDL_DB_NAME, VDL_PREV_DB_NAME;
    private static final String DB_TIMEZONE = "?useLegacyDatetimeCode=false&serverTimezone=Europe/Rome";
    private static boolean setUpDb;

    @Autowired
    MySQLConfig dbConfig;

    private static String getProperty(DataLakeType type, String property) {
        return getDataLake(type).getConfigProperty(property);
    }

    private static MySQLDataLakeBuffer getDataLake(DataLakeType type) {
        if (type == DataLakeType.VDL)
            return new MySQLVirtualDataLake("MySQLDataLakeTest");
        return new MySQLDataLakeBuffer("MySQLDataLakeTest");
    }

    @BeforeClass
    public static void initTestClass() throws Exception {
        TEST_CLASS_NAME = "MySQL DataLake";
        BufferManagerBaseTest.initTestClass();
        MySQLDataLakeTest.setUpDb = true;
    }

    @Before
    public void init() throws Exception {
        if (setUpDb) {
            MySQLDataLakeBuffer.classDefaultConfigurer = dbConfig.forType(DLB).init();
            MySQLVirtualDataLake.classDefaultConfigurer = dbConfig.forType(VDL).init();
        }

        DLB_DB_BASE_URL = MySQLDataLakeBuffer.classDefaultConfigurer.getProperty("url");
        DLB_DB_NAME = MySQLDataLakeBuffer.classDefaultConfigurer.getProperty("database");
        DLB_DB_URL = DLB_DB_BASE_URL + DLB_DB_NAME + DB_TIMEZONE;

        VDL_DB_BASE_URL = MySQLVirtualDataLake.classDefaultConfigurer.getProperty("url");
        VDL_DB_NAME = MySQLVirtualDataLake.classDefaultConfigurer.getProperty("database");
        VDL_DB_URL = VDL_DB_BASE_URL + VDL_DB_NAME + DB_TIMEZONE;

        if (setUpDb) {
            setUpDatabases();
            LoggerFactory.getLogger(MySQLDataLakeTest.class).debug("DLB will be created in: " + DLB_DB_BASE_URL + DLB_DB_NAME);
            LoggerFactory.getLogger(MySQLDataLakeTest.class).debug("VDL will be created in: " + VDL_DB_BASE_URL);
        }
    }

    @AfterClass
    public static void endTestClass() throws Exception {
        tearDownDatabases();
        BufferManagerBaseTest.endTestClass();
    }

    /**
     * Test the prepareData and releaseData endpoints with a DataLake on a MySQL database
     *
     */
    @Test
    public void BufferManagerTest_MySQL_DLB_prepare_delete() throws Exception {
        initTest("BufferManagerTest_MySQL_DLB_create_delete");

        final String[] dataLakeURI = new String[1];  // final Array is needed in order to store/read the URI from the ResultHandlers
        final String[] credentials = new String[2];  // same for credentials
        final MySQLDataLakeBuffer testDL = new MySQLDataLakeBuffer("BufferManager test");

        /* * test prepareData * */
        callPrepareDataAndCheckResult(
                MYSQL,
                DLB,
                result -> {
                    ObjectMapper mapper = new ObjectMapper();
                    PrepareDataResponse resp = mapper.readValue(result.getResponse().getContentAsString(), PrepareDataResponse.class);
                    dataLakeURI[0] = resp.extractDataLakeURI();
                    credentials[0] = DataLakeManager.getUrlParam(dataLakeURI[0], "usr");
                    credentials[1] = DataLakeManager.getUrlParam(dataLakeURI[0], "psw");

                    setUserName(credentials[0]);

                    assertTrue(
                            "Wrong protocol prefix (expected '" + MySQLDataLakeBuffer.URI_PROTOCOL + "')",
                            dataLakeURI[0].startsWith(MySQLDataLakeBuffer.URI_PROTOCOL)
                    );

                    assertEquals("wrong DPO ID", resp.getDPOList().get(0).getDPOId(), TEST_DPO_IDS.get(0));

                    testDL.setURIValue(dataLakeURI[0]);
                    assertTrue("Data Lake table not created.", testDL.exists());
                    setDLName(DataLakeManager.getUrlParam(dataLakeURI[0], "table"));

                    Connection conn = DriverManager.getConnection(
                            DLB_DB_URL,
                            credentials[0],
                            credentials[1]
                    );
                    conn.setCatalog(DLB_DB_NAME);
                    Statement s = conn.createStatement();
                    s.execute(
                            "SELECT COUNT(*) FROM " + MySQLDataLakeBuffer.getTableFromUri(dataLakeURI[0]) + " AS count;"
                    );
                    ResultSet res = s.getResultSet();
                    int count = 0;
                    while (res.next()) {
                        count = res.getInt(1);
                    }

                    assertEquals("DPO count in Data Lake", TEST_DPO_IDS.size(), count);
                }
        );

        // call without credentials
        super.callReleaseDataAndCheckResult(
                dataLakeURI[0],
                result -> {
                    ObjectMapper mapper = new ObjectMapper();
                    String resp = result.getResponse().getContentAsString();

                    assertFalse("An error was returned by releaseData: " + resp, resp.toLowerCase().startsWith("error"));
                    assertTrue("Error - invalid response from releaseData: " + resp,
                            resp.contains("Successfully removed jdbc:mysql:")
                    );

                    assertFalse("MySQL data lake was not deleted!", testDL.exists());
                }
        );
    }

    /**
     * Test the prepareData and releaseData endpoints with a DataLake on a MySQL database
     *
     */
    @Test
    public void BufferManagerTest_MySQL_VDL_prepare_delete() throws Exception {
        initTest("BufferManagerTest_MySQL_VDL_create_delete");

        final String[] dataLakeURI = new String[1];  // final Array is needed in order to store/read the URI from the ResultHandlers
        final String[] credentials = new String[2];  // same for credentials
        final MySQLVirtualDataLake testVDL = new MySQLVirtualDataLake("BufferManager test");

        /* * test prepareData * */
        callPrepareDataAndCheckResult(
                MYSQL,
                VDL,
                result -> {
                    ObjectMapper mapper = new ObjectMapper();
                    PrepareDataResponse resp = mapper.readValue(result.getResponse().getContentAsString(), PrepareDataResponse.class);

                    dataLakeURI[0] = resp.extractDataLakeURI();
                    credentials[0] = DataLakeManager.getUrlParam(dataLakeURI[0], "usr");
                    credentials[1] = DataLakeManager.getUrlParam(dataLakeURI[0], "psw");
                    setUserName(credentials[0]);

                    assertTrue(
                            "Wrong protocol prefix (expected '" + MySQLVirtualDataLake.URI_PROTOCOL + "')",
                            dataLakeURI[0].startsWith(MySQLVirtualDataLake.URI_PROTOCOL)
                    );

                    assertEquals("wrong DPO ID", resp.getDPOList().get(0).getDPOId(), TEST_DPO_IDS.get(0));

                    testVDL.setURIValue(dataLakeURI[0]);
                    assertTrue("Data Lake table not created.", testVDL.exists());

                    String VDLName = testVDL.getDataLakeName();
                    setDLName(VDLName);
                    Connection conn = DriverManager.getConnection(
                            VDL_DB_URL.replace(VDL_DB_NAME, VDLName),
                            credentials[0],
                            credentials[1]
                    );
                    conn.setCatalog(testVDL.getDataLakeName());
                    Statement s = conn.createStatement();

                    String query = "SELECT COUNT(*) FROM " + MySQLVirtualDataLake.getTableFromUri(dataLakeURI[0]) + " AS count;";
                    System.out.println(query);

                    s.execute(query);

                    ResultSet res = s.getResultSet();
                    int count = 0;
                    while (res.next()) {
                        count = res.getInt(1);
                    }
                    assertEquals("DPO count in Data Lake", TEST_DPO_IDS.size(), count);
                }
        );

        callReleaseDataAndCheckResult(
                dataLakeURI[0],
                credentials,
                result -> {
                    ObjectMapper mapper = new ObjectMapper();
                    String resp = result.getResponse().getContentAsString();

                    assertFalse("An error was returned by releaseData: " + resp,
                            resp.toLowerCase().startsWith("error"));
                    assertTrue("Error - invalid response from releaseData: " + resp,
                            resp.contains("Successfully removed jdbc:mysql:")
                    );

                    assertFalse("MySQL data lake was not deleted!", testVDL.exists());
                }
        );
    }

    @Test
    public void BufferManagerTest_MySQL_DLB_empty_write() throws Exception {
        runEmptyWriteTestMySQL(DLB);
    }

    @Test
    public void BufferManagerTest_MySQL_VDL_empty_write() throws Exception {
        runEmptyWriteTestMySQL(VDL);
    }

    private void runEmptyWriteTestMySQL(DataLakeType dlType) throws Exception {
        initTest("BufferManagerTest_MySQL_VDL_empty_write");

        final String[] dataLakeURI = new String[1];  // final Array is needed in order to store/read the URI from the ResultHandlers
        final String[] credentials = new String[2];  // same for credentials
        final MySQLDataLakeBuffer testDL =
                (dlType == VDL) ? new MySQLVirtualDataLake("BufferManagerTest_MySQL_VDL_empty_write")
                        : new MySQLDataLakeBuffer("BufferManagerTest_MySQL_VDL_empty_write");

        final String DB_URL = (dlType == VDL) ? VDL_DB_URL : DLB_DB_URL;
        final String DB_NAME = (dlType == VDL) ? VDL_DB_NAME : DLB_DB_NAME;

        // using a final object in order to pass it in lambda expression
        final AtomicReference<Connection> connection = new AtomicReference<>();

        callPrepareEmptyAndCheckResult(
                MYSQL,
                dlType,
                result -> {
                    // check response correctness
                    ObjectMapper mapper = new ObjectMapper();
                    DataLakeStatus resp = mapper.readValue(
                            result.getResponse().getContentAsString(),
                            DataLakeStatus.class
                    );

                    assertNotNull(resp.getMessage());
                    assertNotNull(resp.getURI());
                    assertTrue("Wrong response message", resp.getMessage().contains(FileSystemDataLake.SUCCESS_MESSAGE));

                    dataLakeURI[0] = resp.getURI();
                    credentials[0] = DataLakeManager.getUrlParam(dataLakeURI[0], "usr");
                    credentials[1] = DataLakeManager.getUrlParam(dataLakeURI[0], "psw");
                    setUserName(credentials[0]);

                    // check DL URI
                    assertTrue("Wrong URI", dataLakeURI[0].startsWith(MySQLVirtualDataLake.URI_PROTOCOL));

                    // check access to DL
                    testDL.setURIValue(dataLakeURI[0]);
                    String dataLakeName = testDL.getDataLakeName();
                    setDLName(dataLakeName);
                    connection.set(DriverManager.getConnection(
                            DB_URL.replace(DB_NAME, dataLakeName),
                            credentials[0],
                            credentials[1]
                    ));
                    connection.get().setCatalog(testDL.getDataLakeName());

                    // check that DL exists and is empty
                    Statement s = connection.get().createStatement();
                    String query = "SELECT COUNT(*) FROM " + MySQLVirtualDataLake.getTableFromUri(dataLakeURI[0]) + " AS count;";
                    System.out.println(query);

                    s.execute(query);

                    ResultSet res = s.getResultSet();
                    int count = 0;
                    while (res.next()) {
                        count = res.getInt(1);
                    }
                    assertEquals("DPO count in Data Lake", 0, count);
                }
        );

        callPopulateDataLakeAndCheckResult(
                dataLakeURI[0],
                result -> {
                    // check response
                    ObjectMapper mapper = new ObjectMapper();
                    DPOWriteOutcome resp = mapper.readValue(
                            result.getResponse().getContentAsString(),
                            DPOWriteOutcome.class
                    );

                    assertNotNull(resp.getMessage());
                    assertNotNull(resp.getDPOId());
                    assertNotNull(resp.getFileLocator());
                    assertEquals(resp.getCode(), 201); // response status is checked by main method, but the content of 'code' must match too

                    // check that file is reachable from the file locator provided by DPOWriteOutcome
                    Statement stm = connection.get().createStatement();
                    ResultSet cti = stm.executeQuery(
                            resp.getFileLocator()
                    );
                    cti.next();
                    String actualContent = cti.getString(1);

                    assertEquals("unexpected CTI content", DPOContent, actualContent);
                }
        );

        callReleaseDataAndCheckResult(
                dataLakeURI[0],
                result -> {} // no need to test releaseData again
        );
    }

    @Test
    public void testCreateEmptyDL_MYSQL() throws Exception{
        initTest("testCreateEmptyDL_MYSQL");
        final String[] dataLakeURI = new String[1];
        final String[] credentials = new String[2];


        /* DATABASE SETTINGS */
        final String DB_TIMEZONE = "?useLegacyDatetimeCode=false&serverTimezone=Europe/Rome";

        /* TEST 1 */
        final MySQLDataLakeBuffer testDL = new MySQLDataLakeBuffer("testCreateEmptyDL_MYSQL");

        String DB_BASE_URL = MySQLDataLakeBuffer.classDefaultConfigurer.getProperty("url");
        String DB_NAME = MySQLDataLakeBuffer.classDefaultConfigurer.getProperty("database");
        String DB_URL = DB_BASE_URL + DB_NAME + DB_TIMEZONE;

        final String DLB_DB_URL = DB_URL;
        final String DLB_DB_NAME = DB_NAME;
        callPrepareEmptyAndCheckResult(
                MYSQL,
                DLB,
                result -> {
                    ObjectMapper mapper = new ObjectMapper();
                    DataLakeStatus resp = mapper.readValue(result.getResponse().getContentAsString(), DataLakeStatus.class);
                    dataLakeURI[0] = resp.getURI();
                    credentials[0] = DataLakeManager.getUrlParam(dataLakeURI[0], "usr");
                    credentials[1] = DataLakeManager.getUrlParam(dataLakeURI[0], "psw");

                    setUserName(credentials[0]);

                    assertTrue(
                            "Wrong protocol prefix (expected '" + MySQLDataLakeBuffer.URI_PROTOCOL + "')",
                            dataLakeURI[0].startsWith(MySQLDataLakeBuffer.URI_PROTOCOL)
                    );

                    testDL.setURIValue(dataLakeURI[0]);
                    assertTrue("Data Lake table not created.", testDL.exists());
                    setDLName(DataLakeManager.getUrlParam(dataLakeURI[0], "table"));

                    Connection conn = DriverManager.getConnection(
                            DLB_DB_URL,
                            credentials[0],
                            credentials[1]
                    );
                    conn.setCatalog(DLB_DB_NAME);
                    Statement s = conn.createStatement();
                    s.execute(
                            "SELECT COUNT(*) FROM " + MySQLDataLakeBuffer.getTableFromUri(dataLakeURI[0]) + " AS count;"
                    );
                    ResultSet res = s.getResultSet();
                    int count = 0;
                    while (res.next()) {
                        count = res.getInt(1);
                    }
                    assertEquals("DPO count in Data Lake", 0, count);
                }
        );

        super.callReleaseDataAndCheckResult(
                dataLakeURI[0],
                result -> {
                    ObjectMapper mapper = new ObjectMapper();
                    String resp = result.getResponse().getContentAsString();

                    assertFalse("An error was returned by releaseData: " + resp, resp.toLowerCase().startsWith("error"));
                    assertTrue("Error - invalid response from releaseData: " + resp,
                            resp.contains("Successfully removed jdbc:mysql:")
                    );

                    assertFalse("MySQL data lake was not deleted!", testDL.exists());
                }
        );

        /* TEST 2 */
        final MySQLVirtualDataLake testVDL = new MySQLVirtualDataLake("testCreateEmptyDL_MYSQL");

        DB_BASE_URL = MySQLVirtualDataLake.classDefaultConfigurer.getProperty("url");
        DB_NAME = MySQLVirtualDataLake.classDefaultConfigurer.getProperty("database");
        DB_URL = DB_BASE_URL + DB_NAME + DB_TIMEZONE;

        final String VDL_DB_URL = DB_URL;
        final String VDL_DB_NAME = DB_NAME;
        callPrepareEmptyAndCheckResult(
                MYSQL,
                VDL,
                result -> {
                    ObjectMapper mapper = new ObjectMapper();
                    DataLakeStatus resp = mapper.readValue(result.getResponse().getContentAsString(), DataLakeStatus.class);

                    dataLakeURI[0] = resp.getURI();
                    credentials[0] = DataLakeManager.getUrlParam(dataLakeURI[0], "usr");
                    credentials[1] = DataLakeManager.getUrlParam(dataLakeURI[0], "psw");
                    setUserName(credentials[0]);

                    assertTrue(
                            "Wrong protocol prefix (expected '" + MySQLVirtualDataLake.URI_PROTOCOL + "')",
                            dataLakeURI[0].startsWith(MySQLVirtualDataLake.URI_PROTOCOL)
                    );

                    testVDL.setURIValue(dataLakeURI[0]);
                    assertTrue("Data Lake table not created.", testVDL.exists());

                    String VDLName = testVDL.getDataLakeName();
                    setDLName(VDLName);
                    Connection conn = DriverManager.getConnection(
                            VDL_DB_URL.replace(VDL_DB_NAME, VDLName),
                            credentials[0],
                            credentials[1]
                    );
                    conn.setCatalog(testVDL.getDataLakeName());
                    Statement s = conn.createStatement();

                    String query = "SELECT COUNT(*) FROM " + MySQLVirtualDataLake.getTableFromUri(dataLakeURI[0]) + " AS count;";
                    System.out.println(query);

                    s.execute(query);

                    ResultSet res = s.getResultSet();
                    int count = 0;
                    while (res.next()) {
                        count = res.getInt(1);
                    }
                    assertEquals("DPO count in Data Lake", 0, count);
                }
        );

        callReleaseDataAndCheckResult(
                dataLakeURI[0],
                result -> {
                    String resp = result.getResponse().getContentAsString();

                    assertFalse("An error was returned by releaseData: " + resp, resp.toLowerCase().startsWith("error"));
                    assertTrue("Error - invalid response from releaseData: " + resp,
                            resp.contains("Successfully removed jdbc:mysql:")
                    );

                    assertFalse("MySQL data lake was not deleted!", testDL.exists());
                }
        );
    }

    /**
     * Required by {@link SecureCredentialsGeneratorTest} to setup and cleanup databases.
     *
     * @param mySqlConfig
     * @throws Exception
     */
    public static void setUpDatabases(MySQLConfig mySqlConfig) throws Exception {
        MySQLDataLakeTest testInitializer = new MySQLDataLakeTest();
        testInitializer.dbConfig = mySqlConfig;
        // setup static variables using the provided configuration.
        setUpDb = true;
        testInitializer.init(); // also calls setUpDatabases()
    }

    /**
     * Create test DB and user
     *
     * @throws Exception mainly {@link java.sql.SQLException SQL Exceptions}, thrown in case of DB connection errors
     */
    static void setUpDatabases() throws Exception {
        // create test DB (with root access)
        // init DB for both DLB and a VDL
        for (DataLakeType type : DataLakeType.values()) {
            String DB_BASE_URL = (type == VDL) ? VDL_DB_BASE_URL: DLB_DB_BASE_URL;
            String DB_NAME = (type == VDL) ? VDL_DB_NAME: DLB_DB_NAME;

            Connection rootConn = null;
            try {
                rootConn = DriverManager.getConnection(
                        DB_BASE_URL + DB_TIMEZONE,
                        getProperty(type, "user.name"),
                        getProperty(type, "user.password")
                );
            } catch (SQLException e) {
                System.err.println("Unable to connect to database '" + DB_BASE_URL + DB_TIMEZONE + "'. " +
                        "Check the server logs to learn more about the error.");
                throw e;
            }

            setPreviousDatabaseName(type, rootConn.getCatalog());

            // create new DB
            Statement smt;
            smt = rootConn.createStatement();
            smt.execute(
                    "DROP DATABASE IF EXISTS " + DB_NAME + ";"
            );
            smt.execute(
                    "CREATE DATABASE " + DB_NAME + ";"
            );
            rootConn.setCatalog(DB_NAME); // equivalent of "USE DB_NAME"
            smt.close();
        }
        // flag the database as already set up
        setUpDb = false;
    }

    private static void setPreviousDatabaseName(DataLakeType type, String name) {
        if (type == VDL) VDL_PREV_DB_NAME = name;
        else DLB_PREV_DB_NAME = name;
    }

    /**
     * Delete test DB and user
     *
     * @throws Exception mainly {@link java.sql.SQLException SQL Exceptions}, thrown in case of DB connection errors
     */

    static void tearDownDatabases() throws Exception {
        // tear down db for both DLB and VDL
        for (DataLakeType type : DataLakeType.values()) {
            String DB_BASE_URL = (type == VDL) ? VDL_DB_BASE_URL: DLB_DB_BASE_URL;
            String PREV_DB_NAME = (type == VDL) ? VDL_PREV_DB_NAME: DLB_PREV_DB_NAME;
            String DB_NAME = (type == VDL) ? VDL_DB_NAME: DLB_DB_NAME;

            // connect to DB as root user
            Connection rootConn = DriverManager.getConnection(
                    DB_BASE_URL + DB_TIMEZONE,
                    getProperty(type, "user.name"),
                    getProperty(type, "user.password")
            );

            try {
                Queries.emptyDatabase(rootConn, DB_NAME);
                Queries.dropDatabase(rootConn, DB_NAME);
                Logger.getLogger("BufferManagerController Test")
                        .info("DATABASE " + DB_NAME + " dropped");
            } catch (SQLException e) {
                Logger.getLogger("BufferManagerController Test")
                        .warning("DATABASE " + DB_NAME + " was not created");
                Logger.getLogger("BufferManagerController Test")
                        .warning(e.getMessage());
            }

            if (PREV_DB_NAME != null && !PREV_DB_NAME.isEmpty()) {
                // re-activate previous database, if any
                rootConn.setCatalog(PREV_DB_NAME); // equivalent of "USE PREV_DB_NAME"
                Logger.getLogger("BufferManagerController Test")
                        .info("restored active DB: " + PREV_DB_NAME);
            }
        }
    }

    /**
     * Mock a call to /v1/releaseData sending a URI to a
     * {@link io.chino.c3isp.buffermanager.restapi.types.datalake.DataLake},
     * then perform some operations to verify that the call returned the correct values and that all the
     * operations succeeded.
     *
     * @param dataLakeURI
     * @param credentials
     * @param checkResultOperations
     * @throws Exception
     */
    protected void callReleaseDataAndCheckResult(String dataLakeURI, String[] credentials, ResultHandler checkResultOperations) throws Exception {
        getMockMvc().perform(
                // Release data requires authentication
                post(String.format("/v1/releaseData?usr=%s$psw=%s", credentials[0], credentials[1]))
                        .content(dataLakeURI)
                        .with(httpBasic(getRestUser(), getRestPassword()))
                        .contentType(MediaType.APPLICATION_JSON)    // client sends JSON PrepareDataQuery
                        .accept(MediaType.TEXT_PLAIN)               // server replies with JSON PrepareDataResponse
        )
                .andDo(
                        MockMvcResultHandlers.print()
                )
                .andExpect(
                        status().isOk()        // check HTTP status (200)
                )
                .andDo(
                        checkResultOperations  // check output is same as expected
                );
    }



    @Test
    public void testExpirationCheck_MYSQL() throws Exception {
        initTest("testExpirationCheck_MYSQL");
        final String[] dataLakeURI = new String[1];

        for (DataLakeType dlType : DataLakeType.values()) {
            dataLakeURI[0] = null;
            try {
                callPrepareEmptyAndCheckResult(
                        MYSQL,
                        dlType,
                        result -> {
                            ObjectMapper mapper = new ObjectMapper();
                            DataLakeStatus resp = mapper.readValue(result.getResponse()
                                    .getContentAsString(), DataLakeStatus.class);
                            dataLakeURI[0] = resp.getURI();
                        }
                );

                MySQLDataLakeBuffer dl = (MySQLDataLakeBuffer) DataLake.fromURI(dataLakeURI[0]);
                assertNotNull("Null creation date in Data Lake", dl.getCreationDate());
                assertNotNull("Null expiration date in Data Lake", dl.getExpirationDate());

                // prevent NullPointerException in tests
                String lastUpdate = (dl.getLastUpdate() != null)
                        ? dl.getLastUpdate().toString()
                        : "(null - double check this assertion, it's not working!)";
                assertNull("Non-null last update date in Data Lake: " + lastUpdate, dl.getLastUpdate());

                Date now = new Date();
                assertEquals("Wrong expiration status.", dl.getExpirationDate().before(now), dl.hasExpired());
            } catch (Exception e) {
                System.err.println("EXCEPTION: Test failed for MySQL " + dlType);
                throw e;
            } finally {
                if (dataLakeURI[0] != null)
                    callReleaseDataAndCheckResult(
                            dataLakeURI[0],
                            result -> {} // no need to test releaseData again
                    );
            }
        }
    }
}