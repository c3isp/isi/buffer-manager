package io.chino.c3isp.buffermanager.restapi.types.datalake;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.chino.c3isp.buffermanager.cfg.HDFSConfig;
import io.chino.c3isp.buffermanager.restapi.types.response.DPOWriteOutcome;
import io.chino.c3isp.buffermanager.restapi.types.response.DataLakeStatus;
import io.chino.c3isp.buffermanager.restapi.types.response.PrepareDataResponse;
import io.chino.c3isp.buffermanager.test.BufferManagerBaseTest;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.util.Date;

import static io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeFileSystemType.HDFS;
import static io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeType.DLB;
import static org.junit.Assert.*;

@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class HDFSDataLakeTest extends BufferManagerBaseTest {

    private static boolean setupRequired;

    @Autowired
    HDFSConfig config;

    private String hdfsUri;
    private String hdfsUsername;
    private Configuration hadoopConf;

    @BeforeClass
    public static void initTestClass() throws Exception {
        TEST_CLASS_NAME = "HDFS DataLake";
        BufferManagerBaseTest.initTestClass();
        setupRequired = true;
    }

    @Before
    public void init() {
        if (setupRequired) {
            HDFSDataLake.classDefaultConfigurer = config.init();
            LoggerFactory.getLogger(HDFSDataLakeTest.class).info("HDFS URI is: " + config.getProperty("root"));
            setupRequired = false;
        }
        hdfsUri = config.getProperty("uri");
        hdfsUsername = config.getProperty("user.name");
        hadoopConf = new Configuration();
        // Init HDFS config (see HDFSDataLake#initConfig)
        hadoopConf.set("fs.hdfs.impl", org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
        hadoopConf.set("fs.file.impl", org.apache.hadoop.fs.LocalFileSystem.class.getName());
        hadoopConf.set("fs.defaultFS", hdfsUri);
        hadoopConf.setBoolean("dfs.support.append", true);
    }

    @Test
    public void BufferManagerTest_HDFS_prepare_delete() throws Exception {
        initTest("BufferManagerTest_MySQL_DLB_create_delete");

        final String[] dataLakeURI = new String[1];  // final Array is needed in order to store/read the URI from the ResultHandlers
        FileSystem hdfs = FileSystem.get(URI.create(hdfsUri), hadoopConf, hdfsUsername);
        ObjectMapper mapper = new ObjectMapper();

        /* * test releaseData * */
        callPrepareDataAndCheckResult(HDFS, DLB, result -> {
            PrepareDataResponse resp = mapper.readValue(result.getResponse().getContentAsString(), PrepareDataResponse.class);
            dataLakeURI[0] = resp.extractDataLakeURI();

            assertNotNull("prepareData response contains a null URI", dataLakeURI[0]);
            // check that DL URI matches the expected format
            assertTrue("Wrong URI", dataLakeURI[0].startsWith(HDFSDataLake.URI_PROTOCOL));

            Path dataLake = new Path(URI.create(dataLakeURI[0]));
            assertTrue("Unable to locate Data Lake folder", hdfs.exists(dataLake));

            //  In the HDFS folder there is also the Pool file pool.csv,
            //  which contains the aggregated data from all the DPOs.
            int expectedCount = BufferManagerBaseTest.TEST_DPO_IDS.size() + 1;
            int actualCount = hdfs.listStatus(dataLake).length;
            assertEquals("Wrong DPO count in Data Lake",
                    expectedCount,
                    actualCount);
        });

        /* * test releaseData * */
        callReleaseDataAndCheckResult(dataLakeURI[0], result -> {
            String resp = result.getResponse().getContentAsString();

            assertFalse("An error was returned by releaseData: " + resp, resp.toLowerCase().startsWith("error"));
            assertTrue("Error - invalid response from releaseData: " + resp,
                    resp.toLowerCase().contains("success") && resp.contains(dataLakeURI[0])
            );

            Path dataLake = new Path(URI.create(dataLakeURI[0]));
            assertFalse("The Data Lake HDFS folder still exists after deletion", hdfs.exists(dataLake));
        });
    }

    @Test
    public void BufferManagerTest_HDFS_prepareEmpty_write_delete() throws Exception {
        initTest("BufferManagerTest_MySQL_DLB_create_delete");

        final String[] dataLakeURI = new String[1];  // final Array is needed in order to store/read the URI from the ResultHandlers
        FileSystem hdfs = FileSystem.get(URI.create(hdfsUri), hadoopConf, hdfsUsername);
        ObjectMapper mapper = new ObjectMapper();

        callPrepareEmptyAndCheckResult(HDFS, DLB, result -> {
            DataLakeStatus resp = mapper.readValue(result.getResponse().getContentAsString(), DataLakeStatus.class);
            dataLakeURI[0] = resp.getURI();

            assertNotNull("prepareEmpty response contains a null URI", dataLakeURI[0]);

            Path dataLake = new Path(URI.create(dataLakeURI[0]));
            assertTrue("Unable to locate Data Lake folder", hdfs.exists(dataLake));
            assertFalse("Non-empty Data Lake!", hdfs.listFiles(dataLake, true).hasNext());
        });

        callPopulateDataLakeAndCheckResult(dataLakeURI[0], result -> {
            DPOWriteOutcome resp = mapper.readValue(
                    result.getResponse().getContentAsString(),
                    DPOWriteOutcome.class
            );

            assertNotNull(resp.getMessage());
            assertNotNull(resp.getDPOId());
            assertNotNull(resp.getFileLocator());
            assertEquals(resp.getCode(), 201); // response status is checked by main method, but the content of 'code' must match too

            Path dataLake = new Path(URI.create(dataLakeURI[0]));
            assertTrue("Unable to locate Data Lake folder", hdfs.exists(dataLake));

            //  callPopulateDataLake... writes 2 DPOs for each item in the TEST_DPO_IDS:
            //  one named after the DPO ID and the other named 'test'.
            //  On top of that, in the HDFS folder there is also the Pool file pool.csv, which contains all the data.
            int expectedCount = 2 * BufferManagerBaseTest.TEST_DPO_IDS.size() + 1;
            int actualCount = hdfs.listStatus(dataLake).length;
            assertEquals("Wrong DPO count in Data Lake",
                    expectedCount,
                    actualCount);
        });

        callReleaseDataAndCheckResult(
                dataLakeURI[0],
                result -> {} // no need to test releaseData again
        );
    }

    @Test
    public void testExpirationCheck_HDFS() throws Exception {
        final String[] dataLakeURI = new String[1];

        callPrepareEmptyAndCheckResult(HDFS, DLB, result -> {
            ObjectMapper mapper = new ObjectMapper();
            DataLakeStatus resp = mapper.readValue(result.getResponse().getContentAsString(), DataLakeStatus.class);
            dataLakeURI[0] = resp.getURI();
        });

        try {
            HDFSDataLake dl = (HDFSDataLake) DataLake.fromURI(dataLakeURI[0]);
            assertNotNull("Null creation date in Data Lake", dl.getCreationDate());
            assertNotNull("Null expiration date in Data Lake", dl.getExpirationDate());

            Date now = new Date();
            assertEquals("Wrong expiration status.", dl.getExpirationDate().before(now), dl.hasExpired());
        } finally {
            if (dataLakeURI[0] != null)
                callReleaseDataAndCheckResult(dataLakeURI[0],
                        result -> {} // no need to test releaseData again
                );
        }

    }

}
