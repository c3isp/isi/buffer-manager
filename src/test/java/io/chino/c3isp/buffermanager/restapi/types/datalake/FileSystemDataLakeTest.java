/*
 * Based on the template by Hewlett Packard Enterprise Development Company, L.P.
 */
package io.chino.c3isp.buffermanager.restapi.types.datalake;


import com.fasterxml.jackson.databind.ObjectMapper;
import io.chino.c3isp.buffermanager.cfg.FileSystemConfig;
import io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeFileSystemType;
import io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeType;
import io.chino.c3isp.buffermanager.restapi.types.response.DPOWriteOutcome;
import io.chino.c3isp.buffermanager.restapi.types.response.DataLakeStatus;
import io.chino.c3isp.buffermanager.restapi.types.response.PrepareDataResponse;
import io.chino.c3isp.buffermanager.test.BufferManagerBaseTest;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.net.URI;
import java.util.Date;
import java.util.Objects;

import static io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeFileSystemType.FS;
import static io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeType.DLB;
import static org.junit.Assert.*;

@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FileSystemDataLakeTest extends BufferManagerBaseTest {

    private static boolean firstRun;

    @Autowired
    FileSystemConfig config;

    @BeforeClass
    public static void initTestClass() throws Exception {
        TEST_CLASS_NAME = "File System DataLake";
        BufferManagerBaseTest.initTestClass();
        firstRun = true;
    }

    @Before
    public void init() {
        if (firstRun) {
            FileSystemDataLake.classDefaultConfigurer = config.init();
            LoggerFactory.getLogger(FileSystemDataLakeTest.class).info("Data Lake folder is: " + config.getProperty("root"));
            firstRun = false;
        }
    }

    /**
     * Test the prepareData and releaseData endpoints with a DataLake on the local File System
     *
     */
    @Test
    public void BufferManagerTest_FileSystem_prepare_delete() throws Exception {
        initTest("BufferManagerTest_FileSystem_create_delete");
        setUserName("none");

        final String[] dataLakeURI = new String[1];  // final Array is needed in order to store/read the URI from the ResultHandlers

        /* * test prepareData * */
        callPrepareDataAndCheckResult(
                FS,
                DLB,
                result -> {
                    ObjectMapper mapper = new ObjectMapper();
                    PrepareDataResponse resp = mapper.readValue(result.getResponse().getContentAsString(), PrepareDataResponse.class);
                    dataLakeURI[0] = resp.extractDataLakeURI();

                    // check that the response contains the DPO ID from the request
                    assertEquals("wrong DPO ID", resp.getDPOList().get(0).getDPOId(), TEST_DPO_IDS.get(0));

                    // check that DL was created and can be read
                    File dataLakeFolder = new File(URI.create(dataLakeURI[0]));
                    assertTrue(dataLakeFolder.exists());

                    setDLName(dataLakeFolder.getName());

                    // check that DL URI matches the expected format
                    assertTrue("Wrong URI", dataLakeURI[0].startsWith(FileSystemDataLake.URI_PROTOCOL));

                    String controlPath = FileSystemDataLake.classDefaultConfigurer.getProperty("root") + dataLakeFolder.getName();
                    File controlFile = new File(URI.create(controlPath));
                    assertTrue("Couldn't read " + controlPath + " (FileSystemDataLake)", controlFile.exists());

                    assertEquals("DataLake points to the wrong folder.", controlFile.getAbsolutePath(), dataLakeFolder.getAbsolutePath());
                });

        /* * test releaseData * */
        callReleaseDataAndCheckResult(
                dataLakeURI[0],
                result -> {
                    String resp = result.getResponse().getContentAsString();

                    assertFalse("An error was returned by releaseData: " + resp, resp.toLowerCase().startsWith("error"));
                    assertTrue("Error - invalid response from releaseData: " + resp,
                            resp.toLowerCase().contains("success") && resp.contains(dataLakeURI[0])
                    );

                    File oldDl = new File(URI.create(dataLakeURI[0]));
                    assertFalse("the dataLake is still accessible (DataLake.fromURI didn't fail)", oldDl.exists());
                }
        );
    }

    @Test
    public void BufferManagerTest_FileSystem_empty_write() throws Exception {

        final String[] dataLakeURI = new String[1];

        // create empty DL
        callPrepareEmptyAndCheckResult(
                FS,
                DLB,
                result -> {
                    // response correctness
                    ObjectMapper mapper = new ObjectMapper();
                    DataLakeStatus resp = mapper.readValue(
                            result.getResponse().getContentAsString(),
                            DataLakeStatus.class
                    );

                    assertNotNull(resp.getMessage());
                    assertNotNull(resp.getURI());
                    assertTrue("Wrong response message", resp.getMessage().contains(FileSystemDataLake.SUCCESS_MESSAGE));

                    dataLakeURI[0] = resp.getURI();

                    // access to DL
                    File dataLakeFolder = new File(URI.create(dataLakeURI[0]));
                    assertTrue(dataLakeFolder.exists());

                    setDLName(dataLakeFolder.getName());

                    // check that DL URI matches the expected format
                    assertTrue("Wrong URI", dataLakeURI[0].startsWith(FileSystemDataLake.URI_PROTOCOL));

                    String controlPath = FileSystemDataLake.classDefaultConfigurer.getProperty("root") + dataLakeFolder.getName();
                    File controlFile = new File(URI.create(controlPath));
                    assertTrue("Couldn't read " + controlPath + " (FileSystemDataLake)", controlFile.exists());

                    assertEquals("DataLake points to the wrong folder.", controlFile.getAbsolutePath(), dataLakeFolder.getAbsolutePath());
                }
        );

        callPopulateDataLakeAndCheckResult(
                dataLakeURI[0],
                result -> {
                    ObjectMapper mapper = new ObjectMapper();
                    DPOWriteOutcome resp = mapper.readValue(
                            result.getResponse().getContentAsString(),
                            DPOWriteOutcome.class
                    );

                    assertNotNull(resp.getMessage());
                    assertNotNull(resp.getDPOId());
                    assertNotNull(resp.getFileLocator());
                    assertEquals(resp.getCode(), 201); // response status is checked by main method, but the content of 'code' must match too

                    // check that file is reachable from the file locator provided by DPOWriteOutcome
                    assertTrue(
                            "DPO was not written in the Data Lake!",
                            new File(URI.create(resp.getFileLocator())).exists()
                    );
                }
        );

        callReleaseDataAndCheckResult(
                dataLakeURI[0],
                result -> {} // no need to test releaseData again
        );
    }

    @Test
    public void testCreateEmptyDL_FS() throws Exception {
        initTest("testCreateEmptyDL_FS");
        final String[] dataLakeURI = new String[1];

        callPrepareEmptyAndCheckResult(
                DataLakeFileSystemType.FS,
                DataLakeType.DLB,
                result -> {
                    ObjectMapper mapper = new ObjectMapper();
                    DataLakeStatus resp = mapper.readValue(result.getResponse().getContentAsString(), DataLakeStatus.class);
                    dataLakeURI[0] = resp.getURI();

                    // check that DL was created and can be read
                    File dataLakeFolder = new File(URI.create(dataLakeURI[0]));
                    assertTrue(dataLakeFolder.exists());

                    setDLName(dataLakeFolder.getName());

                    // check that DL is empty
                    assertEquals("DataLake is not empty!",0, Objects.requireNonNull(dataLakeFolder.list()).length);

                    // check that DL URI matches the expected format
                    String controlPath = FileSystemDataLake.classDefaultConfigurer.getProperty("root") + dataLakeFolder.getName();
                    File controlFile = new File(URI.create(controlPath));
                    assertTrue("Couldn't read " + controlPath + " (FileSystemDataLake)", controlFile.exists());

                    assertEquals("DataLake points to the wrong folder.", controlFile.getAbsolutePath(), dataLakeFolder.getAbsolutePath());
                }
        );

        callReleaseDataAndCheckResult(
                dataLakeURI[0],
                result -> {
                    File oldDl = new File(URI.create(dataLakeURI[0]));
                    assertFalse("the dataLake is still accessible (DataLake.fromURI didn't fail)", oldDl.exists());
                }
        );
    }

    @Test
    public void testExpirationCheck_FS() throws Exception {
        initTest("testExpirationCheck_FS");
        final String[] dataLakeURI = new String[1];

        callPrepareEmptyAndCheckResult(
                DataLakeFileSystemType.FS,
                DataLakeType.DLB,
                result -> {
                    ObjectMapper mapper = new ObjectMapper();
                    DataLakeStatus resp = mapper.readValue(result.getResponse()
                            .getContentAsString(), DataLakeStatus.class);
                    dataLakeURI[0] = resp.getURI();
                }
        );

        FileSystemDataLake dl = (FileSystemDataLake) DataLake.fromURI(dataLakeURI[0]);
        assertNotNull("Null creation date in Data Lake", dl.getCreationDate());
        assertNotNull("Null expiration date in Data Lake", dl.getExpirationDate());
        assertNotNull("Non-null last access date in Data Lake", dl.getLastAccess());

        Date now = new Date();
        assertEquals("Wrong expiration status.", dl.getExpirationDate().before(now), dl.hasExpired());

        callReleaseDataAndCheckResult(
                dataLakeURI[0],
                result -> {
                    File oldDl = new File(URI.create(dataLakeURI[0]));
                    assertFalse("the dataLake is still accessible (DataLake.fromURI didn't fail)", oldDl.exists());
                }
        );
    }
}