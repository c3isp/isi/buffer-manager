/*
 * Based on the template by Hewlett Packard Enterprise Development Company, L.P.
 */
package io.chino.c3isp.buffermanager.test;


import com.fasterxml.jackson.databind.ObjectMapper;
import io.chino.c3isp.buffermanager.BufferManager;
import io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeFileSystemType;
import io.chino.c3isp.buffermanager.restapi.types.requests.DataLakeType;
import io.chino.c3isp.buffermanager.restapi.types.requests.PrepareDataQuery;
import org.junit.*;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import static org.junit.Assert.fail;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@TestPropertySource(locations="classpath:application-test.properties")
public class BufferManagerBaseTest {

    protected static final List<String> TEST_DPO_IDS =
                    Arrays.asList(
                            /* Stix     */
                            "1564732319129-65b11eca-1dcd-4c13-be08-6e4e4689f928"
//                            /* Non stix */
//                            ,"1546960759867-a062c227-2b4c-4173-a63a-df7eb20df54a"
                    );


    @Autowired
    private MockMvc mockMvc;

    @Value("${security.user.name}")
    private String restUser;
    @Value("${security.user.password}")
    private String restPassword;

    protected static String TEST_CLASS_NAME;

    protected static ObjectMapper mapper;

    protected static HashMap<String, String> testInfo = new HashMap<>();


    @BeforeClass
    public static void initTestClass() throws Exception {
        mapper = new ObjectMapper();

        BufferManager.startTesting();

        Logger.getLogger("BufferManagerController Test")
                .info("* * * * * * BufferManager v" +
                        BufferManager.CURRENT_VERSION +
                        " TEST " + TEST_CLASS_NAME + " START * * * * * *");
    }

    @Before
    public void startTest() {
        // must be called for every test, because server is restarted each time.
        BufferManager.startTesting();

        // Log names of test objects to identify leftovers in database (users, tables, databases)
        // created by tests and not deleted:
        testInfo.put("name", "(not set)");           // the name of the test to be executed
        testInfo.put("user", "(not set)");           // the name of the user that performs the operations
        testInfo.put("datalake", "(not set)");       // the name of the folder/table of the datalake
    }

    @After
    public void endTest() {
        // Log names of test objects to identify leftovers in database (users, tables, databases)
        // created by tests and not deleted:
        Logger log = Logger.getLogger("TEST OBJECTS");
        log.info("\n" +
                "* * * * * * * * * * * * * * * * * * * * * * * * * * *\n" +
                "  TEST NAME: " + testInfo.get("name") + "\n" +
                "       USER: " + testInfo.get("user") + "\n" +
                "   DATALAKE: " + testInfo.get("datalake") + "\n" +
                "* * * * * * * * * * * * * * * * * * * * * * * * * * *");
        testInfo = new HashMap<>();
    }

    // Log names of test objects to identify leftovers in database
    protected static void initTest(String testName) {
        testInfo.put("name", testName);
    }
    protected static void setUserName(String userName) {
        testInfo.put("user", userName);
    }
    protected static void setDLName(String dataLakeName) {
        testInfo.put("datalake", dataLakeName);
    }

    public static final String testDPOName = "test";
    public static final String DPOContent = "testDPO";

    @AfterClass
    public static void endTestClass() throws Exception {
        Logger.getLogger("BufferManagerController Test")
                .info("* * * * * * BufferManager v" +
                        BufferManager.CURRENT_VERSION +
                        " TEST " + TEST_CLASS_NAME + " END * * * * * *");
    }

    /**
     * Mock a call to /v1/prepareData asking for a particular {@link DataLakeFileSystemType},
     * then perform some operations to verify that the call returned the correct values and that all the
     * operations succeeded.
     *
     * @param fileSystemType        the type of File system used for the new
     *                              {@link io.chino.c3isp.buffermanager.restapi.types.datalake.DataLake DataLake}
     * @param checkResultOperations a {@link ResultHandler} that checks the correctness of the returned data.
     * @throws Exception
     */
    protected void callPrepareDataAndCheckResult(DataLakeFileSystemType fileSystemType, DataLakeType dlType, ResultHandler checkResultOperations) throws Exception {
        PrepareDataQuery prepQuery = null;
        String file = null;
        try {
            file = "valid_query.json";
            prepQuery = new ObjectMapper().readValue(ClassLoader.getSystemResourceAsStream("./queries/" + file), PrepareDataQuery.class);
            prepQuery.setDPOIdList(TEST_DPO_IDS);
            prepQuery.setFileSystem(fileSystemType);
            prepQuery.setDataLake(dlType);
            prepQuery.setMetadata(
                    READ_METADATA
            );
        } catch (IOException e) {
            fail("Could not map '" + file + "' to PrepareDataQuery.\n"
                    + e.getMessage()
            );
        }

        this.mockMvc.perform(
                post("/v1/prepareData" + mapToUrlParams(prepQuery))
                        .content(new ObjectMapper().writeValueAsString(prepQuery.getDPOIdList()))
                        .with(httpBasic(restUser, restPassword))
                        .header(BufferManager.C3ISP_METADATA_HEADER, getMetadataString(prepQuery)) // set metadata
                        .contentType(MediaType.APPLICATION_JSON)    // client sends JSON PrepareDataQuery
                        .accept(MediaType.APPLICATION_JSON)   // server replies with JSON PrepareDataResponse
                )
                .andDo(
                        MockMvcResultHandlers.print()
                )
                .andExpect(
                        status().isCreated()   // check HTTP status (201)
                )
                .andDo(
                        checkResultOperations  // custom output check
                );
    }

    protected void callPrepareEmptyAndCheckResult(DataLakeFileSystemType fileSystemType, DataLakeType dlType, ResultHandler checkResultOperations) throws Exception {
        String params = "?" +
                "data_lake=" + dlType + "&" +
                "type=" + fileSystemType;

        this.mockMvc.perform(
                post("/v1/prepareEmptyDataLake" + "/" + params)
                        .with(httpBasic(getRestUser(), getRestPassword()))
                        .contentType(MediaType.APPLICATION_JSON)    // client sends empty body
                        .accept(MediaType.APPLICATION_JSON)   // server replies with JSON DataLakeStatus
        )
                .andDo(
                        MockMvcResultHandlers.print()
                )
                .andExpect(
                        status().isCreated()   // check HTTP status (201)
                )
                .andDo(
                        checkResultOperations  // check output is same as expected
                );
    }

    protected void callPopulateDataLakeAndCheckResult(String dataLakeURI, ResultHandler checkResultOperations) throws Exception {

        String DPOContent = "testDPO";

        String uriTemplate = "/v1/populateDataLake"
                + "?" + "uri=" + dataLakeURI;

        // omit optional parameter file_name
        this.mockMvc.perform(
                post(uriTemplate)
                        .content(DPOContent)
                        .with(httpBasic(restUser, restPassword))
                        .contentType(MediaType.TEXT_PLAIN)    // client sends DPO content as text
                        .accept(MediaType.APPLICATION_JSON)   // server replies with JSON DPOWriteOutcome
        )
                .andExpect(
                        status().isCreated()        // check HTTP status (201)
                );

        uriTemplate += "&" + "file_name=" + testDPOName;
        // specify name and perform custom checks
        this.mockMvc.perform(
                post(uriTemplate)
                        .content(DPOContent)
                        .with(httpBasic(restUser, restPassword))
                        .contentType(MediaType.TEXT_PLAIN)    // client sends DPO content as text
                        .accept(MediaType.APPLICATION_JSON)   // server replies with JSON DPOWriteOutcome
        )
                .andDo(
                        MockMvcResultHandlers.print()
                )
                .andExpect(
                        status().isCreated()        // check HTTP status (201)
                )
                .andDo(
                        checkResultOperations       // custom output check
                );
    }

    /**
     * Mock a call to /v1/releaseData sending a URI to a
     * {@link io.chino.c3isp.buffermanager.restapi.types.datalake.DataLake},
     * then perform some operations to verify that the call returned the correct values and that all the
     * operations succeeded.
     *
     * @param dataLakeURI
     * @param checkResultOperations
     * @throws Exception
     */
    protected void callReleaseDataAndCheckResult(String dataLakeURI, ResultHandler checkResultOperations) throws Exception {
        this.mockMvc.perform(
                post("/v1/releaseData")
                        .content(dataLakeURI)
                        .with(httpBasic(restUser, restPassword))
                        .contentType(MediaType.APPLICATION_JSON)    // client sends JSON PrepareDataQuery
                        .accept(MediaType.TEXT_PLAIN)               // server replies with JSON PrepareDataResponse
        )
                .andDo(
                        MockMvcResultHandlers.print()
                )
                .andExpect(
                        status().isOk()        // check HTTP status (200)
                )
                .andDo(
                        checkResultOperations  // custom output check
                );
    }

    private Object getMetadataString(PrepareDataQuery prepQuery) {
        return prepQuery.getMetadata();
    }

    /**
     * Map {@link PrepareDataQuery} attributes to a URL query
     *
     * @param query the query object for the prepareData
     * @return a URL query {@link String} that can be appended to the prepareData request URL
     */
    private String mapToUrlParams(PrepareDataQuery query) {
        StringBuilder urlQuery = new StringBuilder("?");

        urlQuery
                .append("service_name=")
                .append(query.getServiceName())
                .append("&");
        urlQuery
                .append("format=")
                .append(query.getDataFormat().toString())
                .append("&");
        urlQuery
                .append("data_lake=")
                .append(query.getDataLake().toString())
                .append("&");
        urlQuery
                .append("type=")
                .append(query.getFileSystem().toString());

        return urlQuery.toString();
    }

    public MockMvc getMockMvc() {
        return mockMvc;
    }

    public String getRestUser() {
        return restUser;
    }

    public String getRestPassword() {
        return restPassword;
    }

    private static final String READ_METADATA =
            "{\"Request\":{\"Attribute\":[{\"AttributeId\":\"ns:c3isp:dpo-metadata\",\"Value\":\"{\\\"id\\\":\\\"4000123\\\",\\\"dsa_id\\\":\\\"DSA-c7aa89e7-ed98-42a8-91cf-74876c7ebc3e\\\",\\\"start_time\\\":\\\"2017-12-14T12:00:00.0Z\\\",\\\"end_time\\\":\\\"2017-12-14T18:01:01.0Z\\\",\\\"event_type\\\":\\\"Firewall Event\\\",\\\"organization\\\":\\\"3DRepo\\\"}\",\"DataType\":\"string\"}]}}";
}
