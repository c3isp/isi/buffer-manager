# BufferManager Properties

The configuration of the BufferManager can be found in the following application.properties file that
contains basic application (Spring) configuration and basic config for Data Lakes, such as the URL to MySQL database
and the file system URI

The default configuration allows to use the Buffer Manager within the Central ISI.

## Properties values

To customize the configuration of the BufferManager, the following properties can be set at runtime:

### File System Data Lakes
* `data.filesystem.root` ~ the URI to the File System Data Lake folder (begins with `file://`)

### MySQL Data Lakes
* `data.mysql.url` ~ the MySQL URL to the database instance where the Data Lakes must be created (begins with `jdbc:mysql://`) 
* `data.mysql.host` ~ the hostname of the newly created users. For most installations only the default value `%` will work 
and the db might block users that specify a different hostname. Change it only if you know what you are doing.
* `data.mysql.database` ~ Data Lake Buffers are just Schemas created inside an existing database.
This parameter is the name of the Schema which contains DLBs. When required, the BufferManager will check if this Schema exists and, 
if not, will create it.
* `data.mysql.user.name` ~ the name of the **root** user of the database (must have permissions to write, delete, read etc. everything on the db)
* `data.mysql.user.password` ~ the password of the **root** user.

____

### HDFS Data Lakes
* `data.hdfs.uri` ~ The URI to the HDFS installation (begins with `hdfs://`)
* `data.hdfs.root` ~ Name of the root folder containing Data Lakes
* `data.hdfs.user.name` ~ name of the **root** user of the HDFS (must have permissions to write, delete, read etc. everything in the folder)

# Test BufferManager
In order for the tests to succeed, you must have:

* A working instance of all Data Lake types (e.g. File System and MySQL) that you are going to test,
which must be accessible from the machine you run tests from. Please note that querying a MySQL instance from
a different host may be problematic. It is suggested to use a MySQL instance which runs on the same machine where the
tests are being executed. See the section **Run locally**, below.

* For **File System**: read and write permissions over the Data Lake folder

* For **MySQL**: the Schema used for testing DLBs (create and delete it manually when needed, 
pass the name in the `data.mysql.dlb.database` property).

# Run locally
You can run the BufferManager and/or the tests on a local machine with a custom instance of MySQL/file systems
(instead of the remote DB on the IAI virtual machine). Follow those steps:

1. Copy file `datalake.properties` to a file `local.datalake.properties`
and override the required values to match your local Data Lake configuration.

2. Copy file `test.properties` to a file `local.test.properties`
and override the required values to match your local Data Lake test configuration.
These *local* files are ignored by version control and won't be added to the git repository.

3. Set an environment variable
   
           LOCAL_PROPERTIES=true
           
    When this flag is set, the BufferManager will look in the *local* files that we have created.

4. Now you can run the BufferManager using a local database or test that it's working before installing on a remote
machine.